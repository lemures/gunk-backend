using Src.VectorTypes;

namespace Src.SkeletonAnimations
{
    public interface IBarrelPositionCalculator
    {
        Float3 getBarrelBoneWorldPosition(int index);
    }
}