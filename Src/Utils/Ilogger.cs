namespace Src.Utils
{
    public interface IGunkLogger
    {
        void addLog(int authorId, string message);
        
        void addLog(int authorId, string component, string message);
    }
}