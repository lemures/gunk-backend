using Gunk_Scripts.GunkComponents;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class SmallEnergyGenerator : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.SmallEnergyGenerator);
        private static readonly int maxHealth = 1000;

        public SmallEnergyGenerator(IGunkObjectManagerInternalData objectManagerFacade, Float3 position, Faction faction) : base(
            objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleObject(this));
            addPlacedObject();
            addCuboidCollider(new Float3(2, 3, 2));
            addEnergyGenerator(new ConstantValueEnergyGenerator(100.0f));
            addEnergyNeededForConstruction(500.0f);
            finishAddingComponents();
        }
    }
}