﻿using Src.VectorTypes;

namespace Src.SkeletonAnimations.Internal
{
    public class AnimatorBoneData
    {
        public int boneLocalId;
        public int previousBoneLocalId;
        public Float3 boneBasicTranslation;
        public Float3 boneCenter;

        public AnimatorBoneData(int boneLocalId, int previousBoneLocalId, Float3 boneBasicTranslation, Float3 boneCenter)
        {
            this.boneLocalId = boneLocalId;
            this.previousBoneLocalId = previousBoneLocalId;
            this.boneBasicTranslation = boneBasicTranslation;
            this.boneCenter = boneCenter;
        }
    }
}