namespace Gunk_Scripts.GunkComponents
{
    public interface IEnergyGenerator
    {
        float getEnergyGenerationLevel();
    }
}