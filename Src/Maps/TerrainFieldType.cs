using System;

namespace Src.Maps
{
    [Serializable]
    public enum TerrainFieldType
    {
        Ground,
        Water,
        Cliff,
        Sand,
        Stone,
        CrystalMine
    }
}