using System;
using Src.Turrets;
using Src.Utils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public class NavigationAgentComponent : GunkComponent, INavigationAgent, IUpdateCallbackReceiver
    {
        private readonly INavigationAgent agent;

        public NavigationAgentComponent(IGunkObject parent, INavigationAgent agent) : base(parent)
        {
            this.agent = agent;
        }

        public bool isStopped => agent.isStopped;

        public void stop()
        {
            parent.addLog(this, "agent was stopped");
            agent.stop();
        }

        public Float3 destination
        {
            get { return agent.destination; }
            set
            {
                parent.addLog(this, "destination was set to " + value);
                agent.destination = value;
            }
        }

        public void resume()
        {
            parent.addLog(this, "agent was resumed");
            agent.resume();
        }

        public float remainingDistance => agent.remainingDistance;

        public void setMaxSpeed(float value)
        {
            agent.setMaxSpeed(value);
        }

        public Float3 position => agent.position;

        public void update(float deltaTime)
        {
            (agent as IUpdateCallbackReceiver)?.update(deltaTime);

            if (parent.transform.position != agent.position) parent.transform.position = agent.position;
        }

        public override void destroy()
        {
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(NavigationAgentComponent))
                .addVariable(remainingDistance, nameof(remainingDistance))
                .addVariable(position, nameof(position))
                .addVariable(destination, nameof(destination))
                .addVariable(isStopped, nameof(isStopped))
                .print();
        }
    }
}