using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;
using Src.DataStructures;
using Src.Maps;
using Src.SerializationUtils;
using Src.VectorTypes;

namespace Src.Turrets.Tests.Editor
{
    public class ClosestCorruptedFieldLocatorTests
    {
        private CorruptedLand corruptedLand;
        private IGunkObject fakeObject;
        private ITerrainHeightSampler fakeSampler;
        private IIdentityProvider identityProvider = new IdManager();

        [SetUp]
        public void installObjects()
        {
            fakeObject = A.Fake<IGunkObject>();
            A.CallTo(() => fakeObject.transform.position).Returns(Float3.zero);
            corruptedLand = new CorruptedLand(10, 10);
            corruptedLand.infectField(new Int2(7, 9));
            fakeSampler = A.Fake<ITerrainHeightSampler>();
            A.CallTo(() => fakeObject.objectManager.corruptedLand).Returns(corruptedLand);
            A.CallTo(() => fakeSampler.sampleHeight(A<Float3>.Ignored)).Returns(1.0f);
            A.CallTo(() => fakeObject.objectManager.map.heightSampler).Returns(fakeSampler);
        }

        [Test]
        public void targetIsFoundIfInRange()
        {
            var locator = new ClosestCorruptedFieldLocator(fakeObject, 15.0f);
            var foundTarget = locator.getTarget() as CorruptedFieldTarget;

            Assert.AreEqual(new Float3(7.5f, 1.0f, 9.5f), foundTarget?.currentPosition);
        }

        [Test]
        public void targetIsUpdatedAfterCorruptedLandChanges()
        {
            var locator = new ClosestCorruptedFieldLocator(fakeObject, 15.0f);
            corruptedLand.infectField(new Int2(1, 3));
            var foundTarget = locator.getTarget() as CorruptedFieldTarget;

            Assert.AreEqual(new Float3(1.5f, 1.0f, 3.5f), foundTarget?.currentPosition);
        }

        [Test]
        public void nullIsReturnedIfTargetIsOutsideRange()
        {
            var locator = new ClosestCorruptedFieldLocator(fakeObject, 3.0f);
            var foundTarget = locator.getTarget() as CorruptedFieldTarget;

            Assert.AreEqual(null, foundTarget?.currentPosition);
        }

        [Test]
        public void terrainIsSampledAtRightPlace()
        {
            var locator = new ClosestCorruptedFieldLocator(fakeObject, 15.0f);
            var foundTarget = locator.getTarget() as CorruptedFieldTarget;

            A.CallTo(() => fakeSampler.sampleHeight(A<Float3>
                    .That.Matches(field => field.x == 7.5f && field.z == 9.5f)))
                .MustHaveHappened();
        }
    }
}