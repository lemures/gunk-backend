using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Src.Utils
{
    public static class BinarySerializationUtils
    {
        public static byte[] serializeObject(object objectToSerialize)
        {
            var formatter = new BinaryFormatter();
            var memoryStream = new MemoryStream();
            formatter.Serialize(memoryStream, objectToSerialize);
            return memoryStream.GetBuffer();
        }

        public static object deserializeObject(byte[] data)
        {
            var formatter = new BinaryFormatter();
            var memoryStream = new MemoryStream(data);
            return formatter.Deserialize(memoryStream);
        }

        public static T cloneObject<T>(T objectToClone) where T : class
        {
            var bytes = serializeObject(objectToClone);
            return deserializeObject(bytes) as T;
        }
    }
}