using System;
using System.Collections.Generic;
using System.Linq;
using Src.GunkComponents;
using Src.Libraries;
using Src.ShaderUtils;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.Events;
using Src.SkeletonAnimations.Internal;
using Src.Utils;
using Src.VectorTypes;

namespace Src.SkeletonAnimations
{
    public class SkeletonAnimator : ISkeletonAnimator
    {
        private readonly int myId;
        private float currentArgument;
        private readonly ActivePose currentPose;
        private float desiredArgument;

        private readonly SkeletonData skeletonData;

        private IAnimationClipDatabase clipDatabase;

        private IAnimatorsManager animatorsManager;
        
        public SkeletonAnimator(string skeletonDataName, IRenderersParametersManager objectManager, IAnimationClipDatabase clipDatabase, IAssetManager assetManager, int objectId,
            IAnimatorsManager animatorsManager, string startClipName = "")
        {
            this.animatorsManager = animatorsManager;
            this.clipDatabase = clipDatabase;
            skeletonData = new Skeleton(skeletonDataName, assetManager).skeletonData;
            myId = animatorsManager.addAnimator(objectManager, objectId);
            var boneDatas = skeletonData.getBonesData();
            boneDatas.ForEach(x => animatorsManager.addBone(myId, x));
            var startClip = clipDatabase.getClip(startClipName);
            currentPose = new ActivePose(startClip);
            currentClip = startClipName;
        }

        public string currentClip { get; private set; }

        public bool isCurrentClipLoaded()
        {
            return currentPose.isCurrentClipLoaded() && Math.Abs(currentArgument - desiredArgument) < 0.1f;
        }

        public void playClip(string name, float argument = 0)
        {
            var clip = clipDatabase.getClip(name);

            desiredArgument = argument;
            currentArgument = argument;
            currentClip = name;
            currentPose.playNewClip(clip, argument);
        }

        public void changeClipArgument(float value)
        {
            if (float.IsNaN(value))
                throw new ArgumentOutOfRangeException(nameof(value), "clip argument name cannot be NaN");
            desiredArgument = value;
        }

        public List<SocketData> getAllSocketBones()
        {
            var result =
                from bone in skeletonData.getAllBones()
                where bone is SocketBone
                select new SocketData(bone.position, (bone as SocketBone).rotation);
            return result.ToList();
        }

        public Float3 getBarrelBoneWorldPosition(int index, Float3 objectPosition)
        {
            return animatorsManager.getBonePosition(myId, skeletonData.getBarrelBoneId(index), objectPosition);
        }

        public List<EventData> collectTriggeredEvents()
        {
            throw new NotImplementedException();
        }

        public void updateAnimation(float timeElapsed)
        {
            var change = Math.Min(timeElapsed, Math.Abs(desiredArgument - currentArgument));
            currentArgument += (currentArgument < desiredArgument ? 1 : -1) * change;

            currentPose.updateArgument(currentArgument);
            currentPose.update(timeElapsed);

            var output = currentPose.evaluateCurrentPose();
            foreach (var animationOutput in output)
            {
                animatorsManager.updateRotation(myId, animationOutput.boneId, animationOutput.rotation);
                animatorsManager.updateTranslation(myId, animationOutput.boneId, animationOutput.translation);
            }
        }

        public void destroy()
        {
            animatorsManager.removeAnimator(myId);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(SkeletonAnimator))
                .addVariable(myId, nameof(myId))
                .addVariable(currentArgument, nameof(currentArgument))
                .addVariable(skeletonData, nameof(skeletonData))
                .print();
        }
    }
}