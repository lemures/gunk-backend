using System;

namespace Src.Utils
{
    public interface IAssetManager
    {
        void saveObject(object objectToSave, string name, string categoryName = "");

        void saveObjectAsJson(object objectToSave, string name, string categoryName = "");

        void deleteAllVersions(string name, string categoryName = "");
        
        object getObjectSavedCopy(string name, string categoryName = "");

        object getObjectFromJson(string name, Type type, string categoryName = "");
    }
}