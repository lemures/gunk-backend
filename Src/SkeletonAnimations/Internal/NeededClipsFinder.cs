using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Src.SkeletonAnimations.Internal
{
    public static class NeededClipsFinder
    {
        public static List<string> getAllNeededScripts()
        {
            var result = new List<string>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var attributes =
                    from type in assembly.GetTypes()
                    where type.IsDefined(typeof(NeedsClip), false)
                    select type.GetCustomAttributes<NeedsClip>();
                foreach (var attribute in attributes) attribute.ToList().ForEach(x => result.Add(x.clipName));
            }

            //empty "default" Clip
            result.Add("");

            return result;
        }
    }
}