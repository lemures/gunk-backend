using Src.Maps;
using Src.SerializationUtils;
using Src.VectorTypes;

namespace Src.Turrets
{
    public class CorruptedAreaTarget : IDemagableTarget
    {
        private readonly CorruptedLand corruptedLand;

        private int radius;
        
        public CorruptedAreaTarget(Float3 currentPosition, int radius, CorruptedLand corruptedLand)
        {
            this.radius = radius;
            this.currentPosition = currentPosition;
            this.corruptedLand = corruptedLand;
        }

        public Float3 currentPosition { get; }

        public void damage(float damage)
        {
            var position = new Int2((int) currentPosition.x, (int) currentPosition.z);
            for (int i = -radius; i <= radius; i++)
            {
                for (int q = -radius; q <= radius; q++)
                {
                    if (i*i + q*q <= radius * radius)
                    corruptedLand.damageField(position + new Int2(i, q), damage);
                }
            }
        }

        public long uniqueId { get; }
    }
}