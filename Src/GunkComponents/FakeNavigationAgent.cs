using System;
using Src.Turrets;
using Src.Utils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public class FakeNavigationAgent : INavigationAgent, IUpdateCallbackReceiver
    {
        private float maxSpeed = 10.0f;

        public FakeNavigationAgent(Float3 currentPosition)
        {
            this.currentPosition = currentPosition;
            destination = currentPosition;
        }

        public Float3 currentPosition { get; private set; }

        public Float3 destination { get; set; }
        public bool isStopped { get; private set; } = true;

        public void stop()
        {
            isStopped = false;
        }


        public void resume()
        {
            isStopped = true;
        }

        public float remainingDistance => Float3.Distance(destination, currentPosition);

        public void setMaxSpeed(float value)
        {
            maxSpeed = value;
        }

        public Float3 position => currentPosition;

        public void update(float deltaTime)
        {
            if (!isStopped) return;

            if (remainingDistance < maxSpeed * deltaTime)
            {
                currentPosition = destination;
                return;
            }

            currentPosition += Float3.Normalize(destination - currentPosition) * maxSpeed * deltaTime;
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(FakeNavigationAgent))
                .addVariable(remainingDistance, nameof(remainingDistance))
                .addVariable(currentPosition, nameof(currentPosition))
                .addVariable(destination, nameof(destination))
                .addVariable(isStopped, nameof(isStopped))
                .print();
        }
    }
}