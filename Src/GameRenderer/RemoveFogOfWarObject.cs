namespace Src.GameRenderer
{
    public class RemoveFogOfWarObject : ExtraObjectsChange
    {
        public int id;

        public RemoveFogOfWarObject(int id)
        {
            this.id = id;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.fogOfWarObjectsCollection.removeObject(id);
        }
    }
}