﻿using Src.VectorTypes;

namespace Src.ObjectsSelection
{
    public class DragRectangle
    {
        public Float2 firstPosition;
        public Float2 secondPosition;

        public DragRectangle(Float2 firstPosition, Float2 secondPosition)
        {
            this.firstPosition = firstPosition;
            this.secondPosition = secondPosition;
        }

        public override string ToString()
        {
            return "firstPosition: " + firstPosition + " secondPosition: " + secondPosition;
        }
    }
}