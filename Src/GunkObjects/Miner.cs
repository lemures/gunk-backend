using Gunk_Scripts.GunkComponents;
using Src.GunkComponents;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class Miner : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.Miner);
        private readonly float maxEnergy = 2000.0f;
        private readonly float maxHealth = 500;

        public Miner(IGunkObjectManagerInternalData objectManagerFacade, Float3 position, Faction faction) : base(
            objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addPlacedObject(objectManager.calculatePrefabMeshDimensions(prefabName));
            addEnergyNeededForConstruction(1000.0f);
            addEnergy(0, maxEnergy, 100.0f);
            addConnectible(new ConnectibleObject(this));
            addComponent(new MinerAi(this));
            finishAddingComponents();
        }
    }
}