﻿using System;

namespace Src.Utils
{
    public class StopWatch
    {
        private bool isStopped;
        private DateTime lastStartDate;
        private DateTime totalElapsed;

        public StopWatch(bool isStopped)
        {
            lastStartDate = DateTime.Now;
            this.isStopped = isStopped;
        }

        public double getMsElapsed()
        {
            return totalElapsed.Millisecond + (isStopped ? 0 : (DateTime.Now - lastStartDate).Milliseconds);
        }

        public double getSecElapsed()
        {
            return totalElapsed.Second + (isStopped ? 0 : (DateTime.Now - lastStartDate).Seconds);
        }

        /// <summary>
        ///     Stops timer ticking
        /// </summary>
        public void stop()
        {
            if (isStopped == false)
                totalElapsed += DateTime.Now - lastStartDate;
            isStopped = true;
        }

        /// <summary>
        ///     Resumes ticking
        /// </summary>
        public void resume()
        {
            lastStartDate = DateTime.Now;
            isStopped = false;
        }
    }
}