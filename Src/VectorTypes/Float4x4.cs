using System;

namespace Src.VectorTypes
{
    public struct Float4x4
    {
        public static readonly Float4x4 zero = new Float4x4(
            new Float4(0.0f, 0.0f, 0.0f, 0.0f), 
            new Float4(0.0f, 0.0f, 0.0f, 0.0f), 
            new Float4(0.0f, 0.0f, 0.0f, 0.0f), 
            new Float4(0.0f, 0.0f, 0.0f, 0.0f));
        public static readonly Float4x4 identity = new Float4x4(
            new Float4(1f, 0.0f, 0.0f, 0.0f), 
            new Float4(0.0f, 1f, 0.0f, 0.0f), 
            new Float4(0.0f, 0.0f, 1f, 0.0f), 
            new Float4(0.0f, 0.0f, 0.0f, 1f));

        private float m00;
        private float m10;
        private float m20;
        private float m30;

        private float m01;
        private float m11;
        private float m21;
        private float m31;

        private float m02;
        private float m12;
        private float m22;
        private float m32;

        private float m03;
        private float m13;
        private float m23;
        private float m33;

        public Float4x4(
            Float4 column0, 
            Float4 column1, 
            Float4 column2, 
            Float4 column3)
        {
            m00 = column0.x;
            m01 = column1.x;
            m02 = column2.x;
            m03 = column3.x;
            m10 = column0.y;
            m11 = column1.y;
            m12 = column2.y;
            m13 = column3.y;
            m20 = column0.z;
            m21 = column1.z;
            m22 = column2.z;
            m23 = column3.z;
            m30 = column0.w;
            m31 = column1.w;
            m32 = column2.w;
            m33 = column3.w;
        }
        
        public static Float4x4 operator *(Float4x4 lhs, Float4x4 rhs)
    {
      Float4x4 float4X4;
      float4X4.m00 = (float) (lhs.m00 * (double) rhs.m00 + lhs.m01 * (double) rhs.m10 + lhs.m02 * (double) rhs.m20 + lhs.m03 * (double) rhs.m30);
      float4X4.m01 = (float) (lhs.m00 * (double) rhs.m01 + lhs.m01 * (double) rhs.m11 + lhs.m02 * (double) rhs.m21 + lhs.m03 * (double) rhs.m31);
      float4X4.m02 = (float) (lhs.m00 * (double) rhs.m02 + lhs.m01 * (double) rhs.m12 + lhs.m02 * (double) rhs.m22 + lhs.m03 * (double) rhs.m32);
      float4X4.m03 = (float) (lhs.m00 * (double) rhs.m03 + lhs.m01 * (double) rhs.m13 + lhs.m02 * (double) rhs.m23 + lhs.m03 * (double) rhs.m33);
      float4X4.m10 = (float) (lhs.m10 * (double) rhs.m00 + lhs.m11 * (double) rhs.m10 + lhs.m12 * (double) rhs.m20 + lhs.m13 * (double) rhs.m30);
      float4X4.m11 = (float) (lhs.m10 * (double) rhs.m01 + lhs.m11 * (double) rhs.m11 + lhs.m12 * (double) rhs.m21 + lhs.m13 * (double) rhs.m31);
      float4X4.m12 = (float) (lhs.m10 * (double) rhs.m02 + lhs.m11 * (double) rhs.m12 + lhs.m12 * (double) rhs.m22 + lhs.m13 * (double) rhs.m32);
      float4X4.m13 = (float) (lhs.m10 * (double) rhs.m03 + lhs.m11 * (double) rhs.m13 + lhs.m12 * (double) rhs.m23 + lhs.m13 * (double) rhs.m33);
      float4X4.m20 = (float) (lhs.m20 * (double) rhs.m00 + lhs.m21 * (double) rhs.m10 + lhs.m22 * (double) rhs.m20 + lhs.m23 * (double) rhs.m30);
      float4X4.m21 = (float) (lhs.m20 * (double) rhs.m01 + lhs.m21 * (double) rhs.m11 + lhs.m22 * (double) rhs.m21 + lhs.m23 * (double) rhs.m31);
      float4X4.m22 = (float) (lhs.m20 * (double) rhs.m02 + lhs.m21 * (double) rhs.m12 + lhs.m22 * (double) rhs.m22 + lhs.m23 * (double) rhs.m32);
      float4X4.m23 = (float) (lhs.m20 * (double) rhs.m03 + lhs.m21 * (double) rhs.m13 + lhs.m22 * (double) rhs.m23 + lhs.m23 * (double) rhs.m33);
      float4X4.m30 = (float) (lhs.m30 * (double) rhs.m00 + lhs.m31 * (double) rhs.m10 + lhs.m32 * (double) rhs.m20 + lhs.m33 * (double) rhs.m30);
      float4X4.m31 = (float) (lhs.m30 * (double) rhs.m01 + lhs.m31 * (double) rhs.m11 + lhs.m32 * (double) rhs.m21 + lhs.m33 * (double) rhs.m31);
      float4X4.m32 = (float) (lhs.m30 * (double) rhs.m02 + lhs.m31 * (double) rhs.m12 + lhs.m32 * (double) rhs.m22 + lhs.m33 * (double) rhs.m32);
      float4X4.m33 = (float) (lhs.m30 * (double) rhs.m03 + lhs.m31 * (double) rhs.m13 + lhs.m32 * (double) rhs.m23 + lhs.m33 * (double) rhs.m33);
      return float4X4;
    }
        
        public static Float4x4 translate(Float3 vector)
    {
        Float4x4 float4X4;
        float4X4.m00 = 1f;
        float4X4.m01 = 0.0f;
        float4X4.m02 = 0.0f;
        float4X4.m03 = vector.x;
        float4X4.m10 = 0.0f;
        float4X4.m11 = 1f;
        float4X4.m12 = 0.0f;
        float4X4.m13 = vector.y;
        float4X4.m20 = 0.0f;
        float4X4.m21 = 0.0f;
        float4X4.m22 = 1f;
        float4X4.m23 = vector.z;
        float4X4.m30 = 0.0f;
        float4X4.m31 = 0.0f;
        float4X4.m32 = 0.0f;
        float4X4.m33 = 1f;
        return float4X4;
    }
        
        public static Float4x4 rotate(Float3 rotation)
    {
        Float4 quaternion;
            
        var cy = (float)Math.Cos(rotation.z * 0.5);
        var sy = (float)Math.Sin(rotation.z * 0.5);
        var cp = (float)Math.Cos(rotation.y * 0.5);
        var sp = (float)Math.Sin(rotation.y * 0.5);
        var cr = (float)Math.Cos(rotation.x * 0.5);
        var sr = (float)Math.Sin(rotation.x * 0.5);

        quaternion.w = cy * cp * cr + sy * sp * sr;
        quaternion.x = cy * cp * sr - sy * sp * cr;
        quaternion.y = sy * cp * sr + cy * sp * cr;
        quaternion.z = sy * cp * cr - cy * sp * sr;

        var num1 = quaternion.x * 2f;
        var num2 = quaternion.y * 2f;
        var num3 = quaternion.z * 2f;
        var num4 = quaternion.x * num1;
        var num5 = quaternion.y * num2;
        var num6 = quaternion.z * num3;
        var num7 = quaternion.x * num2;
        var num8 = quaternion.x * num3;
        var num9 = quaternion.y * num3;
        var num10 = quaternion.w * num1;
        var num11 = quaternion.w * num2;
        var num12 = quaternion.w * num3;
        Float4x4 float4X4;
        float4X4.m00 = (float) (1.0 - ((double) num5 + (double) num6));
        float4X4.m10 = num7 + num12;
        float4X4.m20 = num8 - num11;
        float4X4.m30 = 0.0f;
        float4X4.m01 = num7 - num12;
        float4X4.m11 = (float) (1.0 - ((double) num4 + (double) num6));
        float4X4.m21 = num9 + num10;
        float4X4.m31 = 0.0f;
        float4X4.m02 = num8 + num11;
        float4X4.m12 = num9 - num10;
        float4X4.m22 = (float) (1.0 - ((double) num4 + (double) num5));
        float4X4.m32 = 0.0f;
        float4X4.m03 = 0.0f;
        float4X4.m13 = 0.0f;
        float4X4.m23 = 0.0f;
        float4X4.m33 = 1f;
        return float4X4;
    }
        
        public Float3 multiplyPoint(Float3 point)
        {
            
            Float3 float3;
            float3.x = (float) ((double) this.m00 * (double) point.x + (double) this.m01 * (double) point.y + (double) this.m02 * (double) point.z) + this.m03;
            float3.y = (float) ((double) this.m10 * (double) point.x + (double) this.m11 * (double) point.y + (double) this.m12 * (double) point.z) + this.m13;
            float3.z = (float) ((double) this.m20 * (double) point.x + (double) this.m21 * (double) point.y + (double) this.m22 * (double) point.z) + this.m23;
            float num = 1f / ((float) ((double) this.m30 * (double) point.x + (double) this.m31 * (double) point.y + (double) this.m32 * (double) point.z) + this.m33);
            float3.x *= num;
            float3.y *= num;
            float3.z *= num;
            return float3;
        }
    }
}