using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddMortarBullet : ExtraObjectsChange
    {
        protected bool Equals(AddMortarBullet other)
        {
            return id == other.id && startPosition.Equals(other.startPosition) && endPosition.Equals(other.endPosition);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AddMortarBullet) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = id;
                hashCode = (hashCode * 397) ^ startPosition.GetHashCode();
                hashCode = (hashCode * 397) ^ endPosition.GetHashCode();
                return hashCode;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.generalPurposeLineRenderer.addObject(id, startPosition, endPosition);
        }

        private readonly int id;

        private readonly Float3 startPosition;

        private readonly Float3 endPosition;

        public AddMortarBullet(int id, Float3 startPosition, Float3 endPosition)
        {
            this.id = id;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }
    }
}