using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddLaser : ExtraObjectsChange
    {
        private readonly Float3 end;
        private readonly Float3 start;

        private readonly int id;

        public AddLaser(int id, Float3 start, Float3 end)
        {
            this.id = id;
            this.start = start;
            this.end = end;
        }

        protected bool Equals(AddLaser other)
        {
            return id == other.id && start.Equals(other.start) && end.Equals(other.end);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((AddLaser) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = id;
                hashCode = (hashCode * 397) ^ start.GetHashCode();
                hashCode = (hashCode * 397) ^ end.GetHashCode();
                return hashCode;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.generalPurposeLineRenderer.addObject(id, start, end);
        }
    }
}