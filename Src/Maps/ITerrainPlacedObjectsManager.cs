using Src.VectorTypes;

namespace Src.Maps
{
    public interface ITerrainPlacedObjectsManager
    {
        bool canBuild(Float3 position, ObjectPattern pattern);
        void construct(Float3 position, ObjectPattern pattern);
        void deconstruct(Float3 position, ObjectPattern pattern);
        void setFieldType(Float3 position, TerrainFieldType newType);
        Float3? calculateClosestAvailablePosition(Float3 position, ObjectPattern pattern);
        TerrainFieldType getFieldType(Float3 location);
    }
}