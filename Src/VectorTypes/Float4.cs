using System;

namespace Src.VectorTypes
{
    /// <summary>
    ///     Helper class for serialization of basic unity built-in types
    /// </summary>
    [Serializable]
    public struct Float4
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public Float4(Float3 data)
        {
            x = data.x;
            y = data.y;
            z = data.z;
            w = 0;
        }

        public Float4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static Float4 operator +(Float4 a, Float4 b)
        {
            return new Float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
        }

        public override string ToString()
        {
            return "{" + x + " " + y + " " + z + " " + w + " }";
        }
    }
}