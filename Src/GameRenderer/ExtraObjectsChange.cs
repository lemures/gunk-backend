namespace Src.GameRenderer
{
    public abstract class ExtraObjectsChange
    {
        public abstract void execute(IExtraObjectsRenderer objectsRenderer);
    }
}