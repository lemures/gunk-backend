namespace Src.GameRenderer
{
    public class RemoveManufacturingRay : ExtraObjectsChange
    {
        private readonly int objectId;

        public RemoveManufacturingRay(int objectId)
        {
            this.objectId = objectId;
        }

        protected bool Equals(RemoveManufacturingRay other)
        {
            return objectId == other.objectId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((RemoveManufacturingRay) obj);
        }

        public override int GetHashCode()
        {
            return objectId;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.miningRayRenderer.removeObject(objectId);
        }
    }
}