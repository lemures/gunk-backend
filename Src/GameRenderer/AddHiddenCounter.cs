using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;

namespace Src.GameRenderer
{
    public class AddHiddenCounter : ExtraObjectsChange
    {
        private readonly Counter counter;

        private readonly int objectId;
        private readonly CounterDrawStyle style;

        public AddHiddenCounter(int objectId, Counter counter, CounterDrawStyle style)
        {
            this.objectId = objectId;
            this.counter = counter;
            this.style = style;
        }

        protected bool Equals(AddHiddenCounter other)
        {
            return objectId == other.objectId && counter.Equals(other.counter) && style == other.style;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((AddHiddenCounter) obj);
        }

        public override int GetHashCode()
        {
            var hashCode = objectId;
            hashCode = (hashCode * 397) ^ counter.GetHashCode();
            hashCode = (hashCode * 397) ^ (int) style;
            return hashCode;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.countersRenderer.addHiddenCounter(objectId, counter, style);
        }
    }
}