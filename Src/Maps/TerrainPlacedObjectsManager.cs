using Src.VectorTypes;

namespace Src.Maps
{
    public class TerrainPlacedObjectsManager : ITerrainPlacedObjectsManager
    {
        private readonly ITerrainHeightSampler heightSampler;
        private readonly TerrainFieldsManager terrainFieldsManager;

        public TerrainPlacedObjectsManager(int mapHeight, int mapWidth, ITerrainHeightSampler heightSampler)
        {
            terrainFieldsManager = new TerrainFieldsManager(mapHeight, mapWidth);
            this.heightSampler = heightSampler;
        }

        public TerrainPlacedObjectsManager(TerrainFieldsManager fieldsManager, ITerrainHeightSampler heightSampler)
        {
            terrainFieldsManager = fieldsManager;
            this.heightSampler = heightSampler;
        }

        public bool canBuild(Float3 position, ObjectPattern pattern)
        {
            var intPosition = new Int2((int) position.x, (int) position.z);
            return terrainFieldsManager.canObjectBeAdded(intPosition, pattern);
        }

        public void construct(Float3 position, ObjectPattern pattern)
        {
            var intPosition = new Int2((int) position.x, (int) position.z);
            terrainFieldsManager.addObject(intPosition, pattern);
        }

        public void deconstruct(Float3 position, ObjectPattern pattern)
        {
            var intPosition = new Int2((int) position.x, (int) position.z);
            terrainFieldsManager.removeObject(intPosition, pattern);
        }

        public void setFieldType(Float3 position, TerrainFieldType newType)
        {
            var intPosition = new Int2((int) position.x, (int) position.z);
            terrainFieldsManager.changeFieldType(intPosition, newType);
        }

        public Float3? calculateClosestAvailablePosition(Float3 position, ObjectPattern pattern)
        {
            var intPosition = new Int2((int) position.x, (int) position.z);
            var calculatedPosition = terrainFieldsManager.calculateClosestAvailablePosition(intPosition, pattern);
            if (calculatedPosition != null)
            {
                var uncorrectedPosition =
                    new Float3(calculatedPosition.Value.x, position.y, calculatedPosition.Value.y);
                var correctedPosition = new Float3(uncorrectedPosition.x,
                    heightSampler.sampleHeight(uncorrectedPosition), uncorrectedPosition.z);
                return correctedPosition;
            }

            return null;
        }

        public TerrainFieldType getFieldType(Float3 location)
        {
            return terrainFieldsManager.getFieldType(new Int2((int) location.x, (int) location.z));
        }
    }
}