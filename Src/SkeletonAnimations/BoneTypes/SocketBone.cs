﻿using System;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.BoneTypes
{
    [Serializable]
    public class SocketBone : Bone
    {
        public int index;
        public float rotation;

        public SocketBone(int index, Float3 position, float rotation) : base(position)
        {
            this.index = index;
            this.position = position;
            this.rotation = rotation;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as SocketBone;
            return bone != null &&
                   index == bone.index;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (index * 397) ^ rotation.GetHashCode();
            }
        }

        public override string ToString()
        {
            return "SocketBone index: " + index + " rotation:" + rotation + " position: " + position;
        }

        public override string getName()
        {
            return "Socket Bone " + index;
        }
    }
}