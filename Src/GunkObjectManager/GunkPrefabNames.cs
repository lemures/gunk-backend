using System;
using System.Collections.Generic;
using System.Text;
using Gunk_Scripts.GunkObjects;

namespace Gunk_Scripts.GunkComponents
{
    public static class GunkPrefabNames
    {
        private static readonly Dictionary<string, GunkObjectType> typesDictionary =
            new Dictionary<string, GunkObjectType>();

        static GunkPrefabNames()
        {
            foreach (GunkObjectType type in Enum.GetValues(typeof(GunkObjectType)))
                typesDictionary.Add(getName(type), type);
        }

        public static string getName(GunkObjectType type)
        {
            return insertSpaceBeforeUpperCase(type.ToString());
        }

        public static GunkObjectType getType(string prefabName)
        {
            if (!typesDictionary.ContainsKey(prefabName))
                throw new ArgumentException("this prefabName(" + prefabName + ") is not valid");

            return typesDictionary[prefabName];
        }

        private static string insertSpaceBeforeUpperCase(string str)
        {
            var sb = new StringBuilder();
            var previousChar = char.MinValue;

            foreach (var c in str)
            {
                if (char.IsUpper(c))
                    if (sb.Length != 0 && previousChar != ' ')
                        sb.Append(' ');

                sb.Append(c);

                previousChar = c;
            }

            return sb.ToString();
        }
    }
}