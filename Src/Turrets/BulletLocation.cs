using Src.VectorTypes;

namespace Src.Turrets
{
    public class BulletLocation
    {
        public Float3 position;

        public Float3 directoryNormal;

        public BulletLocation(Float3 position, Float3 directoryNormal)
        {
            this.position = position;
            this.directoryNormal = directoryNormal;
        }
    }
}