﻿using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkTransform : IGunkComponent
    {
        int id { get; }
        Float3 position { get; set; }
    }
}