using Src.DataStructures;
using Src.Maps;
using Src.VectorTypes;

namespace Gunk_Scripts
{
    public interface IClosestResourceFinder
    {
        void addObject(int id, Float2 position, ResourceType resourceType);
        void removeObject(int id);

        FoundObject findClosestObject(Float2 position, ResourceType desiredType,
            float maxDistance);

        FoundObject findClosestObject(Float2 position, float maxDistance);

        void updateObjectPosition(int id, Float2 newPosition);
    }
}