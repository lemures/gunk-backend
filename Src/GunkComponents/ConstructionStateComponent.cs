using System;
using Src.Turrets;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class ConstructionStateComponent : GunkComponent, IConstructionState, IEnergyUser, IUpdateCallbackReceiver
    {
        private bool constructionReported;
        private IConstructionState currentState;

        private float substracted;

        public ConstructionStateComponent(IGunkObject parent) : base(parent)
        {
            currentState = new UnderConstructionImmediatelyFinished();
        }

        public bool constructionFinished => currentState.constructionFinished;
        public float constructionProgress => currentState.constructionProgress;

        public float calculateEnergyNeeds()
        {
            return (currentState as IEnergyUser)?.calculateEnergyNeeds() ?? 0;
        }

        public void chargeEnergy(float amount)
        {
            parent.addLog(this,
                "construction charged with energy with: " + PrettyPrinter.reformatFloat(amount) + " energy");
            (currentState as IEnergyUser)?.chargeEnergy(amount);
            checkConstructionFinished();
        }

        public void update(float deltaTime)
        {
            checkConstructionFinished();

            var health = parent.getComponent<Health>();
            if (health == null) return;
            health.heal(substracted);
            var toSubstract = health.maxValue * 0.9f * (1.0f - constructionProgress);
            health.injure(toSubstract);
            substracted = toSubstract;
        }

        private void checkConstructionFinished()
        {
            if (currentState.constructionFinished && !constructionReported)
            {
                parent.addLog(this, "reported construction finished");
                constructionReported = true;
                parent.reportConstructionFinished();
            }
        }

        public void changeState(IConstructionState newState)
        {
            parent.addLog(this, "new construction state: " + newState.GetType().Name);
            currentState = newState;
        }

        public override void destroy()
        {
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ConstructionStateComponent))
                .addVariable(constructionFinished, nameof(constructionFinished))
                .addVariable(currentState, nameof(currentState))
                .print();
        }
    }
}