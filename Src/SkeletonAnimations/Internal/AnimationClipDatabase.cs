using System;
using System.Collections.Generic;
using Src.Utils;

namespace Src.SkeletonAnimations.Internal
{
    public class AnimationClipDatabase : IAnimationClipDatabase
    {
        private readonly SortedDictionary<string, SkeletonAnimationClip> allLoadedClips =
            new SortedDictionary<string, SkeletonAnimationClip>();

        private readonly List<string> allAvailableClips = NeededClipsFinder.getAllNeededScripts();
        private readonly string clipsDatabaseCategory = "animationClips";

        private IAssetManager assetManager;
        
        public AnimationClipDatabase(IAssetManager assetManager)
        {
            this.assetManager = assetManager;
        }
        
        private void validateClipNameIsAvailable(string name)
        {
            if (!allAvailableClips.Contains(name)) throw new ClipNotFound(name);
        }

        private void createEmptyClipIfNecessary(string name)
        {
            if (allLoadedClips.ContainsKey(name)) return;
            var newClip = new SkeletonAnimationClip(name);
            allLoadedClips.Add(name, newClip);
            assetManager.saveObject(allLoadedClips[name], name, clipsDatabaseCategory);
        }

        private void loadFromDiskIfAvailable(string name)
        {
            if (allLoadedClips.ContainsKey(name)) return;
            var savedClip =
                assetManager.getObjectSavedCopy(name, clipsDatabaseCategory) as SkeletonAnimationClip;
            if (savedClip != null) allLoadedClips.Add(name, savedClip);
        }

        public SkeletonAnimationClip getClip(string name)
        {
            validateClipNameIsAvailable(name);
            loadFromDiskIfAvailable(name);
            createEmptyClipIfNecessary(name);
            return allLoadedClips[name];
        }

        public void saveClipChangesToDisk(string name)
        {
            if (name == "") return;
            validateClipNameIsAvailable(name);
            createEmptyClipIfNecessary(name);
            assetManager.saveObject(allLoadedClips[name], name, clipsDatabaseCategory);
        }

        private class ClipNotFound : Exception
        {
            public ClipNotFound(string message) : base(message)
            {
            }
        }
    }
}