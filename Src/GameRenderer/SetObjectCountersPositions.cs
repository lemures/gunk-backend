using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class SetObjectCountersPositions : ExtraObjectsChange
    {
        private readonly Float3 newPosition;

        private readonly int objectId;

        public SetObjectCountersPositions(int objectId, Float3 newPosition)
        {
            this.objectId = objectId;
            this.newPosition = newPosition;
        }

        protected bool Equals(SetObjectCountersPositions other)
        {
            return objectId == other.objectId && newPosition.Equals(other.newPosition);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((SetObjectCountersPositions) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (objectId * 397) ^ newPosition.GetHashCode();
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.countersRenderer.setObjectCountersPositions(objectId, newPosition);
        }
    }
}