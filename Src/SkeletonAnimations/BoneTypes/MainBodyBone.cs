﻿using System;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.BoneTypes
{
    [Serializable]
    public class MainBodyBone : Bone
    {
        public MainBodyBone() : base(Float3.zero)
        {
        }

        public override bool needsAssignedComponent()
        {
            return true;
        }

        public override string ToString()
        {
            return "MainBodyBone position: " + position;
        }

        public override string getName()
        {
            return "MainBodyBone";
        }
    }
}