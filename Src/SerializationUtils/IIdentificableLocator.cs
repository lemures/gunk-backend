using System;

namespace Src.SerializationUtils
{
    public interface IIdentifiableLocator
    {
        object getObjectWithIdentity(int id);
    }
}