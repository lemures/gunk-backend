using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class UpdateFogOfWarPosition : ExtraObjectsChange
    {
        public int id;
        public Float3 position;

        public UpdateFogOfWarPosition(int id, Float3 position)
        {
            this.id = id;
            this.position = position;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.fogOfWarObjectsCollection.updatePosition(id, position);
        }
    }
}