using System;
using Gunk_Scripts.GunkComponents;
using Src.Libraries;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;
using Src.VectorTypes;

namespace Src.Turrets
{
    [NeedsClip(idleClipName)]
    [NeedsClip(movingTowardsTargetClipName)]
    public class SimpleTurretRotator : GunkComponent, ITurretAnimationSystem
    {
        private const string idleClipName = "SingleNozzleTower Idle";
        private const string movingTowardsTargetClipName = "SingleNozzleTower moving towards target";

        static SimpleTurretRotator()
        {
            makeIdleClip();
            makeManufactureClip();
        }

        public SimpleTurretRotator(IGunkObject parent) : base(parent)
        {
            parent.animator.playClip(idleClipName);
        }

        public void rotateTowardsTarget(Float3 targetPosition)
        {
            if (parent.animator.currentClip == idleClipName)
                parent.animator.playClip(movingTowardsTargetClipName, calculateManufactureClipArgument(targetPosition));
            else
                parent.animator.changeClipArgument(calculateManufactureClipArgument(targetPosition));
        }

        public void cancelTarget()
        {
            parent.animator.playClip(idleClipName);
        }

        public bool facesTarget()
        {
            return parent.animator.isCurrentClipLoaded() && parent.animator.currentClip == movingTowardsTargetClipName;
        }

        private float calculateManufactureClipArgument(Float3 targetPosition)
        {
            var myPosition = parent.transform.position;
            var difference = targetPosition - myPosition;
            if (Float3.Distance(targetPosition, myPosition) < 0.001f) return 0;
            var sin = -difference.x / Float3.Distance(targetPosition, myPosition);
            return -0.5f + (difference.z < 0 ? 0 : 0.5f) +
                   Math.Sign(difference.z) * (float) (Math.Asin(sin) / (2 * Math.PI));
        }

        private static void makeIdleClip()
        {
            /*
            var database = new AnimationClipDatabase(new DiskResourcesManager());
            var clip = database.getClip(idleClipName);
            var turretBone = new BasicBone(0, 0, Float3.zero, null);
            clip.addOrReplaceCurve(SkeletonData.getBoneId(turretBone),
                SkeletonAnimationClip.BoneAnimationParameter.XRotation, new ConstantValueCurve(0));
            database.saveClipChangesToDisk(idleClipName);
            */
        }

        private static void makeManufactureClip()
        {
            /*
            var database = new AnimationClipDatabase(new DiskResourcesManager());
            var clip = database.getClip(movingTowardsTargetClipName);
            var turretBone = new BasicBone(0, 0, Float3.zero, null);
            clip.addOrReplaceCurve(SkeletonData.getBoneId(turretBone),
                SkeletonAnimationClip.BoneAnimationParameter.YRotation, new FirstArgumentDependentCurve());
            database.saveClipChangesToDisk(movingTowardsTargetClipName);
            */
        }

        public override void destroy()
        {
        }
    }
}