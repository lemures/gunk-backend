using NUnit.Framework;
using Src.Logic;

namespace SrcTests.Logic
{
    public class DayHourSimulationTests
    {
        [Test]
        public void dayStartsAtDayTest()
        {
            var dayHour = 11.50f;
            var simulation = new DayHourSimulation(dayHour, 20, 11, 12, 13);
            Assert.AreEqual(simulation.getHour(), dayHour);
        }

        [Test]
        public void HourIsCorrectAtItsEnd()
        {
            var dayHour = 11.50f;
            var dayLength = 14.0f;
            var simulation = new DayHourSimulation(dayHour, 20, dayLength, 12, 13);

            simulation.update(dayLength);

            Assert.AreEqual(simulation.getHour(), dayHour);
        }

        [Test]
        public void HourIsCorrectAtBeginningOfNight()
        {
            var dayHour = 11.50f;
            var dayLength = 14.0f;
            var nightHour = 20.0f;
            var transitionLength = 15.0f;
            var simulation = new DayHourSimulation(dayHour, nightHour, dayLength, 12, transitionLength);

            simulation.update(dayLength);
            simulation.update(transitionLength);

            Assert.AreEqual(simulation.getHour(), nightHour);
        }

        [Test]
        public void HourIsCorrectAtMiddleOfTransition()
        {
            var dayHour = 11.50f;
            var dayLength = 14.0f;
            var nightHour = 20.0f;
            var transitionLength = 15.0f;
            var simulation = new DayHourSimulation(dayHour, nightHour, dayLength, 12, transitionLength);

            simulation.update(dayLength);
            simulation.update(transitionLength / 2.0f);

            Assert.AreEqual(simulation.getHour(), (nightHour + dayHour) / 2.0f);
        }
    }
}