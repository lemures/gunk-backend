using Gunk_Scripts.GunkComponents;
using Src.Maps;
using Src.SerializationUtils;
using Src.VectorTypes;

namespace Src.Turrets
{
    public class ClosestCorruptedAreaTargetLocator : ITurretTargetLocator
    {
        private readonly IGunkObject gunkObject;
        private readonly CorruptedLand land;
        private readonly float maxRange;
        private readonly int damageRange;

        public ClosestCorruptedAreaTargetLocator(IGunkObject gunkObject, int damageRange, float maxRange = 15.0f)
        {
            land = gunkObject.objectManager.corruptedLand;
            this.maxRange = maxRange;
            this.gunkObject = gunkObject;
            this.damageRange = damageRange;
        }

        public IDemagableTarget getTarget()
        {
            var currentPosition = gunkObject.transform.position;
            var found = land.getClosestCorruptedField(new Int2((int) currentPosition.x, (int) currentPosition.z),
                (int) maxRange);
            if (found == null) return null;
            var targetPosition = new Float3(found.Value.x + 0.5f, 0, found.Value.y + 0.5f);
            targetPosition.y = gunkObject.objectManager.map.heightSampler.sampleHeight(targetPosition);
            return new CorruptedAreaTarget(targetPosition, damageRange, gunkObject.objectManager.corruptedLand);
        }
    }
}