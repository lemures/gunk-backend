using System;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Src.Utils;

namespace Src.SkeletonAnimations.CurvesTypes
{   
    [Serializable]
    [Guid("0ad17b4b-8b8f-4b14-8bd1-a203ee9f667b")]
    public class ConstantValueCurve : IAnimationCurve
    {
        [JsonProperty]
        private readonly float value; 

        public ConstantValueCurve(float value)
        {
            this.value = value;
        }

        public float evaluate(float time, float argument)
        {
            return value;
        }
    }
}