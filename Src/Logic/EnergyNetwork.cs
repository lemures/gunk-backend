﻿using System;
using System.Collections.Generic;
using System.IO;
using Src.Utils;

namespace Src.Logic
{
    public class EnergyNetwork
    {
        private static void checkObjectsDataValid(List<EnergyObjectData> objectsData)
        {
            if (objectsData == null) throw new Exception("objectsData cannot be null");

            if (!objectsData.TrueForAll(x => x != null)) throw new Exception("particular object data cannot be null");

            if (!objectsData.TrueForAll(x => x.energyNeeds >= 0))
                throw new InvalidDataException("requested energy must be non-negative number");

            if (!objectsData.TrueForAll(x => x.energyProducedThisTick >= 0))
                throw new InvalidDataException("energy produced must be non-negative number");

            var allIds = new HashSet<int>();
            objectsData.ForEach(x => allIds.Add(x.objectId));
            if (objectsData.Count != allIds.Count)
                throw new InvalidDataException("there are two objects with the same Id");
        }

        private static void checkSetDivision(List<List<int>> objectsDividedIntoSets)
        {
            var allIds = new HashSet<int>();
            objectsDividedIntoSets.ForEach(x => x.ForEach(y => allIds.Add(y)));
            var totalIdCountInSets = 0;
            objectsDividedIntoSets.ForEach(x => totalIdCountInSets += x.Count);
            if (totalIdCountInSets != allIds.Count)
                throw new InvalidDataException("there are two objects with the same Id");
        }

        public List<EnergyObjectSimulationResults> simulateSingleTick(List<EnergyObjectData> objectsData,
             List<List<int>> objectsDividedIntoSets)
        {
            checkObjectsDataValid(objectsData);
            checkSetDivision(objectsDividedIntoSets);

            var objectSimulationResults = new List<EnergyObjectSimulationResults>();

            var objectDataById = new SortedDictionary<int, EnergyObjectData>();
            objectsData.ForEach(x => objectDataById.Add(x.objectId, x));

            foreach (var set in objectsDividedIntoSets)
            {
                var totalNeeds = 0.0f;
                var totalProduction = 0.0f;
                set.ForEach(x => totalProduction += objectDataById[x].energyProducedThisTick);
                set.ForEach(x => totalNeeds += objectDataById[x].energyNeeds);

                //TODO construction of objects with their energy needs possibly close to zero leads to infinite construction time so this algorithm needs certain upgrades
                var partOfNeedsGranted = totalNeeds > 0.0f ? Math.Min(1.0f, totalProduction / totalNeeds) : 0;
                foreach (var myObject in set)
                {
                    var energyGranted = objectDataById[myObject].energyNeeds * partOfNeedsGranted;
                    objectSimulationResults.Add(new EnergyObjectSimulationResults(myObject, energyGranted));
                }
            }

            return objectSimulationResults;
        }

        public class EnergyObjectData
        {
            public readonly int objectId;

            /// <summary>
            ///     object is guaranteed not to receive more than that amount
            /// </summary>
            public float energyNeeds;

            public float energyProducedThisTick;

            public EnergyObjectData(int objectId, float energyProducedThisTick, float energyNeeds)
            {
                this.objectId = objectId;
                this.energyProducedThisTick = energyProducedThisTick;
                this.energyNeeds = energyNeeds;
            }
        }

        public class EnergyObjectSimulationResults
        {
            public float energyProvided;
            public int objectId;

            public EnergyObjectSimulationResults(int objectId, float energyProvided)
            {
                this.objectId = objectId;
                this.energyProvided = energyProvided;
            }

            public override string ToString()
            {
                return new VariablesReportBuilder(this)
                    .addVariable(energyProvided, nameof(energyProvided))
                    .addVariable(objectId, nameof(objectId))
                    .print();
            }
        }
    }
}