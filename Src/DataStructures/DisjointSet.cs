﻿using System;
using System.Collections.Generic;

namespace Src.DataStructures
{
    [Serializable]
    public class DisjointSet
    {
        private readonly List<Edge> allConnections = new List<Edge>();

        //memory vs time performance trade-off
        private readonly List<int> rep = new List<int>();

        private void enlargeArrays(int newSize)
        {
            while (rep.Count <= newSize) rep.Add(rep.Count);
        }

        private int find(int a)
        {
            enlargeArrays(a);
            return rep[a] == a ? a : rep[a] = find(rep[a]);
        }

        /// <summary>
        ///     marks that a and b are in the same set
        /// </summary>
        public void union(int a, int b)
        {
            allConnections.Add(new Edge(a, b));
            rep[find(a)] = rep[find(b)];
        }

        /// <summary>
        ///     returns true if a and b are in the same set
        /// </summary>
        public bool oneSet(int a, int b)
        {
            return find(a) == find(b);
        }

        /// <summary>
        ///     removes all information about connections of an object
        /// </summary>
        public void removeConnectionsWithElement(int a)
        {
            //resetting rep array
            allConnections.ForEach(x => rep[x.x] = x.x);
            allConnections.ForEach(x => rep[x.y] = x.y);
            //removing edges
            allConnections.RemoveAll(x => x.x == a || x.y == a);
            //reconnecting all connections
            var temp = new List<Edge>(allConnections);
            allConnections.Clear();
            temp.ForEach(x => union(x.x, x.y));
        }

        /// <summary>
        ///     based on all known connections groups numbers into sets, elements should not include duplicates
        /// </summary>
        public List<List<int>> groupIntoSets(List<int> elements)
        {
            if (elements == null) throw new ArgumentNullException(nameof(elements));

            var representativeDictionary = new SortedDictionary<int, List<int>>();
            var representatives = elements.FindAll(x => x >= rep.Count || find(x) == x);

            representatives.ForEach(x => representativeDictionary.Add(x, new List<int>()));
            elements.ForEach(x => representativeDictionary[x < rep.Count ? find(x) : x].Add(x));
            var result = new List<List<int>>();
            representatives.ForEach(x => result.Add(representativeDictionary[x]));

            return result;
        }
    }
}