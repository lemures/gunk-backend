using System.Collections.Generic;
using Src.Libraries;
using Src.Maps;
using Src.Renderers;

namespace Src.GameRenderer
{
    public class AddCorruptionChanges : ExtraObjectsChange
    {
        private readonly List<CorruptedLand.FieldChange> changes;

        public AddCorruptionChanges(List<CorruptedLand.FieldChange> changes)
        {
            this.changes = changes;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.corruptionRenderer.addChanges(changes);
        }
    }
}