using System;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    [Serializable]
    public class GunkLog
    {
        public string author;
        public string message;
        public float time;

        public GunkLog(float time, string author, string message)
        {
            if (author == null) throw new ArgumentNullException(nameof(author));
            if (message == null) throw new ArgumentNullException(nameof(message));

            this.time = time;
            this.author = author;
            this.message = message;
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(GunkLog))
                .addVariable(time, nameof(time))
                .addVariable(author, nameof(author))
                .addVariable(message, nameof(message))
                .print();
        }
    }
}