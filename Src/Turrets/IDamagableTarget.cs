using Src.SerializationUtils;
using Src.VectorTypes;

namespace Src.Turrets
{
    public interface IDemagableTarget: IIdentifiable
    {
        Float3 currentPosition { get; }
        void damage(float damage);
    }
}