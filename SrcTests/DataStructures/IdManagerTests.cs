﻿using NUnit.Framework;
using Src.DataStructures;
using Src.Utils;

namespace SrcTests.DataStructures
{
    public class IdManagerTests
    {
        [Test]
        public void getFreeIdReturnsGoodIDs()
        {
            var idManager = new IdManager();
            for (var i = 0; i < 1000; i++) Assert.AreEqual(i, idManager.getFreeId());
        }

        [Test]
        public void deregisteringSameIdTwiceProducesError()
        {
            var idManager = new IdManager();
            idManager.getFreeId();
            idManager.deregisterId(0);
            Assert.Catch(() => idManager.deregisterId(0));
        }

        [Test]
        public void deregisteringNonExistentIdProducesError()
        {
            var idManager = new IdManager();
            Assert.Catch(() => idManager.deregisterId(0));
        }

        [Test]
        public void deregisterAllIdsReallyFreesIDs()
        {
            var idManager = new IdManager();
            for (var i = 0; i < 1000; i++) idManager.getFreeId();

            idManager.deregisterAllIds();
            for (var i = 0; i < 1000; i++) Assert.AreEqual(i, idManager.getFreeId());
        }

        [Test]
        public void getMaxTakenIDWorks()
        {
            var idManager = new IdManager();
            for (var i = 0; i < 1000; i++) idManager.getFreeId();
            for (var i = 501; i < 1000; i++) idManager.deregisterId(i);

            Assert.AreEqual(500, idManager.getMaxTakenId());
        }

        [Test]
        public void getAllTakenIDsWorksCorrectly()
        {
            var idManager = new IdManager();
            for (var i = 0; i < 1000; i++) idManager.getFreeId();
            for (var i = 0; i < 1000; i += 2) idManager.deregisterId(i);
            var takenIDs = idManager.getAllUsedIDs();
            for (var i = 1; i < 1000; i += 2) Assert.Contains(i, takenIDs);
        }

        [Test]
        public void serializationTest()
        {
            var idManager = new IdManager();
            for (var i = 0; i < 100; i++) idManager.getFreeId();
            var idManagerClone = BinarySerializationUtils.cloneObject(idManager);
            var takenIDs = idManagerClone.getAllUsedIDs();
            for (var i = 0; i < 100; i++) Assert.Contains(i, takenIDs);
        }

        [Test]
        public void getMaxTakenIDOnEmptyManagerProducesError()
        {
            var idManager = new IdManager();
            Assert.AreEqual(-1, idManager.getMaxTakenId());
        }

        [Test]
        public void callingGetFreeIdDeregisterIdRepeatedlyDoesntProduceErrors()
        {
            var idManager = new IdManager();
            for (var i = 0; i < 100; i++)
            {
                var id = idManager.getFreeId();
                idManager.deregisterId(id);
            }
        }
    }
}