using System;

namespace Src.Utils
{
    public class StateMachine<T>
    {
        public StateMachine(State<T> startState)
        {
            currentState = startState;
            startState.setAsActive();
        }

        public State<T> currentState { get; private set; }

        public void changeState(State<T> newState)
        {
            if (newState == null) throw new NullReferenceException("new state cannot be null!");
            currentState.deactivate();
            currentState = newState;
            newState.setAsActive();
        }

        public void update(float deltaTime)
        {
            currentState.update(deltaTime);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(StateMachine<T>))
                .addVariable(currentState, nameof(currentState))
                .print();
        }
    }
}