﻿using System;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.BoneTypes
{
    [Serializable]
    public class BasicBoneOffset : Bone
    {
        public int limbNum;
        public int segmNum;

        public BasicBoneOffset(int segmNum, int limbNum, Float3 position) : base(position)
        {
            this.segmNum = segmNum;
            this.limbNum = limbNum;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as BasicBoneOffset;
            return bone != null &&
                   segmNum == bone.segmNum &&
                   limbNum == bone.limbNum;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (limbNum * 397) ^ segmNum;
            }
        }

        public override string ToString()
        {
            return "BasicBoneOffset segmNum: " + segmNum + " limbNum:" + limbNum + " position: " + position;
        }

        public override string getName()
        {
            return "BasicBoneOffset limb " + limbNum + " segment" + segmNum;
        }
    }
}