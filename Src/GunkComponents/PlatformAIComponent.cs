using System;
using System.Collections.Generic;
using Gunk_Scripts.GunkComponents;
using Src.GunkComponents;
using Src.Libraries;
using Src.Turrets;
using Src.Utils;
using Src.VectorTypes;


namespace Gunk_Scripts.GunkObjects
{
    public class PlatformAiComponent : GunkComponent, IUpdateCallbackReceiver
    {
        private const float TIME_BETWEEN_CONSTRUCTIONS = 2.0f;

        private readonly List<GunkObjectType> constructionQueue = new List<GunkObjectType>();
        private readonly PerManagerObject<RandomObjectFinder> closestPlatformFinder;
        private int? currentlyConstructedId;

        private readonly List<int> objectsMovingFromPlatform = new List<int>();
        private float timeSinceLastConstruction;

        public PlatformAiComponent(IGunkObject parent) : base(parent)
        {
            closestPlatformFinder =
                new PerManagerObject<RandomObjectFinder>("closestPlatformFinder", parent,
                    () => new RandomObjectFinder());
        }

        public bool isConstructing => currentlyConstructedId != null;

        public void update(float deltaTime)
        {
            validateCurrentlyConstructedId();

            updateProgressBar();

            if (!isConstructing)
                updateTimeSinceLastConstruction(deltaTime);

            if (canStartConstruction())
                startFirstAvailableConstruction();

            dealWithObjectsMovingAwayFromPlatform();
        }

        private void markPlatformAsConstructing()
        {
            var position = new Float2(parent.transform.position.x, parent.transform.position.z);
            closestPlatformFinder.getObject().addObject(parent.id, position.x, position.y, parent.faction);
        }

        private void markAsNoLongerConstructing()
        {
            closestPlatformFinder.getObject().removeObject(parent.id);
        }

        private void updateProgressBar()
        {
            var progress = getConstructionProgress();
            parent.getComponent<ProgressComponent>().setValue(progress);
            if (progress == 0)
                parent.getComponent<ProgressComponent>().hide();
            else
                parent.getComponent<ProgressComponent>().show();
        }

        private bool canStartConstruction()
        {
            return !isConstructing && timeSinceLastConstruction >= TIME_BETWEEN_CONSTRUCTIONS;
        }

        private void updateTimeSinceLastConstruction(float deltaTime)
        {
            timeSinceLastConstruction += deltaTime;
        }

        /// <summary>
        ///     checks if constructed object exists and is still under construction and corrects variables if not
        /// </summary>
        private void validateCurrentlyConstructedId()
        {
            if (currentlyConstructedId == null) return;

            if (parent.objectManager.getObjectWithId(currentlyConstructedId.Value) == null)
            {
                parent.addLog(this, "constructed object was apparently destroyed, aborting current construction");
                currentlyConstructedId = null;
                markAsNoLongerConstructing();
                timeSinceLastConstruction = 0.0f;
                return;
            }

            if (parent.objectManager.getObjectWithId(currentlyConstructedId.Value).isConstructed())
            {
                parent.addLog(this, "current object was successfully constructed");
                var destination = parent.transform.position + new Float3(0, 1, -2);
                parent.objectManager.getObjectWithId(currentlyConstructedId.Value).agent.destination = destination;
                objectsMovingFromPlatform.Add(currentlyConstructedId.Value);
                currentlyConstructedId = null;
                markAsNoLongerConstructing();
                timeSinceLastConstruction = 0.0f;
            }
        }

        private void dealWithObjectsMovingAwayFromPlatform()
        {
            objectsMovingFromPlatform.RemoveAll(x => parent.objectManager.getObjectWithId(x) == null);

            var toRemove = new List<int>();
            foreach (var objectMovingFromPlatformId in objectsMovingFromPlatform)
            {
                var movingObject = parent.objectManager.getObjectWithId(objectMovingFromPlatformId);
                if (movingObject.agent.remainingDistance < 0.001f)
                {
                    (movingObject as GunkObject).addNavmeshAgent();
                    movingObject.agent.destination =
                        movingObject.transform.position + new Float3(0, 0, -0.1f);
                    toRemove.Add(objectMovingFromPlatformId);
                }
            }

            toRemove.ForEach(x => objectsMovingFromPlatform.Remove(x));
        }

        private void startFirstAvailableConstruction()
        {
            if (!canStartConstruction()) throw new InvalidOperationException("cannot start construction!");

            if (constructionQueue.Count == 0) return;

            var type = constructionQueue[0];
            var myPosition = parent.transform.position;
            var myFaction = parent.faction;
            var spawnedPosition = myPosition + new Float3(0, 2, 0);

            parent.addLog(this, "starting construction of " + type);
            currentlyConstructedId =
                GunkObjectFactory.spawnObject(parent.objectManager, type, spawnedPosition, myFaction).id;
            var spawned = parent.objectManager.getObjectWithId(currentlyConstructedId.Value);
            spawned.replaceAgentWithFake();

            markPlatformAsConstructing();
        }

        public override void destroy()
        {
            if (isConstructing) markAsNoLongerConstructing();
        }

        public void addObjectToQueue(GunkObjectType type)
        {
            parent.addLog(this, "object " + type.GetType().Name + " was added to queue");
            constructionQueue.Add(type);
        }

        public float getConstructionEnergyNeeds()
        {
            validateCurrentlyConstructedId();
            if (!isConstructing) return 0;

            return parent.objectManager.getObjectWithId(currentlyConstructedId.Value).getEnergyNeeds();
        }

        private float getConstructionProgress()
        {
            if (!isConstructing) return 0;

            var constructed = parent.objectManager.getObjectWithId(currentlyConstructedId.Value);
            return constructed.constructionProgress;
        }

        public void chargeConstructedObject(float amount)
        {
            validateCurrentlyConstructedId();
            if (!isConstructing) return;

            parent.objectManager.getObjectWithId(currentlyConstructedId.Value).provideWithEnergy(amount);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(PlatformAiComponent))
                .addVariable(currentlyConstructedId, nameof(currentlyConstructedId))
                .addVariable(timeSinceLastConstruction, nameof(timeSinceLastConstruction))
                .addVariable(constructionQueue, nameof(constructionQueue))
                .print();
        }
    }
}