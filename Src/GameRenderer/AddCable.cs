using Src.DataStructures;
using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddCable : ExtraObjectsChange
    {
        private readonly Edge edge;
        private readonly Float3 endPosition;
        private readonly Float3 startPosition;

        public AddCable(Edge edge, Float3 startPosition, Float3 endPosition)
        {
            this.edge = edge;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.cableRenderer.addCable(edge, startPosition, endPosition);
        }
    }
}