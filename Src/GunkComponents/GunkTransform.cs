﻿using Src.GameRenderer;
using Src.Libraries;
using Src.Utils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public class GunkTransform : GunkComponent, IGunkTransform
    {
        private Float3 _rotation;
        private Float3 lastPosition;

        public GunkTransform(IGunkObject parent, Float3 position, int gameObjectId) : base(parent)
        {
            lastPosition = position;
            init(gameObjectId);
        }

        public int id { get; private set; }

        public Float3 position
        {
            get { return lastPosition; }
            set
            {
                parent.objectManager.addVisibleObjectsChange(new SetObjectCountersPositions(id, value));
                updatePosition();
            }
        }

        public Float3 rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value;
            }
        }

        public override void destroy()
        {
            parent.objectManager.addVisibleObjectsChange(new DeleteMainModel(id));
        }

        private void init(int gameObjectId)
        {
            id = gameObjectId;
            updatePosition(true);
        }

        public void reportPositionChangedExternally()
        {
            updatePosition();
        }

        private void updatePosition(bool forceUpdate = false)
        {
            parent.objectManager.addVisibleObjectsChange(new SetObjectCountersPositions(parent.id, lastPosition));
            parent.reportPositionChanged(lastPosition);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(this)
                .addVariable(id, "gameObject id")
                .addVariable(lastPosition, "position")
                .addVariable(_rotation, "rotation")
                .print();
        }
    }
}