﻿using NUnit.Framework;
using Src.DataStructures;
using Src.Utils;

namespace SrcTests.DataStructures
{
    public class IdMapTests
    {
        [Test]
        public void addManyObjectDoesNotProduceErrorsTest()
        {
            var idMap = new IdMap();
            for (var i = 0; i < 10000; i++)
                Assert.DoesNotThrow(() => idMap.addObject(i * 100), "adding object with id " + i * 100 + " failed");
        }

        [Test]
        public void removeNonExistingObjectProducesError()
        {
            var idMap = new IdMap();
            Assert.Catch(() => idMap.removeObject(12345));
        }

        [Test]
        public void removeSameObjectTwiceProducesError()
        {
            var idMap = new IdMap();
            idMap.addObject(123456);
            idMap.removeObject(123456);
            Assert.Catch(() => idMap.removeObject(123456));
        }

        [Test]
        public void removeObjectReallyRemovesObject()
        {
            var idMap = new IdMap();
            idMap.addObject(1234);
            idMap.removeObject(1234);
            idMap.addObject(12);
            idMap.addObject(1234);
            Assert.AreEqual(0, idMap.getMappedId(12));
            Assert.AreEqual(1, idMap.getMappedId(1234));
        }

        [Test]
        public void getMappedIdReturnsFirstFourNonNegativeNumbers()
        {
            var idMap = new IdMap();
            idMap.addObject(123);
            idMap.addObject(1234);
            idMap.addObject(12);
            idMap.addObject(1234567);
            idMap.removeObject(1234);
            Assert.AreEqual(0, idMap.getMappedId(123));
            Assert.AreEqual(2, idMap.getMappedId(12));
            Assert.AreEqual(3, idMap.getMappedId(1234567));
        }

        [Test]
        public void getAllObjectIdsWorks()
        {
            var idMap = new IdMap();
            idMap.addObject(123);
            idMap.addObject(1234);
            idMap.addObject(12345);

            var ids = idMap.getAllObjectIds();
            Assert.Contains(123, ids);
            Assert.Contains(1234, ids);
            Assert.Contains(12345, ids);
            Assert.AreEqual(3, ids.Count);
        }

        [Test]
        public void getAllMappedIdsWorks()
        {
            var idMap = new IdMap();
            idMap.addObject(123);
            idMap.addObject(1234);
            idMap.addObject(12345);

            var ids = idMap.getAllMappedIds();
            Assert.Contains(idMap.getMappedId(123), ids);
            Assert.Contains(idMap.getMappedId(1234), ids);
            Assert.Contains(idMap.getMappedId(12345), ids);
            Assert.AreEqual(3, ids.Count);
        }

        [Test]
        public void serializationWorks()
        {
            var idMap = new IdMap();
            idMap.addObject(12345);
            var clone = BinarySerializationUtils.cloneObject(idMap);
            Assert.DoesNotThrow(() => clone.removeObject(12345));
        }

        [Test]
        public void addRemoveObjectRepeatedlyTest()
        {
            var idMap = new IdMap();
            for (var i = 0; i < 100; i++)
            {
                idMap.addObject(-1000);
                idMap.removeObject(-1000);
            }
        }
    }
}