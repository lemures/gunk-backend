using FakeItEasy;
using NUnit.Framework;
using Src.GameRenderer;
using Src.SkeletonAnimations;
using Src.Utils;
using Src.VectorTypes;

namespace Src.Turrets.Tests.Editor
{
    public class MortarTests
    {
        private readonly Float3 barrelPosition = new Float3(1, 3, 5);
        private readonly Float3 targetPosition = new Float3(2, 4, 6);
        private readonly Float3 targetSecondPosition = new Float3(2, 5, 6);
        private readonly int bulletId = 69;
        private IDemagableTarget demagableTarget;
        private IBarrelPositionCalculator fakePositionCalculator;
        private IObjectsChangesReceiver objectsChangesReceiver;
        private MortarWeapon weapon;

        [SetUp]
        public void setUp()
        {
            fakePositionCalculator = A.Fake<IBarrelPositionCalculator>();
            A.CallTo(() => fakePositionCalculator.getBarrelBoneWorldPosition(A<int>.Ignored))
                .Returns(barrelPosition);
            objectsChangesReceiver = A.Fake<IObjectsChangesReceiver>();
            demagableTarget = A.Fake<IDemagableTarget>();
            A.CallTo(() => demagableTarget.currentPosition).Returns(targetPosition);
            var idGenerator = A.Fake<IUniqueIdGenerator>();
            A.CallTo(() => idGenerator.getId()).Returns(bulletId);
            weapon = new MortarWeapon(fakePositionCalculator, objectsChangesReceiver, idGenerator);
        }

        [Test]
        public void mortarBulletIsSpawnedWhenMortarIsFired()
        {
            weapon.fire(demagableTarget);
            A.CallTo(() =>
                objectsChangesReceiver.sendChange(
                    A<ExtraObjectsChange>.That.IsInstanceOf(typeof(AddMortarBullet)))).MustHaveHappened();
        }

        [Test]
        public void targetIsDamagedAfterSomeTime()
        {
            weapon.fire(demagableTarget);
            weapon.update(100.0f);
            A.CallTo(() => demagableTarget.damage(A<float>._)).MustHaveHappened();
        }

        [Test]
        public void targetIsDamagedInEveryLongUpdateCall()
        {
            for (var i = 0; i < 10; i++)
            {
                weapon.fire(demagableTarget);
                weapon.update(10.0f);
            }
            A.CallTo(() => demagableTarget.damage(A<float>._)).MustHaveHappenedANumberOfTimesMatching(x => x == 10);
        }

        [Test]
        public void weaponCannotBeFiredAtOnceAfterItFires()
        {
            weapon.fire(demagableTarget);
            weapon.update(0.1f);
            Assert.IsFalse(weapon.isReady);
        }

        [Test]
        public void weaponIsReadyAfterSomeTime()
        {
            weapon.fire(demagableTarget);
            weapon.update(10f);
            Assert.IsTrue(weapon.isReady);
        }
    }
}