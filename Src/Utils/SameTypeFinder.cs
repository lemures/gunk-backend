using System;
using System.Collections.Generic;
using System.Globalization;
using Src.DataStructures;
using Src.VectorTypes;

namespace Gunk_Scripts
{
    public class SameTypeFinder<TObjectType> where TObjectType : struct, IConvertible
    {
        private readonly List<ClosestObjectFinder> closestResourceFinders = new List<ClosestObjectFinder>();
        private readonly SortedDictionary<int, int> types = new SortedDictionary<int, int>();

        public SameTypeFinder(int mapWidth, int mapHeight)
        {
            var count = Enum.GetValues(typeof(TObjectType)).Length;
            for (var i = 0; i < count; i++) closestResourceFinders.Add(new ClosestObjectFinder(mapWidth, mapHeight));
        }

        public void addObject(int id, Float2 position, TObjectType type)
        {
            closestResourceFinders[type.ToInt32(CultureInfo.CurrentCulture)].addObject(id, position);
            types.Add(id, type.ToInt32(CultureInfo.CurrentCulture));
        }

        public void removeObject(int id)
        {
            closestResourceFinders[types[id]].removeObject(id);
            types.Remove(id);
        }

        public FoundObject findClosestObject(Float2 position, TObjectType desiredType,
            float maxDistance)
        {
            return closestResourceFinders[desiredType.ToInt32(CultureInfo.CurrentCulture)]
                .findObject(position, maxDistance);
        }

        public void updateObjectPosition(int id, Float2 newPosition)
        {
            closestResourceFinders[types[id]].updatePosition(id, newPosition);
        }
    }
}