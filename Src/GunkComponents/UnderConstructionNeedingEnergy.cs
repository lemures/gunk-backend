using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class UnderConstructionNeedingEnergy : IEnergyUser, IConstructionState
    {
        private Counter energyToBuild;
        private readonly float minConstructionTime;

        public UnderConstructionNeedingEnergy(float energyNeededToBuild, float minConstructionTime = 1f)
        {
            energyToBuild = new Counter(0, energyNeededToBuild);
            this.minConstructionTime = minConstructionTime;
        }

        public bool constructionFinished => energyToBuild.isFull();
        public float constructionProgress => energyToBuild.currentValue / energyToBuild.maxValue;

        public float calculateEnergyNeeds()
        {
            return energyToBuild.maxValue / minConstructionTime;
        }

        public void chargeEnergy(float amount)
        {
            energyToBuild.increaseValue(amount);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(UnderConstructionNeedingEnergy))
                .addVariable(energyToBuild, nameof(energyToBuild))
                .addVariable(constructionFinished, nameof(constructionFinished))
                .addVariable(minConstructionTime, nameof(minConstructionTime))
                .print();
        }
    }
}