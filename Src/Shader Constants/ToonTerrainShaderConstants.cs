﻿namespace Src.Shader_Constants
{
    public static class ToonTerrainShaderConstants
    {
        public static string shaderName = "Toon/Terrain";
        public static string grass1Property = "grass1";
        public static string grass2Property = "grass2";
        public static string stone1Property = "stone1";
        public static string stone2Property = "stone2";
        public static string cliff1Property = "cliff1";
        public static string cliff2Property = "cliff2";
        public static string sand1Property = "sand1";
        public static string sand2Property = "sand2";
        public static string splatProperty = "Splat_map";
        public static string normalsProperty = "Terrain_normals";
    }
}