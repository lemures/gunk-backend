using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class UpdateMortarBullet : ExtraObjectsChange
    {
        protected bool Equals(UpdateMortarBullet other)
        {
            return newStartPosition.Equals(other.newStartPosition) && newEndPosition.Equals(other.newEndPosition) && id == other.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((UpdateMortarBullet) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = newStartPosition.GetHashCode();
                hashCode = (hashCode * 397) ^ newEndPosition.GetHashCode();
                hashCode = (hashCode * 397) ^ id;
                return hashCode;
            }
        }

        private Float3 newStartPosition;

        private Float3 newEndPosition;

        private int id;

        public UpdateMortarBullet(Float3 newStartPosition, Float3 newEndPosition, int id)
        {
            this.newStartPosition = newStartPosition;
            this.newEndPosition = newEndPosition;
            this.id = id;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.generalPurposeLineRenderer.updatePosition(id, newStartPosition, newEndPosition);
        }
    }
}