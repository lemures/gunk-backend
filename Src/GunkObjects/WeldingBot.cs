﻿using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class WeldingBot : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.WeldingBot);
        private static readonly float maxHealth = 1000;

        public WeldingBot(IGunkObjectManagerInternalData objectManager, Float3 position, Faction faction) : base(objectManager,
            prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addNavmeshAgent();
            addCuboidCollider(new Float3(2, 2, 2));
            selectionPriority = SelectionPriorityUtils.getWeldingBotSelectionPriority();
            finishAddingComponents();
        }
    }
}