using Src.Maps;
using Src.SerializationUtils;
using Src.VectorTypes;

namespace Src.Turrets
{
    public class CorruptedFieldTarget : IDemagableTarget
    {
        private readonly CorruptedLand corruptedLand;

        public CorruptedFieldTarget(Float3 currentPosition, CorruptedLand corruptedLand)
        {
            this.currentPosition = currentPosition;
            this.corruptedLand = corruptedLand;
        }

        public Float3 currentPosition { get; }

        public void damage(float damage)
        {
            var position = new Int2((int) currentPosition.x, (int) currentPosition.z);
            corruptedLand.damageField(position, damage);
        }

        public long uniqueId { get; }
    }
}