using System;
using Gunk_Scripts.GunkComponents;
using Src.GameRenderer;
using Src.SkeletonAnimations;

namespace Src.Turrets
{
    public class LaserWeapon : IWeapon, IUpdateCallbackReceiver
    {
        private readonly IBarrelPositionCalculator barrelPositionCalculator;
        private readonly IObjectsChangesReceiver changesReceiver;

        private IDemagableTarget currentTarget;

        private bool isLaserSpawned;
        private readonly float laserLifespan = 0.1f;
        private readonly float reloadTime = 0.1f;
        private float timeIdle;
        private float timeLaserExists;

        public LaserWeapon(IBarrelPositionCalculator barrelPositionCalculator, IObjectsChangesReceiver changesReceiver)
        {
            this.changesReceiver = changesReceiver;
            this.barrelPositionCalculator = barrelPositionCalculator;
            //so that weapon is immediately ready
            timeIdle = 100.0f;
        }

        public void fire(IDemagableTarget target)
        {
            if (target == null) throw new NullReferenceException("target cannot be null");

            if (!isReady)
            {
                return;
            }
            //just in case
            currentTarget = target;
            addLaser();
            target.damage(10f);
        }

        public bool isReady => !isLaserSpawned && reloadTime < timeIdle;

        public void stop()
        {
            removeLaser();
        }

        public void update(float deltaTime)
        {
            if (isLaserSpawned)
            {
                var timeToAdd = Math.Min(laserLifespan - timeLaserExists, deltaTime);
                timeLaserExists += timeToAdd;
                deltaTime -= timeToAdd;
                removeOldLaserIfNecessary();
                updateLaserPositionIfPossible();
            }

            timeIdle += deltaTime;
        }

        private void removeLaser()
        {
            if (isLaserSpawned)
            {
                changesReceiver.sendChange(new RemoveLaser(GetHashCode()));
                isLaserSpawned = false;
                timeIdle = 0.0f;
            }
        }

        private void addLaser()
        {
            var change = new AddLaser(GetHashCode(), barrelPositionCalculator.getBarrelBoneWorldPosition(0),
                currentTarget.currentPosition);
            changesReceiver.sendChange(change);
            isLaserSpawned = true;
            timeLaserExists = 0.0f;
        }

        private void removeOldLaserIfNecessary()
        {
            if (isLaserSpawned && laserLifespan <= timeLaserExists) removeLaser();
        }

        private void updateLaserPositionIfPossible()
        {
            if (isLaserSpawned)
            {
                changesReceiver.sendChange(new RemoveLaser(GetHashCode()));
                var change = new AddLaser(GetHashCode(), barrelPositionCalculator.getBarrelBoneWorldPosition(0),
                    currentTarget.currentPosition);
                changesReceiver.sendChange(change);
            }
        }
    }
}