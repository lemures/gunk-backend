using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using Gunk_Scripts.Renderers;
using Src.DataStructures;
using Src.GameRenderer;
using Src.GunkObjectManager;
using Src.Logic;
using Src.Maps;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Internal;
using Src.Utils;
using Src.VectorTypes;

namespace Src.GunkComponents
{
    public class GunkObjectManagerInternalData : IGunkObjectManagerInternalData
    {
        private static GunkObjectManagerInternalData activeManager;

        private readonly SortedDictionary<int, IGunkObject> allObjects = new SortedDictionary<int, IGunkObject>();

        public readonly ConnectablesBuildingsModule connectablesBuildingsModule;

        private readonly EnergyNetwork energyNetwork = new EnergyNetwork();

        private readonly IdManager idManager = new IdManager();

        private readonly UpdateCaller<IGunkObject> updateCaller;

        //private readonly Dictionary<int, ComponentDebugger> debuggers = new Dictionary<int, ComponentDebugger>();

        private readonly IExtraObjectsRenderer extraObjectsRenderer;

        public Players players = new Players();

        private IProfiler profiler;

        private float totalTime;

        //private IGunkLogger logger;

        private IAnimatorsManager animatorsManager;
        
        public GunkObjectManagerInternalData(Gunk_Scripts.GunkComponents.GunkObjectManager parent,
            IExtraObjectsRenderer extraObjectsRenderer, 
            IMap map, 
            IProfiler profiler, 
            IGunkLogger logger, 
            IAssetManager assetManager, 
            IAnimatorsManager animatorsManager)
        {
            this.animatorsManager = animatorsManager;
            this.assetManager = assetManager;
            updateCaller = new UpdateCaller<IGunkObject>(x => !x.isDestroyed(), profiler);
            this.profiler = profiler;
            activeManager = this;
            this.map = map;
            this.extraObjectsRenderer = extraObjectsRenderer;
            this.mapHeight = map.mapHeight;
            this.mapWidth = map.mapWidth;
            this.parent = parent;

            var cableRenderer = new CableChangesReceiver(this);
            connectablesBuildingsModule = new ConnectablesBuildingsModule(cableRenderer);
            closestResourceFinder = new ClosestResourceFinder(mapWidth, mapHeight);

//            agentProducer = new NavmeshAgentProducer(gameObjectManager);
            otherVariables = new Dictionary<string, object>();

            corruptedLand = new CorruptedLand(mapHeight, mapWidth);
            
            corruptedLand.infectField(new Int2(170, 170));


            foreach (var xPos in Enumerable.Range(0, mapWidth))
            foreach (var yPos in Enumerable.Range(0, mapHeight))
                if (map.placedObjectsManager.getFieldType(new Float3(xPos + 0.5f, 0, yPos + 0.5f)) !=
                    TerrainFieldType.Ground)
                    corruptedLand.markFieldAsNonInfectable(new Int2(xPos, yPos));
        }

        public IClosestResourceFinder closestResourceFinder { get; }

        public void addResources(Faction player, GatheredResources resources)
        {
            players.addResources(player, resources);
        }

        public Dictionary<string, object> otherVariables { get; }

        public IGunkObjectManager parent { get; }

        public IConnectablesNetwork connectablesNetwork => connectablesBuildingsModule;

        public CorruptedLand corruptedLand { get; }

        public INavigationAgentProducer agentProducer { get; private set; }

        private IAssetManager assetManager;

        public int mapHeight { get; }
        public int mapWidth { get; }

        public IMap map { get; }

        public void setSpecialFieldType(Float3 position, TerrainFieldType type)
        {
            map.placedObjectsManager.setFieldType(position, type);
        }

        public void lockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern pattern)
        {
            var position = gunkObject.transform.position;
            if (!map.placedObjectsManager.canBuild(position, pattern))
                throw new InvalidOperationException(
                    "you tried to instantiate building in a place that is not free");

            map.placedObjectsManager.construct(position, pattern);
        }

        public void unlockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern pattern)
        {
            var position = gunkObject.transform.position;
            map.placedObjectsManager.deconstruct(position, pattern);
        }

        public IObjectsChangesReceiver objectsChangesReceiver => extraObjectsRenderer;

        public void addVisibleObjectsChange(ExtraObjectsChange change)
        {
            extraObjectsRenderer.sendChange(change);
        }

        public void addComponent(int id, IGunkComponent component)
        {
            //debuggers[id].addComponent(component);
        }

        public void removeComponent(int id, IGunkComponent component)
        {
            //debuggers[id].removeComponent(component);
        }

        public IGunkTransform instantiatePrefab(IGunkObject gunkObject, string prefabName, Float3 positions,
            Float3 eulerRotation)
        { 
            extraObjectsRenderer.prefabRenderer.instantiatePrefab(gunkObject.id, prefabName, positions, eulerRotation);
            
            //debuggers.Add(gunkObject.id, gameObjectManager.addComponent<ComponentDebugger>(spawnedId));
            //debuggers[gunkObject.id].currentId = gunkObject.id;
            return  new GunkTransform(gunkObject, positions, gunkObject.id);
        }

        public void addLog(int id, string author, string message)
        {
            //logger.addLog(id, author, message);
        }

        public int registerGunkObject(IGunkObject objectToRegister)
        {
            var id = idManager.getFreeId();
            allObjects[id] = objectToRegister;
            return id;
        }

        public IGunkObject getObjectWithId(int id)
        {
            return allObjects.ContainsKey(id) ? allObjects[id] : null;
        }

        public ObjectPattern calculatePrefabMeshDimensions(string prefabName)
        {
            /*
            var bounds = MeshBoundsUtils.getPrefabDimensions(prefabManager.findPrefab(prefabName));
            var boundsMin = VectorConversions.toFloat3(bounds.min);
            var boundsMax = VectorConversions.toFloat3(bounds.max);
            */
            var pattern = new ObjectPattern(3, 3);
            
            //TODO move this code out of here
            if (prefabName.ToLower() == "miner")
                pattern.setFieldType(new Int2(0, 0), TerrainFieldType.CrystalMine);

            return pattern;
        }

        public void destroyObject(int id)
        {
            updateCaller.deleteAllObjectFunctions(allObjects[id]);
            allObjects.Remove(id);
            extraObjectsRenderer.sendChange(new HideAndResetObjectCounters(id));
        }

        public ISkeletonAnimator instantiateAnimator(IGunkObject gunkObject, string prefabName,
            string startClipName)
        {
            var animator = new SkeletonAnimatorComponent(gunkObject, prefabName, 
                extraObjectsRenderer.renderersParametersManager,
                assetManager, 
                gunkObject.transform.id, startClipName,
                animatorsManager);
            updateCaller.addFunction(gunkObject, () => animator.updateAnimation(totalTime));
            return animator;
        }

        public List<List<int>> getAllConnectibleNetworks()
        {
            return connectablesBuildingsModule.getAllConnectibleNetworks();
        }
        
        public void setNavigationAgentProducer(INavigationAgentProducer newAgentProducer)
        {
            agentProducer = newAgentProducer;
        }

        public bool canSpaceBeLocked(Float3 position, ObjectPattern pattern)
        {
            return map.placedObjectsManager.canBuild(position, pattern);
        }

        public Float3? calculateClosestAvailablePosition(Float3 position, ObjectPattern pattern)
        {
            return map.placedObjectsManager.calculateClosestAvailablePosition(position, pattern);
        }

        public IResourcesContainer findResourcesWithId(int id)
        {
            return allObjects[id].resourcesContainer;
        }

        public Float3? getObjectPosition(int id)
        {
            return allObjects[id]?.transform?.position;
        }

        public INavigationAgent instantiateNavmeshAgent(IGunkObject gunkObject)
        {
            throw new NotImplementedException();
            //return gameObjectManager.createNavmeshComponent(gunkObject.transform.id);
        }

        private void updateEnergyNetworks(float deltaTime)
        {
            profiler.beginSample("updating energy networks");
            var energyData = new List<EnergyNetwork.EnergyObjectData>();
            foreach (var gunkObject in allObjects.Values)
            {
                var id = gunkObject.id;

                var generated = gunkObject.getEnergyGenerationLevel() * deltaTime;
                var needs = gunkObject.getEnergyNeeds();

                energyData.Add(new EnergyNetwork.EnergyObjectData(id, generated, needs));
            }

            var simulationResult = energyNetwork.simulateSingleTick(energyData, getAllConnectibleNetworks());
            foreach (var objectSimulationResult in simulationResult)
                allObjects[objectSimulationResult.objectId]
                    .provideWithEnergy(objectSimulationResult.energyProvided);
            profiler.endSample();
        }

        private void callGunkObjectUpdateFunctions(float deltaTime)
        {
            profiler.beginSample("calling gunkObject updateFunctions");
            foreach (var gunkObject in allObjects.ToList()) gunkObject.Value.update(deltaTime);

            profiler.endSample();
        }

        public void update(float deltaTime)
        {
            totalTime += deltaTime;
            callGunkObjectUpdateFunctions(deltaTime);
            updateCaller.invokeAll();
            updateEnergyNetworks(deltaTime);
            corruptedLand.update(deltaTime, 0.1f);
            extraObjectsRenderer.sendChange(new AddCorruptionChanges(corruptedLand.getChanges()));
        }

        public List<GunkObjectData> getObjectsData()
        {
            var result = new List<GunkObjectData>();
            foreach (var gunkObject in allObjects.Values)
            {
                var id = gunkObject.id;
                var position = gunkObject.transform.position;
                var type = gunkObject.type;
                var faction = gunkObject.faction;
                var collider = gunkObject.collider;
                result.Add(new GunkObjectData(id, position, type, faction, collider));
            }

            return result;
        }
    }
}