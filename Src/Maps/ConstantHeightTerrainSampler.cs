using Src.VectorTypes;

namespace Src.Maps
{
    public class ConstantHeightTerrainSampler : ITerrainHeightSampler
    {
        public float sampleHeight(Float3 position)
        {
            return 0.0f;
        }

        public Float3 correctHeight(Float3 position)
        {
            return new Float3(position.x, 0, position.z);
        }
    }
}