using Src.Maps;

namespace Gunk_Scripts.GunkComponents
{
    public interface IResourcesContainer
    {
        ResourceType type { get; }
        bool isDepleted();

        GatheredResources gatherResources(float deltaTime);
    }
}