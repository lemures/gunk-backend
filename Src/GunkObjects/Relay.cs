using Gunk_Scripts.GunkComponents;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class Relay : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.Relay);
        private static readonly int maxHealth = 1000;

        public Relay(IGunkObjectManagerInternalData objectManager, Float3 position, Faction faction) :
            base(objectManager, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleConnector(this, 20));
            addPlacedObject();
            addCuboidCollider(new Float3(2, 6, 2));
            addEnergyGenerator(new ConstantValueEnergyGenerator(300.0f));
            addEnergy(100, 500, 50);
            finishAddingComponents();
        }
    }
}