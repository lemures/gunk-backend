using Src.GunkComponents;

namespace Gunk_Scripts.GunkComponents
{
    public abstract class Connectible : GunkComponent
    {
        protected Connectible(IGunkObject parent) : base(parent)
        {
            var buildingData = new ConnectablesBuildingsModule.ConnectibleObjectData(
                parent.id,
                parent.faction,
                parent.transform.position,
                parent.animator.getAllSocketBones());
            parent.objectManager.connectablesNetwork.addObject(buildingData);
        }

        public override void destroy()
        {
            parent.objectManager.connectablesNetwork.removeObject(parent.id);
        }
    }
}