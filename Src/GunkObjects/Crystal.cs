using Gunk_Scripts.GunkComponents;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class Crystal : GunkObject
    {
        protected Crystal(IGunkObjectManagerInternalData objectManagerFacade, string prefabName, Float3 position,
            IResourcesContainer resourcesContainer) : base(objectManagerFacade, prefabName, position)
        {
            addResource(resourcesContainer, true, false);
            finishAddingComponents();
        }
    }
}