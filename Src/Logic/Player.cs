using System;
using Gunk_Scripts.GunkComponents;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Src.Utils;

namespace Src.Logic
{
    [Serializable]
    public class Player
    {
        [JsonProperty]
        [JsonConverter(typeof(StringEnumConverter))]
        public readonly Faction representingFaction;

        [JsonProperty]
        private readonly OwnedResources ownedResources = new OwnedResources();

        public Player(Faction representingFaction)
        {
            this.representingFaction = representingFaction;
        }

        public void addResources(GatheredResources resources)
        {
            ownedResources.addResources(resources);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(Player))
                .addVariable(representingFaction, nameof(representingFaction))
                .addVariable(ownedResources, nameof(ownedResources))
                .print();
        }
    }
}