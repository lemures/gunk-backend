using Gunk_Scripts.Renderers;

namespace Src.GameRenderer
{
    public class HideCounter : ExtraObjectsChange
    {
        private readonly int objectId;
        private readonly CounterDrawStyle style;

        public HideCounter(int objectId, CounterDrawStyle style)
        {
            this.objectId = objectId;
            this.style = style;
        }

        protected bool Equals(HideCounter other)
        {
            return objectId == other.objectId && style == other.style;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((HideCounter) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (objectId * 397) ^ (int) style;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.countersRenderer.hideCounter(objectId, style);
        }
    }
}