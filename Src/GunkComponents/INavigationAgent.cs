﻿using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public interface INavigationAgent
    {
        bool isStopped { get; }
        Float3 destination { get; set; }
        float remainingDistance { get; }
        Float3 position { get; }
        void stop();
        void resume();
        void setMaxSpeed(float value);
    }
}