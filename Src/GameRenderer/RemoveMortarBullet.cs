namespace Src.GameRenderer
{
    public class RemoveMortarBullet : ExtraObjectsChange
    {
        protected bool Equals(RemoveMortarBullet other)
        {
            return id == other.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RemoveMortarBullet) obj);
        }

        public override int GetHashCode()
        {
            return id;
        }

        private int id;

        public RemoveMortarBullet(int id)
        {
            this.id = id;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.generalPurposeLineRenderer.removeObject(id);
        }
    }
}