namespace Src.ObjectsSelection
{
    public enum SelectionPriority
    {
        VeryLow,
        Low,
        Medium,
        High,
        VeryHigh
    }
}