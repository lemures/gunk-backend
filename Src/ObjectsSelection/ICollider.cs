﻿using Src.VectorTypes;

namespace Src.ObjectsSelection
{
    public interface ICollider
    {
        Float3 center { get; set; }

        /// <summary>
        ///     returns true if mouse is on object
        /// </summary>
        bool isUnderPoint(IScreenPositionCalculator positionCalculator, Float2 point);

        /// <summary>
        ///     returns true if the object is inside screen space rectangle with given corners
        /// </summary>
        bool isInScreenRectangle(Float2 leftCorner, Float2 rightCorner, IScreenPositionCalculator positionCalculator);
    }
}