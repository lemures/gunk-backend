using System;
using System.Collections.Generic;
using Gunk_Scripts.GunkComponents;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Src.Utils;

namespace Src.Logic
{
    [Serializable]
    public class Players
    {
        [JsonProperty]
        private readonly List<Player> allPlayers = new List<Player>();

        public Players()
        {
            foreach (Faction faction in Enum.GetValues(typeof(Faction))) allPlayers.Add(new Player(faction));
        }

        public void addResources(Faction faction, GatheredResources resources)
        {
            allPlayers.Find(x => x.representingFaction == faction).addResources(resources);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(allPlayers))
                .addVariable(allPlayers, nameof(allPlayers))
                .print();
        }
    }
}