using System;
using Src.DataStructures;
using Src.Maps;
using Src.VectorTypes;

namespace Gunk_Scripts.Maps
{
    public class ClosestResourceFinder : SameTypeFinder<ResourceType>, IClosestResourceFinder
    {
        public ClosestResourceFinder(int mapWidth, int mapHeight) : base(mapWidth, mapHeight)
        {
        }

        public FoundObject findClosestObject(Float2 position, float maxDistance)
        {
            FoundObject best = null;
            foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
            {
                var current = findClosestObject(position, resourceType, maxDistance);
                if (best == null || current != null && current.distance < best.distance) best = current;
            }

            return best;
        }
    }
}