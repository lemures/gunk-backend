namespace Src.SerializationUtils
{
    public interface IIdentityProvider
    {
        int getFreeId();

        void deregisterId(int id);
    }
}