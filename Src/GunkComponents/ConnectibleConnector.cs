using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    /// <summary>
    ///     component that enables object to act as a connector in energy network
    /// </summary>
    public class ConnectibleConnector : Connectible, IConstructionFinishedCallbackReceiver
    {
        private readonly float connectorRange;

        public ConnectibleConnector(IGunkObject parent, float connectorRange) : base(parent)
        {
            this.connectorRange = connectorRange;
        }

        public void finishConstruction()
        {
            parent.objectManager.connectablesNetwork.transformIntoConnector(parent.id, connectorRange);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ConnectibleConnector))
                .addVariable(connectorRange, nameof(connectorRange))
                .print();
        }
    }
}