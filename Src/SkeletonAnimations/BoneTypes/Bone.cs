﻿using System;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.BoneTypes
{
    [Serializable]
    public abstract class Bone
    {
        public string label;
        public Float3 position;

        protected Bone(Float3 position)
        {
            this.position = position;
        }

        /// <summary>
        ///     returns true if this bone indicates where part of Mesh begins
        /// </summary>
        /// <returns></returns>
        public virtual bool needsAssignedComponent()
        {
            return false;
        }

        public abstract string getName();
    }
}