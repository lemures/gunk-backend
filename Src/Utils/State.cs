﻿namespace Src.Utils
{
    public class State<T>
    {
        protected readonly T parent;

        protected State(T parent)
        {
            this.parent = parent;
        }

        public virtual void setAsActive()
        {
        }

        public virtual void deactivate()
        {
        }

        public virtual void update(float deltaTime)
        {
        }
    }
}