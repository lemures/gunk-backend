using Gunk_Scripts.GunkObjects;
using Gunk_Scripts.Objects_Selection;
using Src.Logic;
using Src.ObjectsSelection;
using Src.VectorTypes;

namespace Src.GunkObjectManager
{
    public class GunkObjectData
    {
        public ICollider collider;
        public int id;

        public GunkObjectData(int id, Float3 position, GunkObjectType type, Faction faction, ICollider collider)
        {
            this.position = position;
            this.type = type;
            this.faction = faction;
            this.id = id;
            this.collider = collider;
        }

        public Float3 position { get; }
        public GunkObjectType type { get; }
        public Faction faction { get; }
    }
}