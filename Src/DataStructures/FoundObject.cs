﻿namespace Src.DataStructures
{
    public class FoundObject
    {
        public readonly int id;
        public float distance;

        public FoundObject(float distance, int id)
        {
            this.distance = distance;
            this.id = id;
        }

        public override string ToString()
        {
            return "id: " + id + " distance: " + distance;
        }
    }
}