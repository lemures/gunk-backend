using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddMainModel : ExtraObjectsChange
    {
        private int id;
        private string prefabName;
        private Float3 position;

        public AddMainModel(int id, string prefabName, Float3 position)
        {
            this.id = id;
            this.prefabName = prefabName;
            this.position = position;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.prefabRenderer.instantiatePrefab(id, prefabName, position, Float3.zero);

        }
    }
}