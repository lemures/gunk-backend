﻿using Src.VectorTypes;

namespace Src.SkeletonAnimations.Internal
{
    public class BoneAnimationOutput
    {
        public readonly int boneId;
        public Float3 rotation;
        public Float3 translation;

        public BoneAnimationOutput(int boneId, Float3 translation, Float3 rotation)
        {
            this.boneId = boneId;
            this.translation = translation;
            this.rotation = rotation;
        }
    }
}