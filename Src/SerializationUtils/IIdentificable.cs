namespace Src.SerializationUtils
{
    public interface IIdentifiable
    {
        long uniqueId { get; }
    }
}