﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Src.SkeletonAnimations.BoneTypes;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.Internal
{
    [Serializable]
    public class SkeletonData
    {
        private const int MAX_LIMB_NUMBER = 10;
        public const int MAX_SEGMENT_NUMBER = 4;
        private const int MAX_SOCKET_NUMBER = 4;
        private const int MAX_BARREL_NUMBER = 6;

        private static readonly int maxBoneCount =
            1 + MAX_LIMB_NUMBER * MAX_SEGMENT_NUMBER + MAX_BARREL_NUMBER + MAX_SOCKET_NUMBER;

        [JsonProperty("barrel bones")]
        private List<BarrelBone> barrelBones = new List<BarrelBone>();
        [JsonProperty("basic bones")]
        private List<BasicBone> basicBones = new List<BasicBone>();
        [JsonProperty("offset bones")]
        private List<BasicBoneOffset> basicOffsetBones = new List<BasicBoneOffset>();
        [JsonProperty("main bone")]
        private MainBodyBone mainBone = new MainBodyBone();
        [JsonProperty("socket bones")]
        private List<SocketBone> socketBones = new List<SocketBone>();

        public void clear()
        {
            barrelBones.Clear();
            basicOffsetBones.Clear();
            mainBone = new MainBodyBone();
            socketBones.Clear();
            basicBones.Clear();
        }
        
        public int getBarrelBoneId(int index)
        {
            var foundBone = barrelBones.Find(x => x.index == index);
            return foundBone != null ? getBoneId(foundBone) : -1;
        }

        public static int getBoneId(Bone bone)
        {
            if (bone is BasicBone) return ((BasicBone) bone).segmNum * MAX_LIMB_NUMBER + ((BasicBone) bone).limbNum;

            if (bone is MainBodyBone) return MAX_SEGMENT_NUMBER * MAX_LIMB_NUMBER + 1;

            if (bone is BarrelBone) return MAX_SEGMENT_NUMBER * MAX_LIMB_NUMBER + 2 + ((BarrelBone) bone).index;

            return -1;
        }

        public static int getMaxBoneCount()
        {
            return maxBoneCount;
        }

        public MainBodyBone getMainBodyBone()
        {
            return mainBone;
        }

        public void addBasicBoneOffset(int segmentNum, int limbNum, Float3 position)
        {
            if (limbNum >= MAX_LIMB_NUMBER || segmentNum >= MAX_SEGMENT_NUMBER) return;
            var newBone = new BasicBoneOffset(segmentNum, limbNum, position);
            var foundBone = basicOffsetBones.Find(x => newBone.Equals(x));
            if (foundBone == null)
                basicOffsetBones.Add(newBone);
            else
                foundBone.position = newBone.position;
        }

        public void addBasicBone(int segmentNum, int limbNum, Float3 position)
        {
            if (limbNum >= MAX_LIMB_NUMBER || segmentNum >= MAX_SEGMENT_NUMBER) return;

            var newBone = new BasicBone(segmentNum, limbNum, position);
            var foundBone = basicBones.Find(x => newBone.Equals(x));
            if (foundBone == null)
                basicBones.Add(newBone);
            else
                foundBone.position = newBone.position;
        }

        private void addSocketBone(int index, Float3 center, float rotation)
        {
            var newBone = new SocketBone(index, center, rotation);
            var foundBone = socketBones.Find(x => newBone.Equals(x));
            if (foundBone == null)
            {
                socketBones.Add(newBone);
            }
            else
            {
                foundBone.position = newBone.position;
                foundBone.rotation = newBone.rotation;
            }
        }

        public void addBarrelBone(int parentSegmentNum, int parentLimbNum, Float3 position)
        {
            var newBone = new BarrelBone(parentSegmentNum, parentLimbNum, getFirstFreeBarrelIndex(), position);
            var foundBone = barrelBones.Find(x => newBone.Equals(x));
            if (foundBone == null)
            {
                barrelBones.Add(newBone);
            }
            else
            {
                foundBone.position = newBone.position;
                foundBone.parentLimbNum = parentLimbNum;
                foundBone.parentSegmNum = parentSegmentNum;
            }
        }

        public bool canAddNextBarrel()
        {
            return getFirstFreeBarrelIndex() < MAX_BARREL_NUMBER;
        }

        public bool canAddNextLimb()
        {
            return getFirstFreeLimbIndex() < MAX_LIMB_NUMBER;
        }

        public bool canAddNextSocket()
        {
            return getFirstFreeSocketIndex() < MAX_SOCKET_NUMBER;
        }

        private int getFirstFreeBarrelIndex()
        {
            var numbersExist = (from bone in barrelBones select bone.index).ToList();
            return Enumerable.Range(0, int.MaxValue).Except(numbersExist).First();
        }

        private int getFirstFreeSocketIndex()
        {
            var numbersExist = (from bone in socketBones select bone.index).ToList();
            return Enumerable.Range(0, int.MaxValue).Except(numbersExist).First();
        }

        public void addNextSocketBone(Float3 position)
        {
            var firstFreeNum = getFirstFreeSocketIndex();
            //TODO this would have behaved much better if the rotation had been based on modelPosition
            addSocketBone(firstFreeNum, position, 0.0f);
        }

        public void addNextLimb(Float3 position)
        {
            var firstFreeNum = getFirstFreeLimbIndex();
            addBasicBone(0, firstFreeNum, position);
        }

        private int getFirstFreeLimbIndex()
        {
            var numbersExist = (from bone in basicBones select bone.limbNum).ToList();
            return Enumerable.Range(0, int.MaxValue).Except(numbersExist).First();
        }

        public void addBoneSegment(BasicBone boneToAddTo, Float3 position)
        {
            addBasicBone(boneToAddTo.segmNum + 1, boneToAddTo.limbNum, position);
        }

        public List<Bone> getAllBones()
        {
            var allBones = new List<Bone>();
            allBones.AddRange(barrelBones);
            allBones.AddRange(basicBones);
            allBones.AddRange(basicOffsetBones);
            allBones.AddRange(socketBones);
            allBones.Add(mainBone);
            return allBones;
        }

        private List<Bone> getAllBoneNeedingAnimation()
        {
            var result = new List<Bone>();
            result.AddRange(basicBones);
            result.AddRange(barrelBones);
            result.Add(mainBone);

            return result;
        }

        /// <summary>
        ///     Erases certain bone
        /// </summary>
        public void eraseBone(Bone bone)
        {
            foreach (var nextBone in getHigherBonesInLimb(bone))
                eraseBone(nextBone);
            
            if (typeof(BasicBone) == bone.GetType())
                basicBones.Remove((BasicBone) bone);
            if (typeof(BasicBoneOffset) == bone.GetType())
                basicOffsetBones.Remove((BasicBoneOffset) bone);
            if (typeof(BarrelBone) == bone.GetType())
                barrelBones.Remove((BarrelBone) bone);
            if (typeof(SocketBone) == bone.GetType())
                socketBones.Remove((SocketBone) bone);
            if (typeof(BasicBone) == bone.GetType() || typeof(BasicBoneOffset) == bone.GetType())
            {
                if (typeof(BasicBone) == bone.GetType() && getLowerBoneInLimb(bone).GetType() == typeof(BasicBoneOffset)
                ) eraseBone(getLowerBoneInLimb(bone));

            }

        }

        /// <summary>
        ///     Calculates position of a bone based on potential existing boneOffset bones
        /// </summary>
        public Float3 getRealBonePosition(Bone bone)
        {
            return bone.position;
        }

        private Bone getPreviousBone(Bone bone)
        {
            if (bone is MainBodyBone)
            {
                return null;
            }

            if (bone is SocketBone)
            {
                return getMainBodyBone();
            }

            if (bone is BasicBoneOffset offset)
            {
                var limbNum = offset.limbNum;
                var segmNum = offset.segmNum;
                var previousBone = basicBones.Find(x => x.limbNum == limbNum && x.segmNum == segmNum - 1) as Bone ??
                                   getMainBodyBone();
                return previousBone;
            }

            if (bone is BasicBone basicBone)
            {
                var limbNum = basicBone.limbNum;
                var segmNum = basicBone.segmNum;
                var previousBone = basicOffsetBones.Find(x => x.limbNum == limbNum && x.segmNum == segmNum) ?? 
                                   basicBones.Find(x => x.limbNum == limbNum && x.segmNum == segmNum - 1) as Bone ??
                                   getMainBodyBone();
                return previousBone;
            }

            if (bone is BarrelBone barrelBone)
            {
                var parentLimbNum = barrelBone.parentLimbNum;
                var parentSegmentNum = barrelBone.parentSegmNum;
                var previousBone =
                    basicBones.Find(x => x.limbNum == parentLimbNum && x.segmNum == parentSegmentNum);
                return previousBone;
            }

            return null;
        }
        
        //returns first bone lower in hierarchy(bone's "parent") (if a bone has corresponding boneOffsetBone then it is returned) otherwise it returns bone with the same limbNum and lower segmNum or mainBone if it doesn't exist
        public Bone getLowerBoneInLimb(Bone bone)
        {
            return getPreviousBone(bone);
        }

        private Bone getLowerAnimableBoneInLimb(Bone bone)
        {
            var result = getLowerBoneInLimb(bone);
            while (result != null && !result.needsAssignedComponent()) result = getLowerBoneInLimb(result);

            return result;
        }

        //returns first bone higher in hierarchy(bone's "son"), returned bone can be boneOffsetBone, returns null if no such bone exists
        public List<Bone> getHigherBonesInLimb(Bone bone)
        {
            return getAllBones().FindAll(x => bone.Equals(getPreviousBone(x)));
        }

        public override string ToString()
        {
            var result = "SkeletonData:  \n";
            result += "total bone count: " + getAllBones().Count + "\n";
            result += "all bones:\n";
            foreach (var bone in getAllBones()) result += "   " + bone + "\n";

            return result;
        }

        public List<AnimatorBoneData> getBonesData()
        {
            var result = new List<AnimatorBoneData>();
            foreach (var bone in getAllBoneNeedingAnimation())
            {
                
                //we only want skeleton graph to contain bones that need animation
                var previousBone = getLowerAnimableBoneInLimb(bone);
                var previousBoneLocalId = previousBone != null ? getBoneId(previousBone) : -1;
                var potentialBoneOffset = getLowerBoneInLimb(bone) as BasicBoneOffset;
                var boneLocalId = getBoneId(bone);
                var bonePositionOffset = potentialBoneOffset == null
                    ? Float3.zero
                    : -((Float3) bone.position - potentialBoneOffset.position);
                var boneData = new AnimatorBoneData(boneLocalId, previousBoneLocalId, bonePositionOffset,
                    bone.position);
                result.Add(boneData);
            }

            return result;
        }
    }
}