﻿using Src.DataStructures;
using Src.VectorTypes;

namespace Gunk_Scripts.Renderers
{
    public interface ICableCollection
    {
        void addCable(Edge edge, Float3 startPosition, Float3 endPosition);
        void removeCable(Edge edge);
    }
}