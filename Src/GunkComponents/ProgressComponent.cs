using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using Src.GameRenderer;

namespace Src.GunkComponents
{
    public class ProgressComponent : GunkComponent
    {
        private Counter progressCounter = new Counter(0, 1.0f);

        public ProgressComponent(IGunkObject parent) : base(parent)
        {
            parent.objectManager.addVisibleObjectsChange(new AddHiddenCounter(parent.id, progressCounter,
                CounterDrawStyle.Progress));
        }

        public override void destroy()
        {
            parent.objectManager.addVisibleObjectsChange(
                new HideCounter(parent.id, CounterDrawStyle.Progress));
        }

        public void setValue(float value)
        {
            progressCounter.changeValue(value);
            parent.objectManager.addVisibleObjectsChange(new SetCounterValue(parent.id,
                CounterDrawStyle.Progress, value));
        }

        public void show()
        {
            parent.objectManager.addVisibleObjectsChange(
                new ShowCounter(parent.id, CounterDrawStyle.Progress));
        }

        public void hide()
        {
            parent.objectManager.addVisibleObjectsChange(
                new HideCounter(parent.id, CounterDrawStyle.Progress));
        }
    }
}