﻿namespace Src.Maps
{
    public interface IMap
    {
        ITerrainHeightSampler heightSampler { get; }
        ITerrainPlacedObjectsManager placedObjectsManager { get; }
        int mapHeight { get; }
        int mapWidth { get; }
    }
}