namespace Src.GameRenderer
{
    public class RemoveLaser : ExtraObjectsChange
    {
        private readonly int id;

        public RemoveLaser(int id)
        {
            this.id = id;
        }

        protected bool Equals(RemoveLaser other)
        {
            return id == other.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((RemoveLaser) obj);
        }

        public override int GetHashCode()
        {
            return id;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.generalPurposeLineRenderer.removeObject(id);
        }
    }
}