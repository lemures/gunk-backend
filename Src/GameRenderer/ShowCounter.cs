using Gunk_Scripts.Renderers;

namespace Src.GameRenderer
{
    public class ShowCounter : ExtraObjectsChange
    {
        private readonly int objectId;
        private readonly CounterDrawStyle style;

        public ShowCounter(int objectId, CounterDrawStyle style)
        {
            this.objectId = objectId;
            this.style = style;
        }

        private bool Equals(ShowCounter other)
        {
            return objectId == other.objectId && style == other.style;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((ShowCounter) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (objectId * 397) ^ (int) style;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.countersRenderer.showCounter(objectId, style);
        }
    }
}