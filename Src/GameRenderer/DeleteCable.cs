using Src.DataStructures;

namespace Src.GameRenderer
{
    public class DeleteCable : ExtraObjectsChange
    {
        private readonly Edge edge;

        public DeleteCable(Edge edge)
        {
            this.edge = edge;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.cableRenderer.removeCable(edge);
        }
    }
}