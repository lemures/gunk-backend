using Src.DataStructures;

namespace Src.Utils
{
    public class UniqueIdGenerator : IUniqueIdGenerator
    {
        private static IdManager idManager = new IdManager();
        
        public int getId()
        {
            return idManager.getFreeId();
        }

        public void returnId(int id)
        {
            idManager.deregisterId(id);
        }
    }
}