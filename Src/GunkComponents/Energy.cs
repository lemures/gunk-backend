﻿using System;
using Gunk_Scripts.Renderers;
using Src.GameRenderer;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class Energy : GunkComponent, IEnergyUser, IEnergy
    {
        public enum EnergyCounterDrawingBehaviour
        {
            AlwaysVisible,
            AlwaysHidden,
            VisibleIfNotFull
        }

        private readonly float maxEnergyProvidedPerSecond;
        private EnergyCounterDrawingBehaviour behaviour;

        private Counter energy;

        public Energy(IGunkObject parent, EntryEnergyState state) : base(parent)
        {
            maxEnergyProvidedPerSecond = state.maxEnergyProvidedPerSecond;
            addEnergyCounter(state.energy, state.maxEnergy);
            updateCounterDrawingBasedOnBehaviour();
        }

        public float calculateEnergyNeeds()
        {
            return Math.Min(maxEnergyProvidedPerSecond, energy.valueToFull);
        }

        public void chargeEnergy(float amount)
        {
            energy.increaseValue(amount);
            updateCounterDrawingBasedOnBehaviour();
        }

        public void setCounterDrawingBehaviour(EnergyCounterDrawingBehaviour newBehaviour)
        {
            behaviour = newBehaviour;
            updateCounterDrawingBasedOnBehaviour();
        }

        public override void destroy()
        {
            parent.objectManager.addVisibleObjectsChange(new HideCounter(parent.id, CounterDrawStyle.Energy));
        }

        private void addEnergyCounter(float currentValue, float maxValue)
        {
            energy = new Counter(currentValue, maxValue);
            parent.objectManager.addVisibleObjectsChange(new AddHiddenCounter(parent.id, energy,
                CounterDrawStyle.Energy));
        }

        public float getCurrentValue()
        {
            return energy.currentValue;
        }

        public bool isEnergyAvailable(float amount)
        {
            return energy.currentValue >= amount;
        }

        public void useEnergy(float amount)
        {
            if (energy.currentValue < amount) throw new ArgumentException("there is not enough energy available");
            parent.addLog(this, "discharged with " + amount + " energy");
            energy.decreaseValue(amount);
            updateCounterDrawingBasedOnBehaviour();
        }

        private void updateCounterDrawingBasedOnBehaviour()
        {
            var drawStyle = CounterDrawStyle.Energy;
            parent.objectManager.addVisibleObjectsChange(new SetCounterValue(parent.id, drawStyle,
                energy.currentValue));
            switch (behaviour)
            {
                case EnergyCounterDrawingBehaviour.AlwaysVisible:
                    parent.objectManager.addVisibleObjectsChange(new ShowCounter(parent.id, drawStyle));
                    break;
                case EnergyCounterDrawingBehaviour.AlwaysHidden:
                    parent.objectManager.addVisibleObjectsChange(new HideCounter(parent.id, drawStyle));
                    break;
                case EnergyCounterDrawingBehaviour.VisibleIfNotFull:
                    if (energy.isFull())
                        parent.objectManager.addVisibleObjectsChange(new ShowCounter(parent.id, drawStyle));
                    else
                        parent.objectManager.addVisibleObjectsChange(new HideCounter(parent.id, drawStyle));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(behaviour), behaviour, null);
            }
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(this)
                .addVariable(energy, nameof(energy))
                .addVariable(maxEnergyProvidedPerSecond, nameof(maxEnergyProvidedPerSecond))
                .addVariable(behaviour, "drawing behaviour")
                .addVariable(calculateEnergyNeeds(), "current energy needs")
                .print();
        }

        public struct EntryEnergyState
        {
            public readonly float energy;
            public readonly float maxEnergy;
            public readonly float maxEnergyProvidedPerSecond;

            public EntryEnergyState(float energy, float maxEnergy, float maxEnergyProvidedPerSecond)
            {
                this.energy = energy;
                this.maxEnergy = maxEnergy;
                this.maxEnergyProvidedPerSecond = maxEnergyProvidedPerSecond;
            }
        }
    }
}