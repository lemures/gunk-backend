namespace Gunk_Scripts.GunkComponents
{
    public interface IConstructionState
    {
        bool constructionFinished { get; }

        float constructionProgress { get; }
    }
}