using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class UnderConstructionImmediatelyFinished : IConstructionState
    {
        public bool constructionFinished => true;
        public float constructionProgress => 1;

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(UnderConstructionImmediatelyFinished))
                .print();
        }
    }
}