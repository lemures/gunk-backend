﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Src.DataStructures
{
    /// <summary>
    ///     enables mapping ids from range {-maxInt,maxInt} to 0,1,2,3.....
    /// </summary>
    [Serializable]
    public class IdMap
    {
        private readonly IdManager idManager;
        private readonly Dictionary<int, int> idMap;

        public IdMap()
        {
            idManager = new IdManager();
            idMap = new Dictionary<int, int>();
        }

        /// <summary>
        ///     adds object and maps it to the first free ID
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="InvalidOperationException">if object is already in map</exception>
        public void addObject(int id)
        {
            if (idMap.ContainsKey(id)) throw new InvalidOperationException("object id " + id + " already added");

            var mappedId = idManager.getFreeId();
            idMap.Add(id, mappedId);
        }

        public void removeObject(int id)
        {
            if (!idMap.ContainsKey(id))
                throw new InvalidOperationException("object to remove id " + id + "  doesn't exist");
            idManager.deregisterId(idMap[id]);
            idMap.Remove(id);
        }

        public void removeAllObjects()
        {
            idManager.deregisterAllIds();
            idMap.Clear();
        }

        /// <summary>
        ///     returns mapped if of a given id or -1 if the object with certain id doesn't exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int getMappedId(int id)
        {
            try
            {
                return idMap[id];
            }
            catch (KeyNotFoundException)
            {
                return -1;
            }
        }

        public uint getObjectCount()
        {
            return (uint) idMap.Count;
        }

        /// <summary>
        ///     returns max taken mapped ID or -1 if none were taken
        /// </summary>
        public int getMaxTakenMappedId()
        {
            return idManager.getMaxTakenId();
        }

        public List<int> getAllMappedIds()
        {
            return idManager.getAllUsedIDs();
        }

        public List<int> getAllObjectIds()
        {
            return idMap.Keys.ToList();
        }
    }
}