using System;
using Newtonsoft.Json;
using NUnit.Framework;
using Src.Logic;

namespace SrcTests.Logic
{
    public class PlayersTest
    {
        [Test]
        public void canBeSerializedTest()
        {
            var players = new Players();
            var serialized = JsonConvert.SerializeObject(players,Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            });;
            Console.Out.Write(serialized);

            var deserialized = JsonConvert.DeserializeObject<Players>(serialized, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            });;
        }
    }
}