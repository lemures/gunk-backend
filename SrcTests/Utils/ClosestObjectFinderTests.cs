using System;
using NUnit.Framework;
using Src.DataStructures;
using Src.VectorTypes;

namespace Gunk_Scripts.Tests.Editor
{
    public class ClosestObjectFinderTests
    {
        [Test]
        public void singleObjectAddedIsSuccessfullyLocated()
        {
            var finder = new ClosestObjectFinder(100, 100);
            finder.addObject(10, new Float2(50, 70));
            Assert.AreEqual(10, finder.findObject(new Float2(10, 10), 1000.0f)?.id);
        }

        [Test]
        public void nullIsReturnedIfObjectIsOutsideRange()
        {
            var finder = new ClosestObjectFinder(100, 100);
            finder.addObject(10, new Float2(50, 70));
            Assert.IsNull(finder.findObject(new Float2(10, 10), 10.0f));
        }

        [Test]
        public void objectIsFoundIfJustInRange()
        {
            var finder = new ClosestObjectFinder(100, 100);
            finder.addObject(10, new Float2(50, 0));
            Assert.IsNull(finder.findObject(new Float2(0, 0), 51.0f));
        }

        [Test]
        public void addingObjectInRangeDoesNotProduceErrors()
        {
            var finder = new ClosestObjectFinder(100, 100);
            Assert.DoesNotThrow(() => finder.addObject(10, new Float2(50, 70)));
        }

        [TestCase(1, -1000)]
        [TestCase(1, 1000)]
        [TestCase(-1000, 1)]
        [TestCase(1000, 1)]
        public void addingObjectOutsideFinderProducesErrors(int wrongPositionX, int wrongPositionY)
        {
            var finder = new ClosestObjectFinder(100, 100);
            Assert.Catch<ArgumentOutOfRangeException>(() =>
                finder.addObject(10, new Float2(wrongPositionX, wrongPositionY)));
        }

        [Test]
        public void removingNonExistentObjectProducesError()
        {
            var finder = new ClosestObjectFinder(100, 100);
            Assert.Catch<ArgumentException>(() => finder.removeObject(0));
        }

        [Test]
        public void addingSameObjectTwiceProducesError()
        {
            var finder = new ClosestObjectFinder(100, 100);
            finder.addObject(10, new Float2(50, 50));
            Assert.Catch<ArgumentException>(() => finder.addObject(10, new Float2(50, 50)));
        }
    }
}