using Gunk_Scripts.Renderers;
using Src.GameRenderer;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class Health : GunkComponent
    {
        private Counter health;

        public Health(IGunkObject parent, float value, float maxValue) : base(parent)
        {
            health = new Counter(value, maxValue);
            parent.objectManager.addVisibleObjectsChange(new AddHiddenCounter(parent.id, health,
                CounterDrawStyle.Health));
            parent.objectManager.addVisibleObjectsChange(new ShowCounter(parent.id, CounterDrawStyle.Health));
        }

        public float maxValue => health.maxValue;

        public float currentValue => health.currentValue;

        public override void destroy()
        {
            parent.objectManager.addVisibleObjectsChange(new HideCounter(parent.id, CounterDrawStyle.Health));
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(health))
                .addVariable(health, nameof(health))
                .print();
        }

        public void injure(float damage)
        {
            health.decreaseValue(damage);
            parent.addLog(this, "damaged with " + damage + " damage");
            if (health.isEmpty()) parent.destroy();
            parent.objectManager.addVisibleObjectsChange(new SetCounterValue(parent.id,
                CounterDrawStyle.Health, health.currentValue));
        }

        public void heal(float toAdd)
        {
            health.increaseValue(toAdd);
            parent.objectManager.addVisibleObjectsChange(new SetCounterValue(parent.id,
                CounterDrawStyle.Health, health.currentValue));
        }
    }
}