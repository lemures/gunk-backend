﻿namespace Gunk_Scripts.GunkComponents
{
    public interface IEnergyUser
    {
        float calculateEnergyNeeds();
        void chargeEnergy(float amount);
    }
}