﻿using System;
using System.Collections.Generic;
using Src.DataStructures;
using Src.ShaderUtils;
using Src.Shader_Constants;
using Src.Utils;
using Src.VectorTypes;

// ReSharper disable NotAccessedField.Local (GpuBoneState Used only in shaders)

namespace Src.SkeletonAnimations.Internal
{
    public interface IAnimatorsManager
    {
        int addAnimator(IRenderersParametersManager objectManager, int gameObjectId);
        void removeAnimator(int animatorId);
        void updateTranslation(int animatorId, int boneLocalId, Float3 newTranslation);
        void updateRotation(int animatorId, int boneLocalId, Float3 newRotation);
        Float3 getBoneRotation(int animatorId, int boneLocalId, Float3 objectRotation);
        Float3 getBonePosition(int animatorId, int boneLocalId, Float3 objectPosition);
        void addBone(int animatorId, AnimatorBoneData data);
    }

    public class AnimatorsManager : IAnimatorsManager
    {
        private const string BONE_STATES_BUFFER_NAME = "boneStates";
        private const string CALCULATED_TRANSFORMATIONS_BUFFER_NAME = "calculatedTransformations";
        private const string MAPPED_BONES_IDS_BUFFER_NAME = "mappedBonesIds";
        private const int MATRIX_COMPUTER_DISPATCH_SIZE = 128;
        private const string MATRIX_COMPUTER_KERNEL_NAME = "TransformationMatrixComputer";
        private const string MAX_BONE_COUNT_PROPERTY_NAME = "maxBoneCount";

        private const int GPU_BONE_STATE_STRUCTURE_SIZE = 4 * (1 + 3 * 4);

        private readonly List<BoneId> allBones = new List<BoneId>();

        private readonly IdManager animatorsIdManager = new IdManager();
        private int[] bonesMappedIds = new int[1];

        private GpuBoneState[] gpuBoneStates = new GpuBoneState[1];
        private readonly IGraphicsCard graphicsCard;

        public AnimatorsManager(IGraphicsCard graphicsCard)
        {
            this.graphicsCard = graphicsCard;
            initShaderVariables();
        }

        public void update()
        {
            sendAnimationDataToShaders(null);
        }

        public int addAnimator(IRenderersParametersManager objectManager, int gameObjectId)
        {
            var myId = animatorsIdManager.getFreeId();
            if (gpuBoneStates.Length < (myId + 1) * SkeletonData.getMaxBoneCount())
            {
                resizeGpuBoneStates(2 * (myId + 1) * SkeletonData.getMaxBoneCount());
                resizeCalculatedTransformations(2 * (myId + 1) * SkeletonData.getMaxBoneCount());
            }

            objectManager.setIntParameter(gameObjectId, ToonStandardConstants.objectNumberProperty, myId);
            objectManager.setIntParameter(gameObjectId, ToonStandardConstants.hasAnimatorProperty, 1);
            return myId;
        }

        public void removeAnimator(int animatorId)
        {
            animatorsIdManager.deregisterId(animatorId);
            allBones.RemoveAll(x => x.objectNumber == animatorId);
        }

        private int getBoneGlobalId(int animatorId, int boneLocalId)
        {
            return SkeletonData.getMaxBoneCount() * animatorId + boneLocalId;
        }

        private void initShaderVariables()
        {
            graphicsCard.setInt(MAX_BONE_COUNT_PROPERTY_NAME, SkeletonData.getMaxBoneCount());
            graphicsCard.initializeEmptyBuffer(CALCULATED_TRANSFORMATIONS_BUFFER_NAME, 30000, 2 * 4 * 4);
            graphicsCard.initializeEmptyBuffer(MAPPED_BONES_IDS_BUFFER_NAME, 30000, 4);
            graphicsCard.initializeEmptyBuffer(BONE_STATES_BUFFER_NAME, 30000,
                GPU_BONE_STATE_STRUCTURE_SIZE);
        }

        private void resizeMappedBoneIds(int newSize)
        {
            Array.Resize(ref bonesMappedIds, newSize);
        }

        private void resizeGpuBoneStates(int newSize)
        {
            Array.Resize(ref gpuBoneStates, newSize);
        }

        private void resizeCalculatedTransformations(int newSize)
        {
            //shaderParametersManager.resizeBuffer(CALCULATED_TRANSFORMATIONS_BUFFER_NAME, newSize);
        }

        public void sendAnimationDataToShaders(IProfiler profiler)
        {
            profiler?.beginSample("Animators Manager: sending data to shaders");

            profiler?.beginSample("setting bone mapped Ids");
            for (var i = 0; i < allBones.Count; i++)
                bonesMappedIds[i] = getBoneGlobalId(allBones[i].objectNumber, allBones[i].boneLocalId);
            profiler?.endSample();

            profiler?.beginSample("sending buffers data to GPU");
            graphicsCard.setBufferData(MAPPED_BONES_IDS_BUFFER_NAME, bonesMappedIds);
            profiler?.endSample();

            profiler?.beginSample("setting bone mapped Ids");
            for (var i = 0; i < allBones.Count; i++) bonesMappedIds[i] = 0;
            profiler?.endSample();

            profiler?.beginSample("sending buffers data to GPU");
            graphicsCard.setBufferData(BONE_STATES_BUFFER_NAME, gpuBoneStates);
            profiler?.endSample();

            graphicsCard.dispatchCompute(MATRIX_COMPUTER_KERNEL_NAME,
                1 + bonesMappedIds.Length / MATRIX_COMPUTER_DISPATCH_SIZE);
            profiler?.endSample();
        }

        public void updateTranslation(int animatorId, int boneLocalId, Float3 newTranslation)
        {
            //TODO move this into a separate compute shader
            gpuBoneStates[getBoneGlobalId(animatorId, boneLocalId)].translation = newTranslation;
        }

        public void updateRotation(int animatorId, int boneLocalId, Float3 newRotation)
        {
            //TODO move this into a separate compute shader
            gpuBoneStates[getBoneGlobalId(animatorId, boneLocalId)].rotation = newRotation;
        }

        public Float3 getBoneRotation(int animatorId, int boneLocalId, Float3 objectRotation)
        {
            var currentBoneId = getBoneGlobalId(animatorId, boneLocalId);
            //TODO make this function work in general case
            return objectRotation + gpuBoneStates[currentBoneId].rotation;
        }

        private void translate(ref Float4x4 matrix, Float3 vec)
        {
            matrix = Float4x4.translate(new Float3(vec.x, vec.y, vec.z)) * matrix;
        }

        private void rotateAroundPoint(ref Float4x4 matrix, Float3 rot, Float3 center)
        {
            translate(ref matrix, -center);
            rot *= -360.0f;
            matrix = Float4x4.rotate(rot) * matrix;
            translate(ref matrix, center);
        }

        public Float3 getBonePosition(int animatorId, int boneLocalId, Float3 objectPosition)
        {
            if (boneLocalId < 0) throw new ArgumentException("boneLocalId cannot be negative number");
            var currentBoneId = getBoneGlobalId(animatorId, boneLocalId);
            var boneCenter = gpuBoneStates[currentBoneId].boneCenter;
            var currentTransformation = Float4x4.identity;
            while (currentBoneId != -1)
            {
                rotateAroundPoint(ref currentTransformation, gpuBoneStates[currentBoneId].rotation,
                    gpuBoneStates[currentBoneId].boneCenter);
                translate(ref currentTransformation,
                    gpuBoneStates[currentBoneId].translation + gpuBoneStates[currentBoneId].boneBasicTranslation);
                currentBoneId = gpuBoneStates[currentBoneId].previousBoneId;
            }

            return objectPosition + currentTransformation.multiplyPoint(boneCenter);
        }

        private void addBoneInternal(int objectNumber, int boneLocalId)
        {
            var result = new BoneId(objectNumber, boneLocalId);
            allBones.Add(result);
            if (allBones.Count > bonesMappedIds.Length) resizeMappedBoneIds(bonesMappedIds.Length * 2);
        }

        public void addBone(int animatorId, AnimatorBoneData data)
        {
            addBoneInternal(animatorId, data.boneLocalId);
            var boneGlobalId = getBoneGlobalId(animatorId, data.boneLocalId);
            var previousBoneGlobalId =
                data.previousBoneLocalId == -1 ? -1 : getBoneGlobalId(animatorId, data.previousBoneLocalId);
            gpuBoneStates[boneGlobalId] =
                new GpuBoneState(previousBoneGlobalId, data.boneCenter, data.boneBasicTranslation);
        }

        private struct GpuBoneState
        {
            //id of bone that this bone is attached to or -1 if the bone is MainBodyBone
            public readonly int previousBoneId;

            //first translation, then rotation
            public Float3 rotation;
            public Float3 translation;

            public readonly Float3 boneCenter;

            //used by OffsetBones
            public readonly Float3 boneBasicTranslation;

            public GpuBoneState(int previousBoneId, Float3 boneCenter, Float3 boneBasicTranslation) : this()
            {
                this.previousBoneId = previousBoneId;
                this.boneCenter = boneCenter;
                this.boneBasicTranslation = boneBasicTranslation;
                rotation = Float3.zero;
                translation = Float3.zero;
            }
        }

        private struct BoneId
        {
            public readonly int objectNumber;
            public readonly int boneLocalId;

            public BoneId(int objectNumber, int boneLocalId)
            {
                this.objectNumber = objectNumber;
                this.boneLocalId = boneLocalId;
            }
        }
    }
}