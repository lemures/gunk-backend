using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class ConnectibleObject : Connectible
    {
        /// <summary>
        ///     component that enables object to be connected to energy networks
        /// </summary>
        public ConnectibleObject(IGunkObject parent) : base(parent)
        {
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ConnectibleConnector))
                .print();
        }
    }
}