using Gunk_Scripts.GunkComponents;
using Src.Maps;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class MineBlue : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.MineBlue);

        public MineBlue(IGunkObjectManagerInternalData objectManagerFacade, Float3 position) : base(objectManagerFacade,
            prefabName, position)
        {
            addResource(createResourcesSource(), false, true);
            objectManager.setSpecialFieldType(position, TerrainFieldType.CrystalMine);
            finishAddingComponents();
        }

        private static IResourcesContainer createResourcesSource()
        {
            return new InfiniteResourcesSource(ResourceType.BlueCrystals, 100.0f);
        }
    }
}