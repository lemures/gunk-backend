﻿using System;

namespace Gunk_Scripts
{
    public class MissingResourcesException : Exception
    {
        public MissingResourcesException(string message) : base(message)
        {
        }
    }
}