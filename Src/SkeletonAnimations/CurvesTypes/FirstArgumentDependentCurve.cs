using System;
using System.Runtime.InteropServices;

namespace Src.SkeletonAnimations.CurvesTypes
{
    //curve that will always return x value of argument
    [Guid("93e084ae-a126-4dfd-b661-b53629001014 ")]
    [Serializable]
    public class FirstArgumentDependentCurve : IAnimationCurve
    {
        public float evaluate(float time, float argument)
        {
            return argument;
        }
    }
}