using Src.Objects_Selection;
using Src.Utils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public class CuboidColliderComponent : GunkComponent, IPositionChangedCallbackReceiver
    {
        private readonly Float3 dimensions;

        public CuboidColliderComponent(IGunkObject parent, Float3 dimensions) : base(parent)
        {
            var collider = new CuboidCollider(parent.transform.position, dimensions);
            this.dimensions = dimensions;
            parent.setCollider(collider);
        }

        public void changePosition(Float3 newPosition)
        {
            parent.collider.center = newPosition;
        }

        public override void destroy()
        {
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(CuboidColliderComponent))
                .addVariable(dimensions, nameof(dimensions))
                .print();
        }
    }
}