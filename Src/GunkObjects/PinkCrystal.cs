using Gunk_Scripts.GunkComponents;
using Src.Maps;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class PinkCrystal : Crystal
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.CrystalPink);

        public PinkCrystal(IGunkObjectManagerInternalData objectManagerFacade, Float3 position)
            : base(objectManagerFacade, prefabName, position, createResourcesSource())
        {
        }

        private static LimitedResourcesSource createResourcesSource()
        {
            return new LimitedResourcesSource(ResourceType.PinkCrystals, 100.0f, 20.0f);
        }
    }
}