﻿using System;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.BoneTypes
{
    [Serializable]
    public class BasicBone : Bone
    {
        public int limbNum;
        public int segmNum;

        public BasicBone(int segmNum, int limbNum, Float3 position) : base(position)
        {
            this.segmNum = segmNum;
            this.limbNum = limbNum;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as BasicBone;
            return bone != null &&
                   segmNum == bone.segmNum &&
                   limbNum == bone.limbNum;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (limbNum * 397) ^ segmNum;
            }
        }

        public override bool needsAssignedComponent()
        {
            return true;
        }

        public override string getName()
        {
            return "BasicBone limb " + limbNum + " segment " + segmNum;
        }

        public override string ToString()
        {
            return "BasicBone segmNum: " + segmNum + " limbNum:" + limbNum + " position: " + position;
        }
    }
}