namespace Src.GameRenderer
{
    public class HideAndResetObjectCounters : ExtraObjectsChange
    {
        private readonly int objectId;

        public HideAndResetObjectCounters(int objectId)
        {
            this.objectId = objectId;
        }

        protected bool Equals(HideAndResetObjectCounters other)
        {
            return objectId == other.objectId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((HideAndResetObjectCounters) obj);
        }

        public override int GetHashCode()
        {
            return objectId;
        }


        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.countersRenderer.hideAndResetObjectCounters(objectId);
        }
    }
}