using Gunk_Scripts.GunkComponents;

namespace Gunk_Scripts.Renderers
{
    public interface INavigationAgentProducer
    {
        INavigationAgent createAgent(int gameObjectId);

        void destroyAgent(int gameObjectId);
    }
}