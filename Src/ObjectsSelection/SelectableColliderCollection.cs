using System;
using System.Collections.Generic;
using System.Linq;
using Src.DataStructures;
using Src.ObjectsSelection;
using Src.Objects_Selection;
using Src.VectorTypes;

namespace Gunk_Scripts.Objects_Selection
{
    [Serializable]
    public class SelectableColliderCollection : IColliderCollection
    {
        private readonly List<SelectableObject> allSelectableObjects = new List<SelectableObject>();
        private readonly IdMap idMap = new IdMap();

        private readonly IScreenPositionCalculator positionCalculator;

        public SelectableColliderCollection(IScreenPositionCalculator positionCalculator)
        {
            this.positionCalculator = positionCalculator;
        }

        public bool hasCollider(int id)
        {
            return idMap.getMappedId(id) != -1;
        }

        public void addCollider(int id, ICollider colliderToAdd, SelectionPriority priority)
        {
            idMap.addObject(id);
            var mappedId = idMap.getMappedId(id);
            while (allSelectableObjects.Count <= mappedId) allSelectableObjects.Add(null);
            allSelectableObjects[mappedId] = new SelectableObject(id, colliderToAdd, priority);
        }

        public void removeCollider(int id)
        {
            allSelectableObjects[idMap.getMappedId(id)] = null;
            idMap.removeObject(id);
        }

        public void removeAllColliders()
        {
            var allIds = idMap.getAllObjectIds();
            allIds.ForEach(removeCollider);
        }

        public void setColliderCenter(int id, Float3 newCenter)
        {
            allSelectableObjects[idMap.getMappedId(id)].collider.center = newCenter;
        }

        private List<SelectableObject> getObjectsUnderPointer(Float2 pointerPosition)
        {
            return allSelectableObjects.FindAll(x =>
                x != null &&
                x.collider.isUnderPoint(positionCalculator, pointerPosition));
        }

        private List<SelectableObject> getAllObjectsInDragRectangle(DragRectangle selectedRectangle)
        {
            return (from selectableObject in
                    allSelectableObjects.FindAll(x =>
                        x != null &&
                        x.collider.isInScreenRectangle(selectedRectangle.firstPosition,
                            selectedRectangle.secondPosition, positionCalculator))
                select selectableObject).ToList();
        }

        private static List<SelectableObject> findHighestPriorityObjects(List<SelectableObject> objectsToFilter)
        {
            if (objectsToFilter.Count == 0) return new List<SelectableObject>();
            var highestPriority = objectsToFilter.Max(x => x.priority);
            return objectsToFilter.FindAll(x => x.priority == highestPriority);
        }

        private SelectableObject findHighestPriorityObjectClosestToCamera(List<SelectableObject> objectsToFilter)
        {
            if (objectsToFilter.Count == 0) return null;

            var thoseWithHighestPriority = findHighestPriorityObjects(objectsToFilter);
            var closest = thoseWithHighestPriority.Min(x => positionCalculator.worldToScreenPoint(x.collider.center).y);
            return thoseWithHighestPriority.Find(x =>
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                positionCalculator.worldToScreenPoint(x.collider.center).y == closest);
        }

        /// <summary>
        ///     returns id of highest priority object that is under Pointer
        /// </summary>
        public int? getObjectUnderPointer(Float2 pointerPosition)
        {
            return findHighestPriorityObjectClosestToCamera(getObjectsUnderPointer(pointerPosition))?.id;
        }

        /// <summary>
        ///     returns ids of highest priority objects that colliders are in given rectangle
        /// </summary>
        public List<int> getObjectsInRectangle(DragRectangle dragRectangle)
        {
            return (from selected in findHighestPriorityObjects(getAllObjectsInDragRectangle(dragRectangle))
                select selected.id).ToList();
        }

        [Serializable]
        private class SelectableObject
        {
            public readonly ICollider collider;
            public int id;
            public SelectionPriority priority;

            public SelectableObject(int id, ICollider collider, SelectionPriority priority)
            {
                this.id = id;
                this.collider = collider;
                this.priority = priority;
            }
        }
    }
}