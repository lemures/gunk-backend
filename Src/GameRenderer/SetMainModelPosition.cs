using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class SetMainModelPosition : ExtraObjectsChange
    {
        public int id;
        public Float3 position;

        public SetMainModelPosition(int id, Float3 position)
        {
            this.id = id;
            this.position = position;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
           objectsRenderer.prefabRenderer.setPosition(id, position); 
        }
    }
}