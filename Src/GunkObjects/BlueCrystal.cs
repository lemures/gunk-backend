using Gunk_Scripts.GunkComponents;
using Src.Maps;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class BlueCrystal : Crystal
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.CrystalBlue);

        public BlueCrystal(IGunkObjectManagerInternalData objectManagerFacade, Float3 position)
            : base(objectManagerFacade, prefabName, position, createResourcesSource())
        {
        }

        private static LimitedResourcesSource createResourcesSource()
        {
            return new LimitedResourcesSource(ResourceType.BlueCrystals, 100.0f, 20.0f);
        }
    }
}