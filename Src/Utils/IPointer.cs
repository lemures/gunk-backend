﻿using Src.ObjectsSelection;
using Src.Objects_Selection;
using Src.VectorTypes;

namespace Src.Utilities.Utils
{
    public interface IPointer
    {
        bool isPointerDragging();
        DragRectangle getDragRectangle();
        bool hasJustStoppedDragging();
        bool wasClicked();
        bool wasRightClicked();
        bool wasDoubleClicked();
        Float2 getPointerPosition();
    }
}