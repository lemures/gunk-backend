using System;
using Gunk_Scripts.GunkComponents;

namespace Src.Turrets
{
    public class TurretComponent : GunkComponent, IUpdateCallbackReceiver
    {
        private readonly ITurretAnimationSystem animationSystem;

        private float energyConsumption;

        private IDemagableTarget target;
        private readonly ITurretTargetLocator targetLocator;
        private readonly IWeapon weapon;

        public TurretComponent(IGunkObject parent, ITurretAnimationSystem animationSystem, IWeapon weapon,
            ITurretTargetLocator targetLocator) : base(parent)
        {
            this.animationSystem = animationSystem;
            this.weapon = weapon;
            this.targetLocator = targetLocator;
        }

        public void update(float deltaTime)
        {
            weapon.update(deltaTime);

            if (target == null) target = targetLocator.getTarget();
            if (target != null) animationSystem.rotateTowardsTarget(target.currentPosition);

            if (animationSystem.facesTarget() && weapon.isReady && target != null)
            {
                if (energyConsumption > 0)
                {
                    if (parent.energy.isEnergyAvailable(energyConsumption))
                        parent.energy.useEnergy(energyConsumption);
                    else
                        return;
                }

                weapon.fire(target);
                target = null;
            }
        }

        public override void destroy()
        {
            weapon.stop();
        }

        public void setEnergyConsumption(float value)
        {
            energyConsumption = value;
        }
    }
}