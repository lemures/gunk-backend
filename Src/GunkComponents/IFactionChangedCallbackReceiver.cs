using Src.Logic;

namespace Gunk_Scripts.GunkComponents
{
    public interface IFactionChangedCallbackReceiver
    {
        void changeFaction(Faction newFaction);
    }
}