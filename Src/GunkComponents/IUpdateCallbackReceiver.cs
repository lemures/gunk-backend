using System;
using Src.Turrets;

namespace Gunk_Scripts.GunkComponents
{
    public interface IUpdateCallbackReceiver
    {
        void update(float deltaTime);
    }
}