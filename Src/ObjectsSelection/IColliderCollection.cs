using Src.VectorTypes;

namespace Src.ObjectsSelection
{
    public interface IColliderCollection
    {
        bool hasCollider(int id);
        void addCollider(int id, ICollider colliderToAdd, SelectionPriority priority);
        void removeCollider(int id);
        void removeAllColliders();
        void setColliderCenter(int id, Float3 newCenter);
    }
}