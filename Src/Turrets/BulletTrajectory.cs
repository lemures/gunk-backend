using System;
using Src.VectorTypes;

namespace Src.Turrets
{
    public static class BulletTrajectory
    {
        private static Float3 getPosition(Float3 startPosition, Float3 targetPosition, float bulletSpeed, float timePassed)
        {
            var diffX = targetPosition.x - startPosition.x;
            var diffZ = targetPosition.z - startPosition.z;

            var flatDistance = Math.Sqrt(diffX * diffX + diffZ * diffZ);
            var time = flatDistance / bulletSpeed;
            
            var G = 9.81f;

            var h0 = startPosition.y;
            var h1 = targetPosition.y;
            var hFin = h0 - G * time * time / 2.0f;
            var verticalSpeed = (h1 - hFin) / time;

            var posX = startPosition.x + diffX * (timePassed / time);
            var posZ = startPosition.z + diffZ * (timePassed / time);
            var posY = verticalSpeed * timePassed + h0 - G * timePassed * timePassed / 2.0f;
            
            return new Float3((float)posX, (float)posY, (float)posZ);
        }

        public static bool hasReachedDestination(Float3 startPosition, Float3 targetPosition, float bulletSpeed,
            float timePassed)
        {
            var diffX = targetPosition.x - startPosition.x;
            var diffZ = targetPosition.z - startPosition.z;
            
            var flatDistance = Math.Sqrt(diffX * diffX + diffZ * diffZ);
            var time = flatDistance / bulletSpeed;

            return time <= timePassed;
        }
        
        public static BulletLocation calculateLocation(Float3 startPosition, Float3 targetPosition, float bulletSpeed, float timePassed)
        {
            var position1 = getPosition(startPosition, targetPosition, bulletSpeed, timePassed);
            var position2 = getPosition(startPosition, targetPosition, bulletSpeed, timePassed + 0.01f);
            return new BulletLocation(position1, Float3.Normalize(position2 - position1));
        }
    }
}