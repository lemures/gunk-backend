﻿using System;
using System.Collections.Generic;
using Src.Utils;

namespace Gunk_Scripts
{
    public class UpdateCaller<T>
    {
        private readonly List<KeyValuePair<T, Action>> updateFunctionsWithOwners =
            new List<KeyValuePair<T, Action>>();

        private readonly Func<T, bool> isObjectActiveFunction;
        private IProfiler profiler;

        public UpdateCaller(Func<T, bool> isObjectActiveFunction, IProfiler profiler)
        {
            this.isObjectActiveFunction = isObjectActiveFunction;
            this.profiler = profiler;
        }

        public void addFunction(T target, Action function)
        {
            updateFunctionsWithOwners.Add(new KeyValuePair<T, Action>(target, function));
        }

        public void invokeAll()
        {
            profiler?.beginSample("Calling update functions in updateCaller " + GetHashCode());
            for (var i = 0; i < updateFunctionsWithOwners.Count; i++)
            {
                var elem = updateFunctionsWithOwners[i];
                if (elem.Key != null && isObjectActiveFunction(elem.Key)) elem.Value();
            }

            profiler?.endSample();
        }

        public void deleteAllObjectFunctions(object target)
        {
            updateFunctionsWithOwners.RemoveAll(x => x.Key as object == target);
        }
    }
}