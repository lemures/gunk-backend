using Src.VectorTypes;

namespace Src.SkeletonAnimations
{
    public class SocketData
    {
        public Float3 position;
        public float rotation;

        public SocketData(Float3 position, float rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }
    }
}