namespace Gunk_Scripts.GunkObjects
{
    public enum GunkObjectType
    {
        Relay,
        WeldingBot,
        Worker,
        Connector,
        SmallEnergyGenerator,
        BigEnergyGenerator,
        CrystalPink,
        CrystalBlue,
        Platform,
        Manufacturer,
        LightPlasmaTank,
        LightTower,
        MinePink,
        MineBlue,
        Miner,
        Radar,
        AntiCorruptionLaser,
        AntiCorruptionMortar
    }
}