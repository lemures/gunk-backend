﻿using System;
using Src.Maps;
using Src.Turrets;
using Src.Utils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public class PlacedObject : GunkComponent, IUpdateCallbackReceiver
    {
        private readonly ObjectPattern pattern;

        public PlacedObject(IGunkObject parent, ObjectPattern objectPattern) : base(parent)
        {
            parent.objectManager.lockSpaceUnderObject(parent, objectPattern);
            pattern = objectPattern;
        }

        public void update(float deltaTime)
        {
            var positionVector3 = parent.transform.position;
            var position = new Int2((int) positionVector3.x, (int) positionVector3.z);
            if (parent.objectManager.corruptedLand.isCorruptionUnder(position, pattern))
                parent.getComponent<Health>().injure(1000.0f * deltaTime);
        }

        public override void destroy()
        {
            parent.objectManager.unlockSpaceUnderObject(parent, pattern);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(PlacedObject))
                .addVariable(pattern, nameof(pattern))
                .print();
        }
    }
}