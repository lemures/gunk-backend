﻿namespace Src.Utils
{
    public static class StringHasher
    {
        private static readonly long p1 = 1000 * 1000 * 1000 + 9;
        private static readonly long alphabetBase = 253;

        public static int calculateHash(string str)
        {
            long res = 0;
            foreach (var letter in str)
            {
                res *= alphabetBase;
                res += letter;
                res %= p1;
            }

            return (int) res;
        }
    }
}