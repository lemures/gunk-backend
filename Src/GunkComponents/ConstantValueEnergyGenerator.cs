namespace Gunk_Scripts.GunkComponents
{
    public class ConstantValueEnergyGenerator : IEnergyGenerator
    {
        public ConstantValueEnergyGenerator(float productionLevel)
        {
            this.productionLevel = productionLevel;
        }

        public float productionLevel { get; }

        public float getEnergyGenerationLevel()
        {
            return productionLevel;
        }
    }
}