using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;
using Src.Maps;
using Src.VectorTypes;

namespace Gunk_Scripts.Tests.Editor
{
    public class PlacedObjectTests
    {
        [Test]
        public void creatingObjectLocksSpace()
        {
            var shape = new ObjectPattern(3, 5);
            var surroundings = new FakeComponentSurroundings(1);
            A.CallTo(() => surroundings.fakeObject.transform.position).Returns(new Float3(1, 2, 3));
            var placedObject = new PlacedObject(surroundings.fakeObject, shape);
            A.CallTo(() => surroundings.fakeManager.lockSpaceUnderObject(surroundings.fakeObject, shape))
                .MustHaveHappenedOnceExactly();
        }

        [Test]
        public void destroyingObjectUnlocksSpace()
        {
            var shape = new ObjectPattern(3, 5);
            var surroundings = new FakeComponentSurroundings(1);
            A.CallTo(() => surroundings.fakeObject.transform.position).Returns(new Float3(1, 2, 3));
            var placedObject = new PlacedObject(surroundings.fakeObject, shape);
            placedObject.destroy();
            A.CallTo(() => surroundings.fakeManager.unlockSpaceUnderObject(surroundings.fakeObject, shape))
                .MustHaveHappenedOnceExactly();
        }
    }
}