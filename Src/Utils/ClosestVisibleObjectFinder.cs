﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Src.DataStructures;
using Src.VectorTypes;

namespace Gunk_Scripts
{
    [Serializable]
    public class ClosestVisibleObjectFinder : ISerializable
    {
        public enum Visibility
        {
            Visible,
            Invisible
        }

        //stores all objects positions
        private readonly SortedDictionary<int, ObjectData> allObjects = new SortedDictionary<int, ObjectData>();
        private readonly int areaHeight;

        //mainly for Serialization
        private readonly int areaWidth;

        private readonly List<ClosestObjectFinder> layerCollections = new List<ClosestObjectFinder>();
        private readonly Visibility[,] relationships;

        private readonly int typesCount;

        //dictionary to know what type certain object is of
        private readonly SortedDictionary<int, int> typesMap = new SortedDictionary<int, int>();

        public ClosestVisibleObjectFinder(int areaWidth, int areaHeight, int typesCount, Visibility[,] typesVisibility)
        {
            this.areaHeight = areaHeight;
            this.areaWidth = areaWidth;
            this.typesCount = typesCount;

            for (var i = 0; i < typesCount; i++) layerCollections.Add(new ClosestObjectFinder(areaWidth, areaHeight));

            relationships = (Visibility[,]) typesVisibility.Clone();
        }

        protected ClosestVisibleObjectFinder(SerializationInfo info, StreamingContext context)
        {
            areaHeight = info.GetInt32("areaHeight");
            areaWidth = info.GetInt32("areaWidth");
            typesCount = info.GetInt32("typeCount");

            for (var i = 0; i < typesCount; i++) layerCollections.Add(new ClosestObjectFinder(areaWidth, areaHeight));

            relationships = (Visibility[,]) info.GetValue("relationships", typeof(Visibility[,]));

            var typesMapKeys = (int[]) info.GetValue("typesMapKeys", typeof(int[]));
            var typesMapValues = (int[]) info.GetValue("typesMapValues", typeof(int[]));
            for (var i = 0; i < typesMapKeys.Length; i++) typesMap[typesMapKeys[i]] = typesMapValues[i];

            var allObjectValues = (ObjectData[]) info.GetValue("allObjectsValues", typeof(ObjectData[]));

            foreach (var position in allObjectValues)
                addObject(position.id, position.x, position.y, typesMap[position.id]);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("areaWidth", areaWidth);
            info.AddValue("areaHeight", areaHeight);
            info.AddValue("typeCount", typesCount);
            info.AddValue("relationships", relationships);

            var typesMapKeys = new int[typesMap.Count];
            var typesMapValues = new int[typesMap.Count];
            typesMap.Values.CopyTo(typesMapValues, 0);
            typesMap.Keys.CopyTo(typesMapKeys, 0);

            info.AddValue("typesMapKeys", typesMapKeys);
            info.AddValue("typesMapValues", typesMapValues);

            var allObjectsValues = new ObjectData[allObjects.Count];
            allObjects.Values.CopyTo(allObjectsValues, 0);
            info.AddValue("allObjectsValues", allObjectsValues);
        }

        /// <summary>
        ///     adds new object with certain id from a certain type
        /// </summary>
        public void addObject(int id, float x, float y, int type)
        {
            layerCollections[type].addObject(id, new Float2(x, y));
            typesMap[id] = type;
            allObjects[id] = new ObjectData(id, x, y);
        }

        /// <summary>
        ///     removes object with a certain id
        /// </summary>
        public void removeObject(int id)
        {
            var type = typesMap[id];
            layerCollections[type].removeObject(id);
            allObjects[id] = null;
        }

        /// <summary>
        ///     updates saved position of a certain object
        /// </summary>
        public void updatePosition(int id, float x, float y)
        {
            var type = typesMap[id];
            layerCollections[type].updatePosition(id, new Float2(x, y));
            allObjects[id] = new ObjectData(id, x, y);
        }

        /// <summary>
        ///     changes saved type of an object
        /// </summary>
        public void changeType(int id, int newtype)
        {
            var position = allObjects[id];
            removeObject(id);
            addObject(id, position.x, position.y, newtype);
        }

        /// <summary>
        ///     finds closest hostile object to the object with certain id
        /// </summary>
        public FoundObject getClosestTarget(int id, float range)
        {
            var type = typesMap[id];
            var position = allObjects[id];
            var resultId = -1;
            var closest = float.MaxValue;
            for (var i = 0; i < typesCount; i++)
            {
                var distanceToLayer = layerCollections[i].findObject(new Float2(position.x, position.y), range);
                if (distanceToLayer != null && relationships[type, i] == Visibility.Visible &&
                    distanceToLayer.distance < closest)
                {
                    closest = distanceToLayer.distance;
                    resultId = distanceToLayer.id;
                }
            }

            if (resultId == -1) return null;
            return new FoundObject(closest, resultId);
        }

        [Serializable]
        private class ObjectData
        {
            public readonly int id;
            public readonly float x;
            public readonly float y;

            public ObjectData(int id, float x, float y)
            {
                this.x = x;
                this.y = y;
                this.id = id;
            }
        }
    }
}