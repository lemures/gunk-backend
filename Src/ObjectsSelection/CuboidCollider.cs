﻿using Src.ObjectsSelection;
using Src.VectorTypes;

namespace Src.Objects_Selection
{
    public class CuboidCollider : ICollider
    {
        private readonly Float3 dimensions;

        public CuboidCollider(Float3 center, Float3 dimensions)
        {
            this.center = center;
            this.dimensions = dimensions;
        }

        public Float3 center { get; set; }

        public bool isUnderPoint(IScreenPositionCalculator positionCalculator, Float2 point)
        {
            var pointerPositionInt = new Int2((int) point.x, (int) point.y);
            var upperSide = new Float3[4];
            var lowerSide = new Float3[4];

            for (var i = 0; i < 4; i++)
            {
                upperSide[i] = center;
                lowerSide[i] = center;

                upperSide[i].y += dimensions.y;
                lowerSide[i].y -= dimensions.y;

                upperSide[i].x += (-1 + 2 * (i & 1)) * dimensions.x;
                lowerSide[i].x += (-1 + 2 * (i & 1)) * dimensions.x;

                upperSide[i].z += (-1 + (i & 2)) * dimensions.z;
                lowerSide[i].z += (-1 + (i & 2)) * dimensions.z;
            }

            var screenUpperSide = new Int2[4];
            var screenLowerSide = new Int2[4];
            for (var i = 0; i < 4; i++)
            {
                var upper = positionCalculator.worldToScreenPoint(upperSide[i]);
                var lower = positionCalculator.worldToScreenPoint(lowerSide[i]);
                screenUpperSide[i] = new Int2((int) upper.x, (int) upper.y);
                screenLowerSide[i] = new Int2((int) lower.x, (int) lower.y);
            }

            for (var i = 0; i < 2; i++)
            {
                if (triangleContains(pointerPositionInt, screenUpperSide[i * 3], screenUpperSide[1],
                    screenUpperSide[2])) return true;
                if (triangleContains(pointerPositionInt, screenLowerSide[i * 3], screenLowerSide[1],
                    screenLowerSide[2])) return true;
            }

            for (var i = 0; i < 4; i++)
            {
                if (triangleContains(pointerPositionInt, screenLowerSide[i], screenUpperSide[i],
                    screenUpperSide[(i + 3) % 4])) return true;
                if (triangleContains(pointerPositionInt, screenLowerSide[i], screenUpperSide[(i + 3) % 4],
                    screenLowerSide[(i + 3) % 4])) return true;
            }

            return false;
        }

        public bool isInScreenRectangle(Float2 leftCorner, Float2 rightCorner,
            IScreenPositionCalculator positionCalculator)
        {
            var myPos = positionCalculator.worldToScreenPoint(center);

            float temp;
            if (leftCorner.x > rightCorner.x)
            {
                temp = leftCorner.x;
                leftCorner.x = rightCorner.x;
                rightCorner.x = temp;
            }

            if (leftCorner.y > rightCorner.y)
            {
                temp = leftCorner.y;
                leftCorner.y = rightCorner.y;
                rightCorner.y = temp;
            }

            return leftCorner.x <= myPos.x &&
                   myPos.x <= rightCorner.x &&
                   leftCorner.y <= myPos.y &&
                   myPos.y <= rightCorner.y;
        }

        private static bool triangleContains(Int2 s, Int2 a, Int2 b, Int2 c)
        {
            float asX = s.x - a.x;
            float asY = s.y - a.y;

            var sAb = (b.x - a.x) * asY - (b.y - a.y) * asX > 0;

            if ((c.x - a.x) * asY - (c.y - a.y) * asX > 0 == sAb) return false;

            return (c.x - b.x) * (s.y - b.y) - (c.y - b.y) * (s.x - b.x) > 0 == sAb;
        }
        
    }
}
