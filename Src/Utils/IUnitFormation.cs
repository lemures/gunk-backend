using Src.VectorTypes;

namespace Src.Utils
{
    public interface IUnitFormation
    {
        Float3 getPosition(int id);
    }
}