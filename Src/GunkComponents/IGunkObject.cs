using Gunk_Scripts.GunkObjects;
using Gunk_Scripts.Objects_Selection;
using Src.Logic;
using Src.ObjectsSelection;
using Src.SkeletonAnimations;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkObject
    {
        IGunkObjectManagerInternalData objectManager { get; }
        IGunkTransform transform { get; }
        ISkeletonAnimator animator { get; }
        Faction faction { get; }
        IEnergy energy { get; }
        IEnergyUser currentEnergyUser { get; set; }
        IResourcesContainer resourcesContainer { get; }
        INavigationAgent agent { get; }
        ICollider collider { get; }

        int id { get; }
        float constructionProgress { get; }
        GunkObjectType type { get; }

        bool isPlayer();

        bool isDestroyed();

        void destroy();

        float getSelectionRadius();

        void addEnergyGenerator(IEnergyGenerator generator);

        float getEnergyGenerationLevel();

        float getEnergyNeeds();

        void provideWithEnergy(float amount);

        void reportConstructionFinished();

        void reportPositionChanged(Float3 newPosition);
        void update(float deltaTime);

        bool isConstructed();

        void addLog(IGunkComponent author, string message);

        T getComponent<T>() where T : class, IGunkComponent;
        void replaceAgentWithFake();
        void setCollider(ICollider collider);
    }
}