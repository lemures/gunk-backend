﻿using System.Collections.Generic;
using Src.DataStructures;
using Src.VectorTypes;

namespace Gunk_Scripts.Renderers
{
    public interface ICableRenderer
    {
        void addCable(Edge edge, Float3 startPosition, Float3 endPosition);
        void removeCable(Edge edge);
        List<Edge> getAllConnections();
        void render();
    }
}