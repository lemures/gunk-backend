namespace Src.Maps
{
    public class FlatMap : IMap
    {
        public FlatMap(int mapHeight, int mapWidth)
        {
            this.mapHeight = mapHeight;
            this.mapWidth = mapWidth;
            heightSampler = new ConstantHeightTerrainSampler();
            placedObjectsManager = new TerrainPlacedObjectsManager(mapHeight, mapWidth, heightSampler);
        }

        public ITerrainHeightSampler heightSampler { get; }
        public ITerrainPlacedObjectsManager placedObjectsManager { get; }
        public int mapHeight { get; }
        public int mapWidth { get; }
    }
}