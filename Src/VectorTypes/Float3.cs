using System;
using Newtonsoft.Json;

namespace Src.VectorTypes
{
    /// <summary>
    ///     Helper class for serialization of basic unity built-in types
    /// </summary>
    [Serializable]
    public struct Float3
    {
        public bool Equals(Float3 other)
        {
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            
            return obj is Float3 other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = x.GetHashCode();
                hashCode = (hashCode * 397) ^ y.GetHashCode();
                hashCode = (hashCode * 397) ^ z.GetHashCode();
                return hashCode;
            }
        }

        public float x;
        public float y;
        public float z;

        public Float3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString()
        {
            return "{" + x + " " + y + " " + z + " " + " }";
        }

        public static float Distance(Float3 a, Float3 b)
        {
            return (float)Math.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z));
        }
        
        public static Float3 zero => new Float3(0, 0, 0);
        
        public static Float3 one => new Float3(1, 1, 1);

        public static Float3 operator +(Float3 a, Float3 b)
        {
            return new Float3(a.x + b.x, a.y + b.y, a.z + b.z);
        }
        
        public static Float3 operator -(Float3 a, Float3 b)
        {
            return new Float3(a.x - b.x, a.y - b.y, a.z - b.z);
        }
        
        public static Float3 operator *(Float3 a, float b)
        {
            return new Float3(a.x * b, a.y * b, a.z * b);
        }
        
        public static Float3 operator *(float a, Float3 b)
        {
            return new Float3(b.x * a, b.y * a, b.z * a);
        }
        
        public static Float3 operator -(Float3 a)
        {
            return new Float3(-a.x, -a.y, -a.z);
        }
        
        public static bool operator ==(Float3 a, Float3 b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static Float3 operator /(Float3 a, float d)
        {
            return new Float3(a.x / d, a.y / d, a.z / d);
        }
        
        public static bool operator !=(Float3 a, Float3 b)
        {
            return !(a == b);
        }

        [JsonIgnore]
        public float Lenght => Distance(zero, this);

        public static float Dot(Float3 a, Float3 b) => a.x * b.x + a.y * b.y + a.z * b.z;
        
        public static Float3 Normalize(Float3 a)
        {
            return new Float3(a.x, a.y, a.z) / a.Lenght;
        }
    }
}