using Gunk_Scripts.Renderers;

namespace Src.GameRenderer
{
    public class SetCounterValue : ExtraObjectsChange
    {
        private readonly float newValue;

        private readonly int objectId;
        private readonly CounterDrawStyle style;

        public SetCounterValue(int objectId, CounterDrawStyle style, float newValue)
        {
            this.objectId = objectId;
            this.style = style;
            this.newValue = newValue;
        }

        protected bool Equals(SetCounterValue other)
        {
            return objectId == other.objectId && style == other.style && newValue.Equals(other.newValue);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((SetCounterValue) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = objectId;
                hashCode = (hashCode * 397) ^ (int) style;
                hashCode = (hashCode * 397) ^ newValue.GetHashCode();
                return hashCode;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.countersRenderer.setCounterValue(objectId, style, newValue);
        }
    }
}