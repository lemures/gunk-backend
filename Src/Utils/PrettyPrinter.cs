﻿using System.Globalization;

namespace Src.Utils
{
    public static class PrettyPrinter
    {
        /// <summary>
        ///     formats a float so that it is way more human-readable
        /// </summary>
        /// <param name="val">variable to be formatted</param>
        /// <returns>a nicely formatted float</returns>
        public static string reformatFloat(float val)
        {
            return ((int) (val * 100.0f) / 100.0f).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     formats a double so that it is way more human-readable
        /// </summary>
        /// <param name="val">variable to be formatted</param>
        /// <returns>a nicely formatted double</returns>
        public static string reformatDouble(double val)
        {
            return reformatFloat((float) val);
        }
    }
}