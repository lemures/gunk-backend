namespace Src.GameRenderer
{
    public class SetFogOfWarObjectRange : ExtraObjectsChange
    {
        public int id;
        public float range;

        public SetFogOfWarObjectRange(int id, float range)
        {
            this.id = id;
            this.range = range;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.fogOfWarObjectsCollection.setRange(id, range);
        }
    }
}