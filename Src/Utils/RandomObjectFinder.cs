using System;
using System.Collections.Generic;
using Src.Logic;
using Src.VectorTypes;

namespace Src.Libraries
{
    public class RandomObjectFinder
    {
        private readonly Random generator = new Random();

        private readonly List<ObjectData> allObjects = new List<ObjectData>();

        private void validateObjectDoesNotExist(int id)
        {
            if (allObjects.Find(x => x.id == id) != null)
                throw new InvalidOperationException("object with this id already exists");
        }

        private void validateObjectExists(int id)
        {
            if (allObjects.Find(x => x.id == id) == null)
                throw new InvalidOperationException("object with this id does not exist");
        }

        public void addObject(int id, float x, float y, Faction faction)
        {
            validateObjectDoesNotExist(id);
            allObjects.Add(new ObjectData(new Float2(x, y), id, faction));
        }

        public void removeObject(int id)
        {
            validateObjectExists(id);
            allObjects.RemoveAll(x => x.id == id);
        }

        public int findRandomObjectInRange(float x, float y, Faction faction, float maxDistance)
        {
            var matching = allObjects.FindAll(objectData => objectData.faction == faction
                                                            && objectData.position.Distance(new Float2(x, y)) <
                                                            maxDistance);
            return matching.Count == 0 ? -1 : matching[generator.Next(0, matching.Count)].id;
        }

        public class ObjectData
        {
            public Faction faction;
            public int id;
            public Float2 position;

            public ObjectData(Float2 position, int id, Faction faction)
            {
                this.position = position;
                this.id = id;
                this.faction = faction;
            }
        }
    }
}