﻿using System.Collections.Generic;
using Src.VectorTypes;

namespace Src.SkeletonAnimations
{
    public interface ISkeletonAnimator
    {
        string currentClip { get; }

        List<SocketData> getAllSocketBones();

        void playClip(string name, float argument = 0.0f);

        void changeClipArgument(float value);

        bool isCurrentClipLoaded();

        Float3 getBarrelBoneWorldPosition(int index, Float3 objectPosition);
    }
}