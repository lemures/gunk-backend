using Gunk_Scripts.GunkComponents;
using Src.GunkComponents;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class Platform : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.Platform);
        private readonly float maxHealth = 500;

        private readonly PlatformAiComponent platformAiComponent;

        public Platform(IGunkObjectManagerInternalData objectManagerFacade, Float3 position, Faction faction) : base(
            objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addPlacedObject();
            addEnergyNeededForConstruction(300.0f);
            addConnectible(new ConnectibleObject(this));
            platformAiComponent = new PlatformAiComponent(this);
            addComponent(platformAiComponent);
            addObjectToConstructionQueue(GunkObjectType.Worker);
            addObjectToConstructionQueue(GunkObjectType.WeldingBot);
            addComponent(new ProgressComponent(this));
            getComponent<ProgressComponent>().show();
            getComponent<ProgressComponent>().setValue(0.3f);
            finishAddingComponents();
        }

        public void addObjectToConstructionQueue(GunkObjectType objectType)
        {
            platformAiComponent.addObjectToQueue(objectType);
        }
    }
}