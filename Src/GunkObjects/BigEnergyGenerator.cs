using Gunk_Scripts.GunkComponents;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class BigEnergyGenerator : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.BigEnergyGenerator);
        private static readonly int maxHealth = 1000;

        public BigEnergyGenerator(IGunkObjectManagerInternalData objectManagerFacade, Float3 position, Faction faction) : base(
            objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleObject(this));
            addPlacedObject();
            addCuboidCollider(new Float3(2, 3, 2));
            addEnergyGenerator(new ConstantValueEnergyGenerator(500.0f));
            addEnergyNeededForConstruction(2000.0f);
            finishAddingComponents();
        }
    }
}