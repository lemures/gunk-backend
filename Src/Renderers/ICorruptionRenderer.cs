using System.Collections.Generic;
using Src.Maps;

namespace Src.Renderers
{
    public interface ICorruptionRenderer
    {
        void addChanges(List<CorruptedLand.FieldChange> changes);
    }
}