namespace Src.Utils
{
    public interface IProfiler
    {
        void beginSample(string name);

        void endSample();
    }
}