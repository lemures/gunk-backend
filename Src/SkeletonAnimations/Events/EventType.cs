namespace Src.SkeletonAnimations.Events
{
    public enum EventType
    {
        CustomAction,
        ClipEnded,
        ClipStartedPlaying,
        ClipFullyLoaded
    }
}