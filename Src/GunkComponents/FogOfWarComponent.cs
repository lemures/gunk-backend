using Gunk_Scripts.GunkComponents;
using Src.GameRenderer;
using Src.Logic;
using Src.Utils;
using Src.VectorTypes;

namespace Src.GunkComponents
{
    public class FogOfWarComponent : GunkComponent, IPositionChangedCallbackReceiver, IFactionChangedCallbackReceiver
    {
        private readonly float fogOfWarRange;
        private bool isEnabled;

        public FogOfWarComponent(IGunkObject parent, float fogOfWarRange = 15.0f) : base(parent)
        {
            this.fogOfWarRange = fogOfWarRange;
        }

        public void changeFaction(Faction newFaction)
        {
            if (newFaction == Faction.MainPlayer && !isEnabled)
            {
                parent.objectManager.addVisibleObjectsChange(new AddFogOfWarObject(parent.id, parent.transform.position));
                parent.objectManager.addVisibleObjectsChange(new SetFogOfWarObjectRange(parent.id, fogOfWarRange));
                isEnabled = true;
            }
        }

        public void changePosition(Float3 newPosition)
        {
            if (isEnabled)
            {
                parent.objectManager.addVisibleObjectsChange(new UpdateFogOfWarPosition(parent.id, newPosition));
            }
        }

        public override void destroy()
        {
            if (isEnabled)
            {
                parent.objectManager.addVisibleObjectsChange(new RemoveFogOfWarObject(parent.id));
            }
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(FogOfWarComponent))
                .addVariable(fogOfWarRange, nameof(fogOfWarRange))
                .print();
        }
    }
}