using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddManufacturingRay : ExtraObjectsChange
    {
        private readonly Float3 end;

        private readonly int objectId;
        private readonly Float3 start;

        public AddManufacturingRay(int objectId, Float3 start, Float3 end)
        {
            this.objectId = objectId;
            this.start = start;
            this.end = end;
        }

        protected bool Equals(AddManufacturingRay other)
        {
            return objectId == other.objectId && start.Equals(other.start) && end.Equals(other.end);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((AddManufacturingRay) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = objectId;
                hashCode = (hashCode * 397) ^ start.GetHashCode();
                hashCode = (hashCode * 397) ^ end.GetHashCode();
                return hashCode;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.miningRayRenderer.addObject(objectId, start, end);
        }
    }
}