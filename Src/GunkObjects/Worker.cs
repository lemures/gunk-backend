using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class Worker : GunkObject
    {
        private static readonly string PrefabName = GunkPrefabNames.getName(GunkObjectType.Worker);

        private MiningBotNavmeshAi aiComponent;
        private readonly float maxHealth = 200.0f;

        public Worker(IGunkObjectManagerInternalData objectManagerFacade, Float3 position, Faction faction) : base(
            objectManagerFacade, PrefabName, position)
        {
            setFaction(faction);
            addNavmeshAgent();
            addCuboidCollider(Float3.one);
            selectionPriority = SelectionPriorityUtils.getWorkerSelectionPriority();
            addMiningBotAi();
            addEnergyNeededForConstruction(200.0f);
            addHealth(maxHealth, maxHealth);
            finishAddingComponents();
        }

        private void addMiningBotAi()
        {
            aiComponent = new MiningBotNavmeshAi(this);
            addComponent(aiComponent);
        }

        public void giveOrderToMine(int resourceId)
        {
            aiComponent.goMineSpecificResource(resourceId);
        }
    }
}