﻿using System.Collections.Generic;
using Gunk_Scripts.GunkComponents;
using Src.Libraries;
using Src.ShaderUtils;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Events;
using Src.SkeletonAnimations.Internal;
using Src.Utils;
using Src.VectorTypes;

namespace Src.GunkComponents
{
    public class SkeletonAnimatorComponent : GunkComponent, ISkeletonAnimator
    {
        private readonly SkeletonAnimator animator;

        public SkeletonAnimatorComponent(IGunkObject parent, string prefabName, IRenderersParametersManager objectManager, IAssetManager assetManager,
            int objectId, string startClipName, IAnimatorsManager animatorsManager) : base(parent)
        {
            animator = new SkeletonAnimator(
                prefabName, 
                objectManager,
                new AnimationClipDatabase(assetManager), 
                assetManager, 
                objectId,
                animatorsManager,
                startClipName);
        }

        public void playClip(string name, float argument = 0)
        {
            animator.playClip(name, argument);
        }

        public string currentClip => animator.currentClip;

        public List<SocketData> getAllSocketBones()
        {
            return animator.getAllSocketBones();
        }

        public void changeClipArgument(float value)
        {
            animator.changeClipArgument(value);
        }

        public Float3 getBarrelBoneWorldPosition(int index, Float3 objectPosition)
        {
            return animator.getBarrelBoneWorldPosition(index, objectPosition);
        }

        public bool isCurrentClipLoaded()
        {
            return animator.isCurrentClipLoaded();
        }

        public List<EventData> collectTriggeredEvents()
        {
            return animator.collectTriggeredEvents();
        }

        public void updateAnimation(float timeElapsed)
        {
            animator.updateAnimation(timeElapsed);
        }

        public override void destroy()
        {
            animator.destroy();
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(SkeletonAnimatorComponent))
                .addVariable(animator, nameof(animator))
                .print();
        }
    }
}