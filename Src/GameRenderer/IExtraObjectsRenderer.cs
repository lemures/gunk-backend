using Gunk_Scripts.Renderers;
using Src.Renderers;
using Src.ShaderUtils;

namespace Src.GameRenderer
{
    public interface IExtraObjectsRenderer : IObjectsChangesReceiver
    {
        void render();

        void enableRendering();

        void disableRendering();
        ICableRenderer cableRenderer { get; }
        ICountersCollection countersRenderer { get; }
        ILineRenderer generalPurposeLineRenderer { get; }
        ILineRenderer manufacturingRayRenderer { get; }
        ILineRenderer miningRayRenderer { get; }
        IFogOfWarObjectsCollection fogOfWarObjectsCollection { get; }
        ICorruptionRenderer corruptionRenderer { get; }
        IPrefabRenderer prefabRenderer { get; }
        IRenderersParametersManager renderersParametersManager { get; }
    }
}