using Gunk_Scripts.GunkComponents;
using Src.Logic;
using Src.Turrets;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class AntiCorruptionLaser : GunkObject
    {
        private static readonly string prefabName = GunkPrefabNames.getName(GunkObjectType.AntiCorruptionLaser);
        private static readonly int maxHealth = 1000;

        public AntiCorruptionLaser(IGunkObjectManagerInternalData objectManagerFacade, Float3 position, Faction faction) : base(
            objectManagerFacade, prefabName, position, "")
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addPlacedObject();
            addEnergyNeededForConstruction(500.0f);
            addConnectible(new ConnectibleObject(this));
            addEnergy(0, 1000, 500.0f);
            addEnergyNeededForConstruction(1000.0f);

            var turretRotator = new SimpleTurretRotator(this);
            var calculator = new GunkObjectBarrelPositionCalculator(this);
            var weapon = new LaserWeapon(calculator, objectManager.objectsChangesReceiver);
            var locator = new ClosestCorruptedFieldLocator(this, 20.0f);
            var turret = new TurretComponent(this, turretRotator, weapon, locator);
            turret.setEnergyConsumption(100.0f);
            addComponent(turretRotator);
            addComponent(turret);

            finishAddingComponents();
        }
    }
}