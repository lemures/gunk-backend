namespace Src.SkeletonAnimations.Internal
{
    public interface IAnimationClipDatabase
    {
        SkeletonAnimationClip getClip(string name);
        void saveClipChangesToDisk(string name);
    }
}