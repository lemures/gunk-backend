﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts;
using Newtonsoft.Json;
using Src.SkeletonAnimations.BoneTypes;
using Src.Utils;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.Internal
{
    [Serializable]
    public class Skeleton
    {
        private IAssetManager assetManager;
        
        public Skeleton(string skeletonName, IAssetManager assetManager)
        {
            this.assetManager = assetManager;
            revertFromFile(skeletonName);
            if (skeletonData == null) skeletonData = new SkeletonData();
            if (skeletonData.getAllBones().Find(x => x is SocketBone) == null)
            {
                skeletonData.addNextSocketBone(Float3.zero);
            }
        }

        public Skeleton(IAssetManager assetManager)
        {
            this.assetManager = assetManager;
            skeletonData = new SkeletonData();
            if (skeletonData.getAllBones().Find(x => x is SocketBone) == null)
            {
                skeletonData.addNextSocketBone(Float3.zero);
            }
        }

        public SkeletonData skeletonData { get; private set; }

        public static Skeleton constructSkeletonWithSingleSocketBone(IAssetManager assetManager)
        {
            var result = new Skeleton(assetManager);
            result.skeletonData.addNextSocketBone(Float3.zero);
            return result;
        }

        

        private static string getDatabaseName(string skeletonName)
        {
            return skeletonName + "_skeletonData";
        }

        /// <summary>
        ///     saves internalData on disk, saved instance are identified by skeletonName
        /// </summary>
        /// <param name="skeletonName"></param>
        public void saveSkeletonDataToFile(string skeletonName)
        {
            assetManager.saveObjectAsJson(skeletonData, getDatabaseName(skeletonName), "skeletons");
        }

        public void deleteSavedData(string skeletonName)
        {
            assetManager.deleteAllVersions(getDatabaseName(skeletonName), "skeletons");
        }

        public bool isDatabaseValid(string databaseName)
        {
            return assetManager.getObjectFromJson(databaseName, typeof(SkeletonData), "skeletons") is SkeletonData;
        }

        /// <summary>
        ///     loads from disk last data that were saved under this name and returns true if was successfully imported
        /// </summary>
        public bool revertFromFile(string skeletonName)
        {
            skeletonData =
                assetManager.getObjectFromJson(getDatabaseName(skeletonName), typeof(SkeletonData), "skeletons") as SkeletonData;
            if (skeletonData != null)
            {
//                var json = JsonConvert.SerializeObject(skeletonData, Formatting.Indented);
            }
            
            if (skeletonData?.getAllBones().Find(x => x is SocketBone) == null)
            {
                skeletonData?.addNextSocketBone(Float3.zero);
            }

            return skeletonData != null;
        }

        public List<SocketData> getAllSocketBones()
        {
            var result =
                from bone in skeletonData.getAllBones()
                where bone is SocketBone
                select new SocketData(bone.position, (bone as SocketBone).rotation);
            return result.ToList();
        }

        public override string ToString()
        {
            return skeletonData.ToString();
        }
    }
}