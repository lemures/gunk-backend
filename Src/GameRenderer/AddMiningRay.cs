using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddMiningRay : ExtraObjectsChange
    {
        private readonly Float3 end;

        private readonly int id;
        private readonly Float3 start;


        public AddMiningRay(int id, Float3 start, Float3 end)
        {
            this.id = id;
            this.start = start;
            this.end = end;
        }

        protected bool Equals(AddMiningRay other)
        {
            return id == other.id && start.Equals(other.start) && end.Equals(other.end);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((AddMiningRay) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = id;
                hashCode = (hashCode * 397) ^ start.GetHashCode();
                hashCode = (hashCode * 397) ^ end.GetHashCode();
                return hashCode;
            }
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.miningRayRenderer.addObject(id, start, end);
        }
    }
}