namespace Src.GameRenderer
{
    public interface IObjectsChangesReceiver
    {
        void sendChange(ExtraObjectsChange change);
    }
}