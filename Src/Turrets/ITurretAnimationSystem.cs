using Src.VectorTypes;

namespace Src.Turrets
{
    public interface ITurretAnimationSystem
    {
        void rotateTowardsTarget(Float3 targetPosition);
        void cancelTarget();
        bool facesTarget();
    }
}