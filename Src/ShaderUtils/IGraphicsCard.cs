using System;

namespace Src.ShaderUtils
{
    public interface IGraphicsCard
    {
        void setFloat(string name, float val);

        void setInt(string name, int val);

        void dispatchCompute(string name, int x = 1, int y = 1, int z = 1);

        void setBufferData(string name, Array data);

        void initializeEmptyBuffer(string name, int bufferSize, int stride);
    }
}