using NUnit.Framework;
using Src.DataStructures;
using Src.Maps;
using Src.SerializationUtils;
using Src.VectorTypes;

namespace Src.Turrets.Tests.Editor
{
    public class CorruptedFieldTargetTests
    {
        [Test]
        public void rightFieldGetsDamaged()
        {
            var corruptedLand = new CorruptedLand(10, 10);
            corruptedLand.infectField(new Int2(5, 3));
            IIdentityProvider identityProvider = new IdManager();

            var target = new CorruptedFieldTarget(new Float3(5.5f, 7.7f, 3.4f), corruptedLand);
            target.damage(10.0f);

            Assert.IsFalse(corruptedLand.isCorrupted(new Int2(5, 3)));
        }
    }
}