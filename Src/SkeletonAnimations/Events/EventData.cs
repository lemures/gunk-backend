namespace Src.SkeletonAnimations.Events
{
    public class EventData
    {
        public float timeHappened;
        public EventType type;
    }
}