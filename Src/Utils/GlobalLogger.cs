using System;

namespace Src.Utils
{
    public static class GlobalLogger
    {
        public static Action<string> logAction = Console.Out.WriteLine;
        
        public static void log(string message)
        {
            logAction.Invoke(message);
        }
    }
}