namespace Src.GameRenderer
{
    public class DeleteMainModel : ExtraObjectsChange
    {
        private int id;

        public DeleteMainModel(int id)
        {
            this.id = id;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.prefabRenderer.destroyObject(id);
        }
    }
}