using Src.VectorTypes;

namespace Src.ObjectsSelection
{
    public interface IScreenPositionCalculator
    {
        Float2 worldToScreenPoint(Float3 worldPosition);
    }
}