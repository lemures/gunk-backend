using FakeItEasy;
using NUnit.Framework;
using Src.GameRenderer;
using Src.SkeletonAnimations;
using Src.VectorTypes;

namespace Src.Turrets.Tests.Editor
{
    public class LaserWeaponTests
    {
        private readonly Float3 barrelPosition = new Float3(1, 3, 5);
        private readonly Float3 targetPosition = new Float3(2, 4, 6);
        private readonly Float3 targetSecondPosition = new Float3(2, 5, 6);
        private IDemagableTarget demagableTarget;
        private IBarrelPositionCalculator fakePositionCalculator;
        private IObjectsChangesReceiver objectsChangesReceiver;
        private LaserWeapon weapon;

        [SetUp]
        public void setUp()
        {
            fakePositionCalculator = A.Fake<IBarrelPositionCalculator>();
            A.CallTo(() => fakePositionCalculator.getBarrelBoneWorldPosition(A<int>.Ignored))
                .Returns(barrelPosition);
            objectsChangesReceiver = A.Fake<IObjectsChangesReceiver>();
            demagableTarget = A.Fake<IDemagableTarget>();
            A.CallTo(() => demagableTarget.currentPosition).Returns(targetPosition);
            weapon = new LaserWeapon(fakePositionCalculator, objectsChangesReceiver);
        }

        [Test]
        public void laserIsSpawnedWhenFireIsCalled()
        {
            weapon.fire(demagableTarget);

            A.CallTo(() => objectsChangesReceiver.sendChange(
                    A<ExtraObjectsChange>.That.IsEqualTo(
                        new AddLaser(weapon.GetHashCode(), barrelPosition, targetPosition))))
                .MustHaveHappened();
        }

        [Test]
        public void weaponIsNotReadyAfterFiring()
        {
            weapon.fire(demagableTarget);

            Assert.IsFalse(weapon.isReady);
        }

        [Test]
        public void weaponIsReadyImmediatelyAfterCreation()
        {
            Assert.IsTrue(weapon.isReady);
        }

        [Test]
        public void weaponIsReadyAFewSecondsAfterFiring()
        {
            weapon.fire(demagableTarget);
            weapon.update(10.0f);
            Assert.IsTrue(weapon.isReady);
        }

        [Test]
        public void updateChangesLaserPosition()
        {
            weapon.fire(demagableTarget);
            A.CallTo(() => demagableTarget.currentPosition).Returns(targetSecondPosition);
            weapon.update(0f);
            A.CallTo(() => objectsChangesReceiver.sendChange(
                    A<ExtraObjectsChange>.That.IsEqualTo(
                        new RemoveLaser(weapon.GetHashCode()))))
                .MustHaveHappenedOnceExactly();
            A.CallTo(() => objectsChangesReceiver.sendChange(
                    A<ExtraObjectsChange>.That.IsEqualTo(
                        new AddLaser(weapon.GetHashCode(), barrelPosition, targetSecondPosition))))
                .MustHaveHappened();
        }

        [Test]
        public void laserIsDestroyedAfterSomeTime()
        {
            weapon.fire(demagableTarget);
            A.CallTo(() => demagableTarget.currentPosition).Returns(targetSecondPosition);
            weapon.update(10.0f);
            A.CallTo(() => objectsChangesReceiver.sendChange(
                    A<ExtraObjectsChange>.That.IsEqualTo(
                        new RemoveLaser(weapon.GetHashCode()))))
                .MustHaveHappenedOnceExactly();
        }

        [Test]
        public void stopDestroysLaser()
        {
            weapon.fire(demagableTarget);
            A.CallTo(() => demagableTarget.currentPosition).Returns(targetSecondPosition);
            weapon.stop();
            A.CallTo(() =>
                    objectsChangesReceiver.sendChange(A<ExtraObjectsChange>.That.IsInstanceOf(typeof(RemoveLaser))))
                .MustHaveHappened();
        }

        [Test]
        public void laserIsNotSpawnedIfWeaponIsNotFired()
        {
            weapon.update(10.0f);
            A.CallTo(() => objectsChangesReceiver.sendChange(A<ExtraObjectsChange>.Ignored)).MustNotHaveHappened();
        }

        [Test]
        public void noChangesAreSendIfStopIcCalledWithNoTarget()
        {
            weapon.stop();
            weapon.update(10.0f);
            A.CallTo(() => objectsChangesReceiver.sendChange(A<ExtraObjectsChange>.Ignored))
                .MustNotHaveHappened();
        }
    }
}