using Src.Maps;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class GatheredResources
    {
        public readonly float amount;
        public readonly ResourceType type;

        public GatheredResources(ResourceType type, float amount)
        {
            this.type = type;
            this.amount = amount;
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(GatheredResources))
                .addVariable(type, nameof(type))
                .addVariable(amount, nameof(amount))
                .print();
        }
    }
}