using System;
using System.Collections.Generic;
using Src.VectorTypes;

namespace Src.Maps
{
    [Serializable]
    public class ObjectPattern
    {
        private ObjectPattern(List<Field> points)
        {
            fieldsLocations = points;
        }

        public ObjectPattern(List<Int2> points)
        {
            fieldsLocations = new List<Field>();
            points.ForEach(x => fieldsLocations.Add(new Field(x.x, x.y)));
        }

        public ObjectPattern(Float3 boundsMin, Float3 boundsMax)
        {
            addFieldsInBounds(boundsMin, boundsMax);
        }

        /// <summary>
        ///     creates shape with center in (0,0) and with it's fields lying in rectangle. Rectangle sides must be odd.
        /// </summary>
        /// <exception cref="InvalidOperationException">if sides are not odd</exception>
        public ObjectPattern(int width, int height)
        {
            if (height % 2 == 0 || width % 2 == 0) throw new InvalidOperationException("sides must be odd");

            var lowerLeftVertex = new Int2(-width / 2, -height / 2);
            addFieldsInRectangle(lowerLeftVertex, width, height);
        }

        public List<Field> fieldsLocations { get; } = new List<Field>();

        private void addFieldsInBounds(Float3 boundsMin, Float3 boundsMax)
        {
            var minX = (int) Math.Floor(boundsMin.x);
            var minZ = (int) Math.Floor(boundsMin.z);
            var maxX = (int) Math.Ceiling(boundsMax.x);
            var maxZ = (int) Math.Ceiling(boundsMax.z);

            addFieldsInRectangle(new Int2(minX, minZ), maxX - minX, maxZ - minZ);
        }

        private void addFieldsInRectangle(Int2 lowerLeftVertex, int width, int height)
        {
            for (var i = lowerLeftVertex.x; i < lowerLeftVertex.x + width; i++)
            for (var q = lowerLeftVertex.y; q < lowerLeftVertex.y + height; q++)
                fieldsLocations.Add(new Field(i, q));
        }

        private static Int2 rotatePoint(Int2 pointToRotate, float angleInDegrees)
        {
            var angleInRadians = angleInDegrees * (Math.PI / 180);
            var cosTheta = Math.Cos(angleInRadians);
            var sinTheta = Math.Sin(angleInRadians);
            return new Int2
            (
                (int) Math.Round(cosTheta * pointToRotate.x - sinTheta * pointToRotate.y),
                (int) Math.Round(sinTheta * pointToRotate.x + cosTheta * pointToRotate.y)
            );
        }

        public ObjectPattern translate(Int2 translationVector)
        {
            return mapValues(this, field => new Field(field.type, field.position + translationVector));
        }

        public ObjectPattern rotate(float anglesInDegrees)
        {
            return mapValues(this, field => new Field(field.type, rotatePoint(field.position, anglesInDegrees)));
        }

        private ObjectPattern mapValues(ObjectPattern pattern, Func<Field, Field> transformationFunction)
        {
            var original = pattern.fieldsLocations;
            var result = new List<Field>();

            original.ForEach(x => result.Add(transformationFunction(x)));
            return new ObjectPattern(result);
        }

        public void setFieldType(Int2 position, TerrainFieldType newType)
        {
            if (!fieldsLocations.Exists(x => x.position == position))
                throw new ArgumentException("field with certain position does not exist");

            var foundIndex = fieldsLocations.FindIndex(field => field.position == position);
            fieldsLocations[foundIndex] = new Field(newType, fieldsLocations[foundIndex].position);
        }

        public override string ToString()
        {
            var result = "total fields count: " + fieldsLocations.Count + "\n";
            fieldsLocations.ForEach(field => result += field + "\n");
            return result;
        }

        public struct Field
        {
            public TerrainFieldType type;

            public int x { get; }
            public int y { get; }

            public Int2 position => new Int2(x, y);

            public Field(TerrainFieldType type, Int2 position)
            {
                this.type = type;
                x = position.x;
                y = position.y;
            }

            public Field(TerrainFieldType type, int x, int y)
            {
                this.type = type;
                this.x = x;
                this.y = y;
            }

            public Field(int x, int y)
            {
                this.x = x;
                this.y = y;
                type = TerrainFieldType.Ground;
            }

            public override string ToString()
            {
                return "(" + x + "," + y + ")" + "  " + type;
            }
        }
    }
}