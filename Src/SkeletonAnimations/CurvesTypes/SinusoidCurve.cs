using System;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace Src.SkeletonAnimations.CurvesTypes
{
    [Guid("2e257716-f4d9-42d7-9559-1f3e39cd97cc")]
    [Serializable]
    public class SinusoidCurve : IAnimationCurve
    {
        [JsonProperty]
        private readonly float multiplier;
        
        [JsonProperty]
        private readonly float offset;

        /// <summary>
        ///     creates curve that evaluates to multiplier*sin(time)+offset
        /// </summary>
        public SinusoidCurve(float multiplier, float offset)
        {
            this.multiplier = multiplier;
            this.offset = offset;
        }


        public float evaluate(float time, float argument)
        {
            return multiplier * (float) Math.Sin(time) + offset;
        }
    }
}