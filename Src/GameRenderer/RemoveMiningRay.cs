namespace Src.GameRenderer
{
    public class RemoveMiningRay : ExtraObjectsChange
    {
        public readonly int id;

        public RemoveMiningRay(int id)
        {
            this.id = id;
        }

        protected bool Equals(RemoveMiningRay other)
        {
            return id == other.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((RemoveMiningRay) obj);
        }

        public override int GetHashCode()
        {
            return id;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.miningRayRenderer.removeObject(id);
        }
    }
}