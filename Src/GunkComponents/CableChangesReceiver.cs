using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using Src.DataStructures;
using Src.GameRenderer;
using Src.VectorTypes;

namespace Src.GunkComponents
{
    public class CableChangesReceiver : ICableCollection
    {
        private readonly IGunkObjectManagerInternalData parent;

        public CableChangesReceiver(IGunkObjectManagerInternalData parent)
        {
            this.parent = parent;
        }

        public void addCable(Edge edge, Float3 startPosition, Float3 endPosition)
        {
            parent.addVisibleObjectsChange(new AddCable(edge, startPosition, endPosition));
        }

        public void removeCable(Edge edge)
        {
            parent.addVisibleObjectsChange(new DeleteCable(edge));
        }
    }
}