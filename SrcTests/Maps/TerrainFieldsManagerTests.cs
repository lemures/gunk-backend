using NUnit.Framework;
using Src.VectorTypes;

namespace Src.Maps.Tests.Editor
{
    [TestFixture]
    public class TerrainFieldsManagerTests
    {
        private static void setFieldTypesMatchingShape(Int2 center, ObjectPattern pattern,
            TerrainFieldsManager terrainFieldsManager)
        {
            foreach (var field in pattern.fieldsLocations)
            {
                var location = center + new Int2(field.x, field.y);
                terrainFieldsManager.changeFieldType(new Int2(location.x, location.y), field.type);
            }
        }


        [TestCase(1, 1)]
        [TestCase(31, 5)]
        [TestCase(5, 101)]
        [TestCase(101, 101)]
        public void addingObjectLocksSpaceSuccessfully(int objectHeight, int objectWidth)
        {
            var placedManager = new TerrainFieldsManager(101, 101);
            var shape = new ObjectPattern(objectHeight, objectWidth);
            var center = new Int2(50, 50);
            placedManager.addObject(center, shape);
            Assert.IsFalse(placedManager.canObjectBeAdded(center, shape));
        }

        [TestCase(1, 1)]
        [TestCase(31, 5)]
        [TestCase(5, 101)]
        [TestCase(101, 101)]
        public void removingObjectUnlocksSpaceSuccessfully(int objectHeight, int objectWidth)
        {
            var placedManager = new TerrainFieldsManager(101, 101);
            var shape = new ObjectPattern(1, 1);
            var center = new Int2(50, 50);
            placedManager.addObject(center, shape);
            placedManager.removeObject(center, shape);
            Assert.IsTrue(placedManager.canObjectBeAdded(center, shape));
        }

        [Test]
        public void calculateClosestAvailablePositionFindsMatch()
        {
            var placedManager = new TerrainFieldsManager(100, 100);
            placedManager.changeFieldType(new Int2(50, 50), TerrainFieldType.Cliff);

            var shape = new ObjectPattern(1, 1);
            var center = new Int2(47, 54);
            shape.setFieldType(new Int2(0, 0), TerrainFieldType.Cliff);

            var match = placedManager.calculateClosestAvailablePosition(center, shape);

            Assert.NotNull(match);
            Assert.AreEqual(match.Value, new Int2(50, 50));
        }

        [Test]
        public void calculateClosestAvailablePositionReturnsNullIfNoMatchesExist()
        {
            var placedManager = new TerrainFieldsManager(100, 100);

            var shape = new ObjectPattern(1, 1);
            var center = new Int2(47, 54);
            shape.setFieldType(Int2.zero, TerrainFieldType.Cliff);

            var match = placedManager.calculateClosestAvailablePosition(center, shape);

            Assert.IsNull(match);
        }

        [Test]
        public void objectWithMixedFieldTypesCanBePlaced()
        {
            var placedManager = new TerrainFieldsManager(100, 100);
            var shape = new ObjectPattern(3, 3);

            var center = new Int2(50, 50);

            var offset1 = new Int2(-1, -1);
            var offset2 = new Int2(-1, 1);
            var offset3 = new Int2(1, -1);
            var offset4 = new Int2(1, 1);

            shape.setFieldType(offset1, TerrainFieldType.Cliff);
            shape.setFieldType(offset2, TerrainFieldType.Water);
            shape.setFieldType(offset3, TerrainFieldType.CrystalMine);
            shape.setFieldType(offset4, TerrainFieldType.Cliff);

            setFieldTypesMatchingShape(new Int2(center.x, center.y), shape, placedManager);
            Assert.IsTrue(placedManager.canObjectBeAdded(center, shape));
        }

        [Test]
        public void singleFieldObjectAndRightFieldTypeCanBePlaced()
        {
            var placedManager = new TerrainFieldsManager(100, 100);
            placedManager.changeFieldType(new Int2(50, 50), TerrainFieldType.Cliff);
            var shape = new ObjectPattern(1, 1);
            var center = new Int2(50, 50);
            shape.setFieldType(Int2.zero, TerrainFieldType.Cliff);
            Assert.IsTrue(placedManager.canObjectBeAdded(center, shape));
        }

        [Test]
        public void singleFieldObjectAndWrongFieldTypeCannotBePlaced()
        {
            var placedManager = new TerrainFieldsManager(100, 100);
            var shape = new ObjectPattern(1, 1);
            var center = new Int2(50, 50);
            shape.setFieldType(Int2.zero, TerrainFieldType.Cliff);
            Assert.IsFalse(placedManager.canObjectBeAdded(center, shape));
        }
    }
}