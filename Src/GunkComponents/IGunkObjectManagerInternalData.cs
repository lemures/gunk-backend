using System.Collections.Generic;
using Gunk_Scripts.Renderers;
using Src.GameRenderer;
using Src.GunkComponents;
using Src.Libraries;
using Src.Logic;
using Src.Maps;
using Src.SkeletonAnimations;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkObjectManagerInternalData
    {
        IGunkObjectManager parent { get; }

        IConnectablesNetwork connectablesNetwork { get; }
        INavigationAgentProducer agentProducer { get; }
        int mapHeight { get; }
        int mapWidth { get; }

        Dictionary<string, object> otherVariables { get; }
        CorruptedLand corruptedLand { get; }
        IMap map { get; }
        IObjectsChangesReceiver objectsChangesReceiver { get; }
        void lockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern buildingPattern);
        void unlockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern buildingPattern);

        void addVisibleObjectsChange(ExtraObjectsChange change);

        void addComponent(int id, IGunkComponent component);

        void setSpecialFieldType(Float3 position, TerrainFieldType type);
        void removeComponent(int id, IGunkComponent component);

        IGunkTransform instantiatePrefab(IGunkObject gunkObject, string prefabName, Float3 positions,
            Float3 eulerRotation);

        int registerGunkObject(IGunkObject objectToRegister);

        void destroyObject(int id);
        ISkeletonAnimator instantiateAnimator(IGunkObject gunkObject, string prefabName, string startClipName);
        List<List<int>> getAllConnectibleNetworks();

        ObjectPattern calculatePrefabMeshDimensions(string prefabName);
        IGunkObject getObjectWithId(int id);
        void addLog(int id, string author, string message);
        void addResources(Faction player, GatheredResources resources);
    }
}