﻿namespace Gunk_Scripts.GunkComponents
{
    public interface IEnergy
    {
        void chargeEnergy(float amount);
        float getCurrentValue();
        void useEnergy(float amount);
        bool isEnergyAvailable(float neededEnergy);
    }
}