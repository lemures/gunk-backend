using System;
using System.Security.Cryptography;
using FakeItEasy;
using Newtonsoft.Json;
using NUnit.Framework;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;
using Src.Utils;
using Src.VectorTypes;

namespace SrcTests.SkeletonAnimations
{
    [NeedsClip("blah")]
    public class SkeletonAnimationClipTests
    {
        [Test]
        public void clipsCanBeSerialized()
        {
           var database = new AnimationClipDatabase(A.Fake<IAssetManager>());
           var clip = database.getClip("blah");
           var turretBone = new BasicBone(0, 0, Float3.zero);
           clip.addOrReplaceCurve(SkeletonData.getBoneId(turretBone),
               SkeletonAnimationClip.BoneAnimationParameter.XRotation, new ConstantValueCurve(0));

            var serialized = JsonConvert.SerializeObject(clip,Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            });;

            var deserialized = JsonConvert.DeserializeObject<SkeletonAnimationClip>(serialized, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            });;
            deserialized.evaluateAtPoint(2.0f); 
        }
    }
}