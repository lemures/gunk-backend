using Src.VectorTypes;

namespace Src.Maps
{
    public interface ITerrainHeightSampler
    {
        float sampleHeight(Float3 position);

        Float3 correctHeight(Float3 position);
    }
}