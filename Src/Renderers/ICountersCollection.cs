﻿using Gunk_Scripts.GunkComponents;
using Src.VectorTypes;

namespace Gunk_Scripts.Renderers
{
    public interface ICountersCollection
    {
        void addHiddenCounter(int objectId, Counter counter, CounterDrawStyle style);
        void showCounter(int objectId, CounterDrawStyle style);
        void hideCounter(int objectId, CounterDrawStyle style);
        void setObjectCountersPositions(int objectId, Float3 newWorldPosition);
        void hideAndResetObjectCounters(int objectId);
        void setCounterValue(int objectId, CounterDrawStyle style, float newValue);
        void setCounterMaxValue(int objectId, CounterDrawStyle style, float newMaxValue);
    }
}