using System.Collections.Generic;
using Gunk_Scripts.GunkObjects;
using Src.GunkObjectManager;
using Src.Logic;
using Src.SerializationUtils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkObjectManager
    {
        //IGunkObjectManagerInternalData getInternalData(IGunkObject gunkObject);

        List<GunkObjectData> getObjectsData();

        void spawnObject(GunkObjectType type, Float3 position, Faction faction);

//TODO remove this or uncomment
/*
        bool isAnythingSelected();

        void enableSelectingObjects();

        void disableSelectingObjects();

        void temporarilyConnectBuilding(List<SocketData> socketBones, Vector3 position, Faction faction);

        bool canConnectBuilding(Vector3 position, Faction faction);
        bool canPlaceObject(Vector3 position, ObjectPattern pattern);
        ObjectPattern calculatePrefabMeshDimensions(string prefabName);
*/
    }
}