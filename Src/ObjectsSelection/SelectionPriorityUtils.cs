using Gunk_Scripts.GunkObjects;
using Src.ObjectsSelection;
using Src.Objects_Selection;

namespace Gunk_Scripts.Objects_Selection
{
    public static class SelectionPriorityUtils
    {
        public static SelectionPriority getUnitSelectionPriority()
        {
            return SelectionPriority.VeryHigh;
        }

        public static SelectionPriority getWorkerSelectionPriority()
        {
            return SelectionPriority.High;
        }

        public static SelectionPriority getWeldingBotSelectionPriority()
        {
            return SelectionPriority.Medium;
        }

        public static SelectionPriority getBuildingSelectionPriority()
        {
            return SelectionPriority.Low;
        }

        public static SelectionPriority getNonSpecifiedGroupSelectionPriority()
        {
            return SelectionPriority.VeryLow;
        }

        public static SelectionPriority getPriority(GunkObjectType type)
        {
            switch (type)
            {
                case GunkObjectType.Worker:
                    return getWorkerSelectionPriority();
                case GunkObjectType.WeldingBot:
                    return getWeldingBotSelectionPriority();
                default:
                    return getNonSpecifiedGroupSelectionPriority();
            }
        }
    }
}