using Gunk_Scripts.GunkComponents;
using Src.SkeletonAnimations;
using Src.VectorTypes;

namespace Src.Turrets
{
    public class GunkObjectBarrelPositionCalculator : IBarrelPositionCalculator
    {
        private readonly IGunkObject parent;

        public GunkObjectBarrelPositionCalculator(IGunkObject parent)
        {
            this.parent = parent;
        }

        public Float3 getBarrelBoneWorldPosition(int index)
        {
            return parent.animator.getBarrelBoneWorldPosition(index, parent.transform.position);
        }
    }
}