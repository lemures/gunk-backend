﻿using System.Collections.Generic;
using Src.VectorTypes;

namespace Gunk_Scripts.Renderers
{
    public interface ILineRenderer
    {
        void render();

        void addObject(int id, Float3 startPosition, Float3 endPosition);

        void updatePosition(int id, Float3 newStart, Float3 newEnd);

        void removeObject(int id);

        List<int> getExistingObjectsIds();

        Float3 getStartPosition(int id);

        Float3 getEndPosition(int id);
    }
}