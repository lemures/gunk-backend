using Src.Maps;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    public class InfiniteResourcesSource : IResourcesContainer
    {
        private readonly float amountGatheredPerSecond;

        public InfiniteResourcesSource(ResourceType resourceType, float amountGatheredPerSecond)
        {
            type = resourceType;
            this.amountGatheredPerSecond = amountGatheredPerSecond;
        }

        public bool isDepleted()
        {
            return false;
        }

        public GatheredResources gatherResources(float deltaTime)
        {
            return new GatheredResources(type, amountGatheredPerSecond * deltaTime);
        }

        public ResourceType type { get; }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(InfiniteResourcesSource))
                .addVariable(amountGatheredPerSecond, nameof(amountGatheredPerSecond))
                .print();
        }
    }
}