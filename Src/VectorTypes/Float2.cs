using System;

namespace Src.VectorTypes
{
    /// <summary>
    ///     Helper class for serialization of basic unity built-in types
    /// </summary>
    [Serializable]
    public struct Float2
    {
        public float x;
        public float y;

        public Float2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
        
        public static Float2 operator +(Float2 a, Float2 b)
        {
            return new Float2(a.x + b.x, a.y + b.y);
        }

        public static Float2 operator -(Float2 a, Float2 b)
        {
            return new Float2(a.x - b.x, a.y - b.y);
        }

        public static Float2 operator -(Float2 a)
        {
            return new Float2(-a.x, -a.y);
        }

        public static Float2 operator *(Float2 a, float b)
        {
            return new Float2(a.x * b, a.y * b);
        }

        public static Float2 operator *(float a, Float2 b)
        {
            return new Float2(b.x * a, b.y * a);
        }

        public static float Distance(Float2 a, Float2 b)
        {
            return (float) Math.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
        }

        public override string ToString()
        {
            return "{" + x + " " + y + "}";
        }

        public float Distance(Float2 other)
        {
            return (float) Math.Sqrt((x - other.x) * (x - other.x) + (y - other.y) * (y - other.y));
        }
        
        public static Float2 zero => new Float2(0, 0);
    }
}