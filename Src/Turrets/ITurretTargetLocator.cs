namespace Src.Turrets
{
    public interface ITurretTargetLocator
    {
        IDemagableTarget getTarget();
    }
}