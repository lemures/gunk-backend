﻿using System.Collections.Generic;
using NUnit.Framework;
using Src.DataStructures;

namespace SrcTests.DataStructures
{
    public class DisjointSetTests
    {
        [Test]
        public void unionAndOneSetWorkTest()
        {
            var disjointSet = new DisjointSet();
            disjointSet.union(1001, 1003);
            disjointSet.union(1001, 1002);
            Assert.IsTrue(disjointSet.oneSet(1002, 1003));
            disjointSet.oneSet(1, 1001);
            disjointSet.removeConnectionsWithElement(1001);
            Assert.IsFalse(disjointSet.oneSet(1002, 1003));
        }

        [Test]
        public void groupIntoSetsTest()
        {
            var disjointSet = new DisjointSet();
            disjointSet.union(1000, 2000);
            disjointSet.union(1000, 3000);
            disjointSet.union(1000, 4000);
            disjointSet.union(100, 400);

            var groupedSets1 = disjointSet.groupIntoSets(new List<int> {1000, 2000, 3000, 4000, 100, 400});
            Assert.AreEqual(2, groupedSets1.Count);
            Assert.AreEqual(6, groupedSets1[0].Count + groupedSets1[1].Count);
            foreach (var set in groupedSets1)
            foreach (var a in set)
            foreach (var b in set)
                Assert.IsTrue(disjointSet.oneSet(a, b), a + "  " + b + " were grouped into the same set");
        }
    }
}