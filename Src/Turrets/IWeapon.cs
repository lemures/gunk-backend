using Gunk_Scripts.GunkComponents;

namespace Src.Turrets
{
    public interface IWeapon : IUpdateCallbackReceiver
    {
        /// <summary>
        /// returns true is the weapon can be fired again
        /// </summary>
        bool isReady { get; }
        
        /// <summary>
        /// fires bullet against 
        /// </summary>
        void fire(IDemagableTarget target);

        /// <summary>
        /// stops all actions the weapon is doing
        /// </summary>
        void stop();
    }
}