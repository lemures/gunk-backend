﻿using System;
using Src.Utils;

namespace Gunk_Scripts.GunkComponents
{
    [Serializable]
    public struct Counter
    {
        public Counter(float currentValueInternal, float maxValueInternal)
        {
            currentValue = currentValueInternal;
            maxValue = maxValueInternal;
        }

        public float maxValue { get; }

        public float currentValue { get; private set; }

        public float valueToFull => maxValue - currentValue;

        public bool isFull()
        {
            return maxValue - currentValue == 0.0f;
        }

        public bool isEmpty()
        {
            return currentValue == 0.0f;
        }

        public void decreaseValue(float howMuchToDecrease)
        {
            currentValue = currentValue - howMuchToDecrease;
            currentValue = Math.Max(0, currentValue);
        }

        public void increaseValue(float howMuchToIncrease)
        {
            currentValue = currentValue + howMuchToIncrease;
            currentValue = Math.Min(maxValue, currentValue);
        }

        public void changeValue(float newValue)
        {
            currentValue = newValue;
            currentValue = Math.Min(maxValue, currentValue);
            currentValue = Math.Max(0, currentValue);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(Counter))
                .addVariable(currentValue, nameof(currentValue))
                .addVariable(maxValue, nameof(maxValue))
                .print();
        }
    }
}