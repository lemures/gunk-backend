using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public interface IPositionChangedCallbackReceiver
    {
        void changePosition(Float3 newPosition);
    }
}