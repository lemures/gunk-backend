﻿using System;
using Src.VectorTypes;

namespace Src.SkeletonAnimations.BoneTypes
{
    [Serializable]
    public class BarrelBone : Bone
    {
        protected bool Equals(BarrelBone other)
        {
            return index == other.index && parentLimbNum == other.parentLimbNum && parentSegmNum == other.parentSegmNum;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = index;
                hashCode = (hashCode * 397) ^ parentLimbNum;
                hashCode = (hashCode * 397) ^ parentSegmNum;
                return hashCode;
            }
        }

        public int index;
        public int parentLimbNum;
        public int parentSegmNum;

        public BarrelBone(int parentSegmNum, int parentLimbNum, int index, Float3 position) : base(
            position)
        {
            this.parentSegmNum = parentSegmNum;
            this.parentLimbNum = parentLimbNum;
            this.index = index;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as BarrelBone;
            return bone != null &&
                   index == bone.index;
        }

        public override string getName()
        {
            return "BarrelBone " + index + ", limb " + parentLimbNum + " segment " + parentSegmNum + 1;
        }
    }
}