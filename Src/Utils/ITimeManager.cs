﻿namespace Src.Utils
{
    public interface ITimeManager
    {
        float time { get; }
        float deltaTime { get; }
        void freezeTime();
        void unfreezeTime();
        void reset();
    }
}