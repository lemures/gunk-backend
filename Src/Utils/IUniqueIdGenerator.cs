namespace Src.Utils
{
    public interface IUniqueIdGenerator
    {
        int getId();

        void returnId(int id);
    }
}