﻿using Gunk_Scripts.GunkComponents;
using Src.Logic;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkObjects
{
    public class Connector : GunkObject
    {
        private static readonly string prefabName = "Connector";
        private static readonly int maxHealth = 1000;

        public Connector(IGunkObjectManagerInternalData objectManager, Float3 position, Faction faction) : base(objectManager,
            prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleConnector(this, 20));
            addPlacedObject();
            addCuboidCollider(new Float3(2, 6, 2));
            addEnergyNeededForConstruction(1000.0f);
            finishAddingComponents();
        }
    }
}