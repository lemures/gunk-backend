using System;
using System.Collections.Generic;
using System.Diagnostics;
using Src.GameRenderer;
using Src.SerializationUtils;
using Src.SkeletonAnimations;
using Src.Utils;
using Src.VectorTypes;

namespace Src.Turrets
{
    public class MortarWeapon : IWeapon
    {
        private class MortarBullet
        {
            private float timeAlive;

            private int id;

            private IDemagableTarget target;

            private IObjectsChangesReceiver objectsChangesReceiver;

            private IBarrelPositionCalculator barrelPositionCalculator;
            
            private const float bulletSpeed = 10.0f;

            private const float damage = 4.0f;

            private int barrelId;

            private bool isDestroyed = false;
            
            public MortarBullet(IDemagableTarget target, 
                IUniqueIdGenerator idGenerator, 
                IObjectsChangesReceiver objectsChangesReceiver, 
                IBarrelPositionCalculator barrelPositionCalculator,
                int barrelId)
            {
                id = idGenerator.getId();
                this.target = target;
                this.barrelId = barrelId;
                this.objectsChangesReceiver = objectsChangesReceiver;
                this.barrelPositionCalculator = barrelPositionCalculator;

                var bulletLocation = calculateCurrentBulletLocation(); 
                var bulletStartPosition = bulletLocation.position;
                var bulletEndPosition = bulletLocation.position - bulletLocation.directoryNormal;
                var createBullet = new AddMortarBullet(id, bulletStartPosition, bulletEndPosition);
                objectsChangesReceiver.sendChange(createBullet);
            }

            private BulletLocation calculateCurrentBulletLocation()
            {
                var startPosition = barrelPositionCalculator.getBarrelBoneWorldPosition(barrelId);
                var endPosition = target.currentPosition;
                return BulletTrajectory.calculateLocation(startPosition, endPosition, bulletSpeed, timeAlive);                
            }
            
            public bool hasReachedTarget()
            {
                var startPosition = barrelPositionCalculator.getBarrelBoneWorldPosition(barrelId);
                var endPosition = target.currentPosition;
                return BulletTrajectory.hasReachedDestination(startPosition, endPosition, bulletSpeed, timeAlive);
            }
            
            public void update(float elapsed)
            {
                timeAlive += elapsed;

                if (!hasReachedTarget())
                {
                    updateBulletPosition();
                }
                else
                {
                    if (isDestroyed) return;
                    
                    target.damage(10.0f);
                    destroy();
                }
            }

            private void updateBulletPosition()
            {
                var bulletLocation = calculateCurrentBulletLocation();
                var bulletStartPosition = bulletLocation.position;
                var bulletEndPosition = bulletLocation.position - bulletLocation.directoryNormal;
                var change = new UpdateMortarBullet(bulletStartPosition, bulletEndPosition, id);
                objectsChangesReceiver.sendChange(change);
            }
            
            public void destroy()
            {
                if (isDestroyed)
                {
                    return;
                }
                isDestroyed = true;
                var change = new RemoveMortarBullet(id);
                objectsChangesReceiver.sendChange(change);
            }
        }
        
        private readonly IBarrelPositionCalculator barrelPositionCalculator;
        private readonly IObjectsChangesReceiver changesReceiver;
        private readonly IUniqueIdGenerator idGenerator;

        private readonly float reloadTime = 2.0f;
        private float timeIdle = 10.0f;

        private List<MortarBullet> bullets = new List<MortarBullet>();

        public MortarWeapon(IBarrelPositionCalculator barrelPositionCalculator, IObjectsChangesReceiver changesReceiver, IUniqueIdGenerator idGenerator)
        {
            this.barrelPositionCalculator = barrelPositionCalculator;
            this.changesReceiver = changesReceiver;
            this.idGenerator = idGenerator;
        }      

        public bool isReady => timeIdle >= reloadTime;

        public void fire(IDemagableTarget target)
        {
            if (target == null) throw new NullReferenceException("target cannot be null");
            
            if (!isReady) {return;}

            timeIdle = 0;
            var bullet = new MortarBullet( target, idGenerator, changesReceiver, barrelPositionCalculator, 0);  
            bullets.Add(bullet);
        }

        private void destroyAllBullets()
        {
            bullets.ForEach(x => x.destroy());
            bullets.Clear();
        }

        public void stop()
        {
            //nothing to do here, what is fired cannot be unfired
        }

        public void update(float deltaTime)
        {
            timeIdle += deltaTime;
            bullets.ForEach(x => x.update(deltaTime));
            bullets.RemoveAll(x => x.hasReachedTarget());
        }
    }
}