using Src.VectorTypes;

namespace Src.GameRenderer
{
    public class AddFogOfWarObject : ExtraObjectsChange
    {
        public int id;
        public Float3 position;

        public AddFogOfWarObject(int id, Float3 position)
        {
            this.id = id;
            this.position = position;
        }

        public override void execute(IExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.fogOfWarObjectsCollection.addObject(id, position);
        }
    }
}