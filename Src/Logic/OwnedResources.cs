using System;
using System.Collections.Generic;
using Gunk_Scripts.GunkComponents;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Src.Maps;
using Src.Utils;

namespace Src.Logic
{
    public class OwnedResources
    {
        [JsonProperty]
        private readonly Dictionary<ResourceType, float> currentLevels = new Dictionary<ResourceType, float>();

        public OwnedResources()
        {
            foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
                currentLevels.Add(resourceType, 0);
        }

        public void addResources(GatheredResources resources)
        {
            currentLevels[resources.type] += resources.amount;
        }

        public float getResourcesCount(ResourceType resourceType)
        {
            return currentLevels[resourceType];
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(OwnedResources))
                .addVariable(currentLevels, nameof(currentLevels))
                .print();
        }
    }
}