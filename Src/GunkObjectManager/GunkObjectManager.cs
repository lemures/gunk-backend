﻿using System.Collections.Generic;
using System.Dynamic;
using Gunk_Scripts.GunkObjects;
using Src.DataStructures;
using Src.GameRenderer;
using Src.GunkComponents;
using Src.GunkObjectManager;
using Src.Logic;
using Src.Maps;
using Src.SerializationUtils;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Internal;
using Src.Utils;
using Src.VectorTypes;

namespace Gunk_Scripts.GunkComponents
{
    public class GunkObjectManager : IGunkObjectManager
    {   
        private readonly GunkObjectManagerInternalData internalData;

        public GunkObjectManager(IExtraObjectsRenderer extraObjectsRenderer, 
            IMap map, 
            IProfiler profiler, 
            IGunkLogger logger, 
            IAssetManager assetManager, 
            IAnimatorsManager animatorsManager)
        {
            internalData =
                new GunkObjectManagerInternalData(this, extraObjectsRenderer, map,
                    profiler, logger, assetManager, animatorsManager);
        }

        public List<GunkObjectData> getObjectsData()
        {
            return internalData.getObjectsData();
        }

        public void spawnObject(GunkObjectType type, Float3 position, Faction faction)
        {
            GunkObjectFactory.spawnObject(internalData, type, position, faction);
        }

        public bool canPlaceObject(Float3 position, string prefabName)
        {
            var pattern = calculatePrefabMeshDimensions(prefabName);
            return internalData.canSpaceBeLocked(position, pattern);
        }

        public Float3? getClosestFreePosition(Float3 position, string prefabName)
        {
            var pattern = calculatePrefabMeshDimensions(prefabName);
            return internalData.calculateClosestAvailablePosition(position, pattern);
        }

        public bool canConnectBuilding(Float3 position, Faction faction)
        {
            return internalData.connectablesBuildingsModule.isConnectorNearby(position, faction);
        }

        //TODO this function clearly does not belong here
        public void temporarilyConnectBuilding(List<SocketData> socketBones, Float3 position, Faction faction)
        {
            internalData.connectablesBuildingsModule.temporarilyConnectBuilding(socketBones, position, faction);
        }

        //TODO this function clearly does not belong here
        public void clearTemporaryConnections()
        {
            internalData.connectablesBuildingsModule.clearTemporaryConnections();
        }

        public void update(float deltaTime)
        {
            internalData.update(deltaTime);
        }

        public ObjectPattern calculatePrefabMeshDimensions(string prefabName)
        {
            return internalData.calculatePrefabMeshDimensions(prefabName);
        }
    }
}