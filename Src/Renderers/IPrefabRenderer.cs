﻿using Src.VectorTypes;

namespace Src.Renderers
{
    public interface IPrefabRenderer
    {
        void instantiatePrefab(int id, string prefabName, Float3 position, Float3 eulerRotation);
        void destroyObject(int id);
        void destroyAllObjects();
        void setPosition(int id, Float3 newPosition);
        void setRotation(int id, Float3 newEulerRotation);
        void hideGameObjects();
        void showGameObjects();
    }
}