﻿namespace Src.Logic
{
    public enum Faction
    {
        None,
        MainPlayer,
        Corruption,
        Nature,
        Player2,
        Player3,
        Player4
    }
}