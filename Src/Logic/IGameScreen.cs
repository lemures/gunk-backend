namespace Src.Logic
{
    public interface IGameScreen
    {
        void update(float deltaTime);

        void enable();

        void disable();
    }
}