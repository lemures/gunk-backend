using System;
using System.Runtime.InteropServices;
using Src.SkeletonAnimations.CurvesTypes;

namespace Src.Utils
{
    public static class GuidUtils
    {
        public static string value(Type type)
        {
            return ((GuidAttribute) Attribute.GetCustomAttribute(type, typeof(GuidAttribute))).Value;
        }
    }
}